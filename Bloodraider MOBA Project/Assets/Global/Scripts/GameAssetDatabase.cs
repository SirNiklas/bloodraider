﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;

public class GameAssetDatabase : MonoBehaviour
{
    public static GameAssetDatabase Instance;
    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public int spellsVersion, itemsVersion, buffsVersion;
    public string progressInfo;

    public string versionsDownloadLink, spellResourceDownloadLink, itemResourceDownloadLink, buffsResourceDownloadLink;
    public List<string> spellAssetNameList, itemAssetNameList, buffAssetNameList;

    public Dictionary<string, Spell> spells;
    //public Dictionary<string, Item> items;
    public Dictionary<string, Buff> buffs;

    private WWW spellsWWW, itemsWWW, buffsWWW;

    public void LoadVersions()
    {
        using (WebClient w = new WebClient())
        {
            string[] versionsSplitted = w.DownloadString(versionsDownloadLink).Split(',');

            spellsVersion = int.Parse(versionsSplitted[0]);
            itemsVersion = int.Parse(versionsSplitted[1]);
            buffsVersion = int.Parse(versionsSplitted[2]);
        }
    }

    public void LoadSpells()
    {
        WWW spellsWWW = WWW.LoadFromCacheOrDownload(spellResourceDownloadLink, spellsVersion);

        //while (!spellsWWW.isDone)
        //{
        //    progressInfo = "Loading spell-resources: " + spellsWWW.progress.ToString();
        //}
        foreach (var item in spellsWWW.assetBundle.LoadAll())
        {
            print(item.name);
        }
        foreach (var item in spellAssetNameList)
        {
            spells.Add(item, (Spell)spellsWWW.assetBundle.Load(item));
        }
    }

    public void LoadItems()
    {

    }

    public void LoadBuffs()
    {
        WWW buffsWWW = WWW.LoadFromCacheOrDownload(buffsResourceDownloadLink, buffsVersion);

        if (buffsWWW.error != null)
        {
            print(buffsWWW.error);
        }
        //while (!spellsWWW.isDone)
        //{
        //    progressInfo = "Loading buff-resources: " + spellsWWW.progress.ToString();
        //}
        foreach (var item in buffAssetNameList)
        {
            buffs.Add(item, (Buff)buffsWWW.assetBundle.Load(item));
        }
    }
    private IEnumerator LoadBuffsAsync()
    {
        
        yield return buffsWWW;

    }
}

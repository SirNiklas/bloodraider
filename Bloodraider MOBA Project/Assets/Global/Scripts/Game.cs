﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;

[Serializable]
public class Game
{
    public PlayMode playMode;

    public List<string> allPlayers, spectators;
    public List<PlayerSlot> greenSlots, redSlots;
    public PlayerSlot currentSlot;

    public Game()
    {
        greenSlots = new List<PlayerSlot>();
        redSlots = new List<PlayerSlot>();
        allPlayers = new List<string>();
        spectators = new List<string>();
    }

    #region Playerslots
    public void JoinGreen(string name, NetworkPlayer player)
    {
        RemoveSlot(name);

        PlayerSlot p = new PlayerSlot(name, UnitSide.Green, true, player);
        if (name == GameInformationManager.Instance.userProfileName)
            currentSlot = p;

        greenSlots.Add(p);
    }
    public void JoinRed(string name, NetworkPlayer player)
    {
        RemoveSlot(name);

        PlayerSlot p = new PlayerSlot(name, UnitSide.Red, true, player);
        if (name == GameInformationManager.Instance.userProfileName)
            currentSlot = p;

        redSlots.Add(p);
    }
    public void RemoveSlot(string name)
    {
        PlayerSlot pGreen = DoesSideContainName(name, true);
        if (pGreen != null)
            greenSlots.Remove(pGreen);
        PlayerSlot pRed = DoesSideContainName(name, false);
        if (pRed != null)
            redSlots.Remove(pRed);
    }

    public string[] GetGreenPlayers()
    {
        string[] gs = new string[greenSlots.Count];
        for (int i = 0; i < greenSlots.Count; i++)
        {
            gs[i] = greenSlots[i].name;
        }

        return gs;
    }
    public string[] GetRedPlayers()
    {
        string[] rs = new string[redSlots.Count];
        for (int i = 0; i < redSlots.Count; i++)
        {
            rs[i] = redSlots[i].name;
        }

        return rs;
    }
    //public string[] GetAllPlayers()
    //{
    //    string[] aps = new string[allPlayers.Count];
    //    for (int i = 0; i < allPlayers.Count; i++)
    //    {
    //        aps[i] = allPlayers[i];
    //    }

    //    return aps;
    //}

    public PlayerSlot DoesSideContainName(string name, bool green)
    {
        if (green)
        {
            return greenSlots.Find((p) => { if (p.name == name) return true; else return false; });
        }
        else
        {
            return redSlots.Find((p) => { if (p.name == name) return true; else return false; });
        }
    }
    #endregion

    public void SetPlayMode(string mode)
    {
        playMode = (PlayMode)Enum.Parse(typeof(PlayMode), mode);
    }

    //public void FillEmptySlotsWithBots()
    //{
    //    for (int i = 0; i < greenSlots.Length; i++)
    //    {
    //        if (!greenSlots[i].isEmpty)
    //            continue;

    //        greenSlots[i] = new PlayerSlot("", UnitSide.Green, false);
    //        greenSlots[i].name = "Bot " + greenSlots[i].GetHashCode();
    //    }
    //    for (int i = 0; i < redSlots.Length; i++)
    //    {
    //        if (!redSlots[i].isEmpty)
    //            continue;

    //        redSlots[i] = new PlayerSlot("", UnitSide.Neutral, false);
    //        redSlots[i].name = "Bot " + redSlots[i].GetHashCode();
    //    }
    //}
    //public PlayerSlot GetUserPlayerSlot()
    //{
    //    if (greenSlots[0].isPlayer)
    //        return greenSlots[0];
    //    else if (redSlots[0].isPlayer)
    //        return redSlots[0];

    //    return null;
    //}

    public static Game LoadFromTemp()
    {
        BinaryFormatter b = new BinaryFormatter();
        using (FileStream fs = File.Open(@"temp\current.dat", FileMode.Open))
        {
            return (Game)b.Deserialize(fs);
        }
    }
    public static void SaveToTemp(Game game)
    {
        BinaryFormatter b = new BinaryFormatter();
        using (FileStream fs = File.Open(@"temp\current.dat", FileMode.Create))
        {
            b.Serialize(fs, game);
        }
    }
}

[Serializable]
public enum PlayMode
{
    FullDraft
}
﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PlayerSlot
{
    public string name, hero;
    public UnitSide side;

    [NonSerialized]
    public NetworkPlayer networkPlayer;

    public bool isPlayer;
    public bool isEmpty { get { return (name == "" && side == UnitSide.Neutral); } }

    public PlayerSlot(string _name, UnitSide _side, bool isplayer, NetworkPlayer player)
    {
        name = _name;
        hero = "";
        side = _side;
        isPlayer = isplayer;
        networkPlayer = player;
    }

    public void SetEmpty()
    {
        name = "";
        hero = "";
        side = UnitSide.Neutral;
        isPlayer = false;
    }
}

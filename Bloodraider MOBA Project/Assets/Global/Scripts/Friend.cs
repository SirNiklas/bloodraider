﻿using UnityEngine;
using System.Collections;
using System.Net;
using System;

[Serializable]
public class Friend
{
    public int raiderId;
    public string currentName;
    public FriendActivity friendActivity;

    public bool canFriendsJoin;
    public string currentLobbyGUID, currentLobbyPassword;

    public Friend()
    {
        raiderId = 0;
    }

    public bool EqualsFriend(Friend other)
    {
        if(other.currentName == currentName)
            return true;
        else return false;
    }
}

[Serializable]
public enum FriendActivity
{
    InLobby,
    InGame,
    Watching,
    Online,
    Offline,
}
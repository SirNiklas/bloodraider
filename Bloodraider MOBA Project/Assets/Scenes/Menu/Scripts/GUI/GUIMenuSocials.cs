﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;

public class GUIMenuSocials : MonoBehaviour
{
    public static GUIMenuSocials Instance;

    public Rect socialsRect, socialsTopRect;
    public Stack<string> socialFeed;
    public bool isFriendlistEnabled;
    public string currentErrorMsg;
    public int currentFriendlistServerPing;

    private string friendRaiderIDText = "<FriendRaiderID>";
    private Vector2 friendlistScrollPos, friendrequestsScrollPos, friendBlocksScrollPos, socialFeedListScrollPos;

    void Awake()
    {
        Instance = this;
        LoadSocialFeed();

        if (socialFeed == null)
            socialFeed = new Stack<string>();

        socialsTopRect = new Rect(10, 10, 400, 80);
        socialsRect = new Rect(10, 100, 400, Screen.width / 2 - 100);
    }
    void OnApplicationQuit()
    {
        SaveSocialFeed();
    }
    void OnGUI()
    {
        if (!GUIMenu.Instance.hasLoaded)
            return;
        if (!GUIMenu.Instance.isUsernameChosen)
            return;

        #region Top bar
        GUI.Box(socialsTopRect, "");

        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(socialsTopRect));
        GUILayout.BeginVertical();

        if (!isFriendlistEnabled)
        {
            if (GUILayout.Button("Retry connecting to friendlist server"))
            {
                FriendlistManager.Instance.PingAndControlFriendlistServerSystems();
            }

            GUILayout.Label(currentErrorMsg);

            GUILayout.EndVertical();
            GUILayout.EndArea();
            return;
        }
        else
        {
            GUILayout.Label(currentErrorMsg);
            GUILayout.Label("The friendlist server is online.");
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
        #endregion

        #region Friends
        GUI.Box(socialsRect, "Socials");
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(socialsRect));
        GUILayout.BeginVertical();

        GUILayout.Label("Friends");
        GUILayout.Space(8);

        GUILayout.Label("Your Raider-ID: #" + FriendlistManager.Instance.me.raiderId);

        GUILayout.Label("Add someone");
        GUILayout.BeginHorizontal();
        friendRaiderIDText = GUILayout.TextField(friendRaiderIDText, 30);
        if (GUILayout.Button("Add"))
        {
            FriendlistManager.Instance.SendAddFriend(int.Parse(friendRaiderIDText));
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(8);

        #region Friendlist
        GUILayout.Label("Friendlist");
        friendlistScrollPos = GUILayout.BeginScrollView(friendlistScrollPos);

        Friend toRemove = null;
        Friend toBlock = null;
        foreach (var friend in FriendlistManager.Instance.friends)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(friend.currentName + "\n" + friend.friendActivity.ToString() + ", #" + friend.raiderId);

            if (GUILayout.Button("Remove"))
            {
                toRemove = friend;
            }
            if (GUILayout.Button("Block"))
            {
                toBlock = friend;
            }

            GUI.enabled = friend.canFriendsJoin;
            if (GUILayout.Button("Join"))
            {
                Network.Connect(friend.currentLobbyGUID, friend.currentLobbyPassword);
            }
            GUI.enabled = true;

            GUILayout.EndHorizontal();
        }

        if (toRemove != null)
        {
            FriendlistManager.Instance.RemoveFriend(toRemove);
            toRemove = null;
        }
        if (toBlock != null)
        {
            FriendlistManager.Instance.MoveFriendToBlockList(toBlock);
            toBlock = null;
        }

        if (FriendlistManager.Instance.friends.Count <= 0)
            GUILayout.Label("No friends found");
        GUILayout.EndScrollView();
        GUILayout.Space(8);
        #endregion

        #region Blocked players
        GUILayout.Label("Blocked players");
        friendlistScrollPos = GUILayout.BeginScrollView(friendlistScrollPos);

        foreach (var friend in FriendlistManager.Instance.friendBlocklist)
        {
            GUILayout.Label(friend.currentName);
        }

        if (FriendlistManager.Instance.friendBlocklist.Count <= 0)
            GUILayout.Label("No blocked players found");
        GUILayout.EndScrollView();
        #endregion

        #region Social feed
        GUILayout.Label("Social feed");
        GUILayout.Space(8);

        socialFeedListScrollPos = GUILayout.BeginScrollView(socialFeedListScrollPos);
        foreach (var post in socialFeed)
        {
            GUILayout.Label(post);
        }
        GUILayout.EndScrollView();
        #endregion

        GUILayout.EndVertical();
        GUILayout.EndArea();
        #endregion
    }

    public void PostFeedMessage(string message)
    {
        socialFeed.Push(message);

        if (socialFeed.Count > 20)
            socialFeed.Pop();
    }

    void SaveSocialFeed()
    {
        string msg = "";
        foreach (var feedmsg in socialFeed)
        {
            msg += "$" + feedmsg;
        }
        if(msg.Length > 0)
            msg.Remove(0, 1);

        using (StreamWriter sw = new StreamWriter(File.Create(@"save\socialfeed.data")))
        {
            sw.Write(msg);
        }
    }
    void LoadSocialFeed()
    {
        if (!File.Exists(@"save\current_socialfeed.data"))
            return;

        string msg = "";

        using (StreamReader sw = new StreamReader(File.Open(@"save\socialfeed.data", FileMode.Open, FileAccess.Read)))
        {
            msg = sw.ReadToEnd();
        }

        string[] msgSplitted = msg.Split('$');
        foreach (var item in msgSplitted)
        {
            PostFeedMessage(item);
        }
    }
}

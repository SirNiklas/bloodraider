﻿using UnityEngine;
using System.Collections;
using System.Net;
using System;
using System.Collections.Generic;

using System.Linq;
using System.ComponentModel;
using System.Threading;


public class GUIMenu : MonoBehaviour
{
    public static GUIMenu Instance;

    public const int serverPort = 55000;
    public Texture2D loadingScreenTexture;
    public Rect tabSelectionRect, tabRect, chooseUsernameRect;
    public MenuTabs currentlySelectedTab;
    public MenuPlayTabs currentlySelectedPlayTab;
    public Game game;
    public NetworkReachability internetReachability;
    public List<string> chatMessages;

    public bool hasLoaded, inLobby, autoClose, isUsernameChosen = true, cheatsEnabledGameSettings, playTutorial;

    private string newsText, loadingText, lobbyChatText = "";
    private Vector2 newsScrollPos, lobbiesScrollPos, watchScrollPos, lobbyChatScrollPos;

    private float chatMessageSpamCheckDeltaTimeCounter;
    public string currentLobbyServerGUID, natFacilitatorIP, masterServerIP, textConnectIP = "127.0.0.1", textConnectPassword = "<Password>";
    public bool refreshModePlay = true, canFriendsJoin;
    private string lobbyGameStartStateText, currentMatch, username = "<Username>";
    private string[] allCurrentLeagues, currentlyWatchedLeague;
    private HostData[] hostsPlay, hostsWatch;
    private string pw = "<Password>", serverName = "Bloodraider lobby", serverPw = "<Password>", serverKey = "<LeagueKey>";
    public int serverMaxConnectedPlayers = 100;
    private PlayMode filterPlayMode = PlayMode.FullDraft;
    private BackgroundWorker bgWorker;

    public GUIMenu()
    {
        Instance = this;
    }

    void Initialize()
    {
        loadingText = "Initializing...";

        if (internetReachability == NetworkReachability.NotReachable && !Debug.isDebugBuild)
        {
            loadingText = "No internet connection found, client closing...";
            Debug.LogError("No internet connection found, client closing...");

            //autoClose = true;
            return;
        }

        try
        {
            loadingText = "Checking newest version...";
            IsClientUpToDate();

            loadingText = "Loading...";
            #region Initialization
            using (WebClient w = new WebClient())
            {
                newsText = w.DownloadString("http://dl.dropboxusercontent.com/u/89935882/Bloodraider%20MOBA/m_news.txt");

                string[] ips = w.DownloadString("http://dl.dropboxusercontent.com/u/89935882/Bloodraider%20MOBA/g_networkserverinfo.txt").Split('\n');
                natFacilitatorIP = ips[0];
                masterServerIP = ips[1];
            }
            #endregion
        }
        catch (Exception ex)
        {
            loadingText = "An error has occured: " + ex.ToString();

            Debug.LogException(ex);
            //autoClose = true;

            //return;
        }
    }

    void bgWorker_DoWork(object sender, DoWorkEventArgs e)
    {
        Initialize();
    }
    void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
        hasLoaded = true;
    }

    void Awake()
    {
        chooseUsernameRect = new Rect(Screen.width / 2 - 120, Screen.height / 2 - 80, 240, 160);
        tabSelectionRect = new Rect(Screen.width / 2 - 300, 5, 600, 70);
        tabRect = new Rect(Screen.width / 2 - 300, 85, 600, Screen.height - 100);

        System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
    }

    void Start()
    {
        networkView.group = 1;
        internetReachability = Application.internetReachability;

        bgWorker = new BackgroundWorker();
        bgWorker.DoWork +=bgWorker_DoWork;
        bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
        bgWorker.RunWorkerAsync();
    }

    void Update()
    {
        //if (hasLoaded && Network.natFacilitatorIP != natFacilitatorIP)
        //{
        //    //Network.natFacilitatorIP = natFacilitatorIP;
        //    //Network.natFacilitatorPort = 50005;

        //    //MasterServer.ipAddress = masterServerIP;
        //    //MasterServer.port = 23466;

        //    //Network.connectionTesterIP = natFacilitatorIP;
        //    //Network.connectionTesterPort = 50005;

        //    //Debug.LogError("Testconnection: " + Network.TestConnection());
        //    //Debug.LogError("Testconnection NAT: " + Network.TestConnectionNAT());

        //    //print("Connection status: " + Network.TestConnection() + ", NAT-facilitator status: " + Network.TestConnectionNAT());
        //}

        if (chatMessageSpamCheckDeltaTimeCounter >= 0)
        {
            chatMessageSpamCheckDeltaTimeCounter += Time.deltaTime;
            if (chatMessageSpamCheckDeltaTimeCounter >= SystemChat.chatMessageWritingCooldown)
                chatMessageSpamCheckDeltaTimeCounter = -1;
        }

        if (Network.peerType == NetworkPeerType.Disconnected)
        {
            FriendlistManager.Instance.OnDisconnectFromLobby();
            FriendlistManager.Instance.me.friendActivity = FriendActivity.Online;
        }
        else
            FriendlistManager.Instance.OnJoinLobby(canFriendsJoin, Network.player.guid, Network.incomingPassword);

        if (refreshModePlay)
        {
            hostsWatch = null;
            hostsPlay = MasterServer.PollHostList();
        }
        else
        {
            hostsPlay = null;
            hostsWatch = MasterServer.PollHostList();
        }

        if (autoClose)
        {
            Application.Quit();
        }
    }

    void OnGUI()
    {
        if (!hasLoaded)
        {
            //GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loadingScreenTexture);
            GUI.Label(new Rect(10, 10, 200, 45), loadingText);

            return;
        }
        if (!isUsernameChosen)
        {
            GUI.Box(chooseUsernameRect, "Choose a username");
            GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(chooseUsernameRect));
            GUILayout.BeginVertical();

            username = GUILayout.TextField(username, 22);

            playTutorial = GUILayout.Toggle(playTutorial, "Play the beginner tutorial");

            GUI.enabled = (username.Length > 3 && username != "<Username>" && !username.Contains("$") && !username.Contains(" "));
            if (GUILayout.Button("Okay"))
            {
                GameInformationManager.Instance.SetUsernameAndGuid(username);
                isUsernameChosen = true;

                if (playTutorial)
                {
                    Game game = new Game();
                    game.currentSlot = new PlayerSlot(GameInformationManager.Instance.userProfileName, UnitSide.Green, true, default(NetworkPlayer));
                    game.greenSlots.Add(game.currentSlot);
                    game.allPlayers.Add(GameInformationManager.Instance.userProfileName);

                    LocalGameSettings local = new LocalGameSettings();
                    local.areCheatsEnabled = false;
                    local.isLeagueMatch = false;
                    local.isSpectatorMode = false;
                    local.leagueName = "";
                    local.matchName = "Tutorial";

                    Game.SaveToTemp(game);
                    LocalGameSettings.SaveToTemp(local);

                    Application.LoadLevel("Tutorial");
                }
            }
            GUI.enabled = true;

            GUILayout.EndVertical();
            GUILayout.EndArea();

            return;
        }

        #region Tabselection
        GUI.Box(tabSelectionRect, "Bloodraider v" + GameInformationManager.version);

        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(tabSelectionRect));
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("News"))
        {
            currentlySelectedTab = MenuTabs.News;
        }
        if (GUILayout.Button("Play"))
        {
            refreshModePlay = true;
            RefreshHosts();

            currentlySelectedTab = MenuTabs.Play;
        }
        GUI.enabled = false;
        if (GUILayout.Button("Watch"))
        {
            refreshModePlay = false;
            currentlySelectedTab = MenuTabs.Watch;
        }
        GUI.enabled = true;
        if (GUILayout.Button("Profile/Settings"))
        {
            currentlySelectedTab = MenuTabs.ProfileAndSettings;
        }
        if (GUILayout.Button("Exit"))
        {
            currentlySelectedTab = MenuTabs.Exit;
        }

        GUILayout.EndHorizontal();
        GUILayout.EndArea();
        #endregion
        
        #region Tab
        GUI.Box(tabRect, currentlySelectedTab.ToString().Replace("And", "/"));
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(tabRect));

        switch (currentlySelectedTab)
        {
            #region News
            case MenuTabs.News:
                newsScrollPos = GUILayout.BeginScrollView(newsScrollPos);
                GUILayout.Label(newsText);
                GUILayout.EndScrollView();
                break;
            #endregion

            #region Play
            case MenuTabs.Play:
                GUILayout.Label(currentlySelectedPlayTab.ToString());

                GUILayout.BeginVertical();
                #region Disconnected
                if (Network.peerType == NetworkPeerType.Disconnected)
                {
                    GUILayout.BeginHorizontal();

                    GUILayout.BeginVertical();
                    if (GUILayout.Button("League lobby", GUILayout.MaxWidth(100)))
                    {
                        currentlySelectedPlayTab = MenuPlayTabs.League;
                    }
                    if (GUILayout.Button("Public lobby", GUILayout.MaxWidth(100)))
                    {
                        currentlySelectedPlayTab = MenuPlayTabs.Public;
                    }
                    GUILayout.EndVertical();

                    GUILayout.BeginVertical();

                    serverName = GUILayout.TextField(serverName, 38);

                    GUI.enabled = currentlySelectedPlayTab == MenuPlayTabs.Public;
                    serverPw = GUILayout.TextField(serverPw, 30);
                    GUI.enabled = true;

                    #region League
                    if (currentlySelectedPlayTab == MenuPlayTabs.League)
                    {
                        GUILayout.Space(8);
                        GUILayout.Label("League settings\n------");
                        
                        serverKey = GUILayout.TextField(serverKey, 48);
                        GUILayout.Space(8);

                        GUILayout.Label("Max. connected players: " + serverMaxConnectedPlayers);
                        GUILayout.Label("(Minimal: 10 Maximal: 100)");
                        serverMaxConnectedPlayers = Mathf.RoundToInt(GUILayout.HorizontalSlider((int)serverMaxConnectedPlayers, 10, 100));

                        GUILayout.Label("------");
                        GUILayout.Space(8);
                    }
                    #endregion

                    if (GUILayout.Button("Create lobby (" + filterPlayMode + ")"))
                    {
                        if (currentlySelectedPlayTab == MenuPlayTabs.League && IsValidLeague(serverKey) == null)
                        {
                            print("The given league key is not valid.");
                            return;
                        }

                        game = new Game();

                        if (currentlySelectedPlayTab == MenuPlayTabs.League)
                            Network.incomingPassword = serverKey;
                        else
                            Network.incomingPassword = serverPw;

                        if (currentlySelectedPlayTab == MenuPlayTabs.Public)
                        {
                            serverKey = "";
                            serverMaxConnectedPlayers = 10;
                        }

                        inLobby = true;

                        Network.InitializeSecurity();
                        NetworkConnectionError error = Network.InitializeServer(serverMaxConnectedPlayers, serverPort, !Network.HavePublicAddress());
                        if (error != NetworkConnectionError.NoError)
                        {
                            Debug.LogError("Server could not be initialized: " + error.ToString());
                        }
                        else
                        {
                            currentMatch = serverName;

                            if(currentlySelectedPlayTab == MenuPlayTabs.Public)
                                MasterServer.RegisterHost(filterPlayMode.ToString(), serverName);

                            currentLobbyServerGUID = Network.player.guid;
                            FriendlistManager.Instance.me.friendActivity = FriendActivity.InLobby;

                            SystemTorchInteraction.Instance.PlayAll();
                        }
                    }
                    GUILayout.EndVertical();

                    GUILayout.EndHorizontal();

                    GUILayout.Space(8);

                    GUILayout.Label("Settings");
                    if (GUILayout.Button("Playmode: Full Draft"))
                    {
                        filterPlayMode = PlayMode.FullDraft;
                    }
                    canFriendsJoin = GUILayout.Toggle(canFriendsJoin, "Can friends join (no password required)?");
                    GUILayout.Space(8);

                    GUILayout.Label("Lobbies (for: " + filterPlayMode + ")");
                    if (GUILayout.Button("Refresh"))
                    {
                        RefreshHosts();
                    }

                    // Direct connect
                    GUILayout.BeginHorizontal();
                    textConnectIP = GUILayout.TextField(textConnectIP, 40);
                    textConnectPassword = GUILayout.TextField(textConnectPassword, 40);
                    if (GUILayout.Button("Connect", GUILayout.MinWidth(85)))
                    {
                        NetworkConnectionError error = Network.Connect(textConnectIP, serverPort, textConnectPassword);
                        if (error != NetworkConnectionError.NoError)
                        {
                            print("Direct connect error (IP): " + error.ToString() + ", trying to connect with given IP as GUID");
                            NetworkConnectionError error2 = Network.Connect(textConnectIP, textConnectPassword);
                            if (error != NetworkConnectionError.NoError)
                            {
                                print("Direct connect error (GUID): " + error.ToString() + ", no connection possible");
                            }
                            else
                                SystemTorchInteraction.Instance.PlayAll();
                        }
                        else
                            SystemTorchInteraction.Instance.PlayAll();
                    }
                    GUILayout.EndHorizontal();

                    if (hostsPlay == null)
                        RefreshHosts();

                    lobbiesScrollPos = GUILayout.BeginScrollView(lobbiesScrollPos);

                    if (hostsPlay != null)
                    {
                        if (hostsPlay.Length <= 0)
                            GUILayout.Label("No lobbies found!");

                        foreach (var host in hostsPlay)
                        {
                            GUILayout.BeginHorizontal();

                            GUILayout.Label(host.gameName + ", (Current players: " + host.connectedPlayers + ", Password protected: " + host.passwordProtected + ") " + host.gameType);
                            pw = GUILayout.TextField(pw, 30);

                            if (GUILayout.Button("Join"))
                            {
                                game = new Game();

                                inLobby = true;
                                currentMatch = host.gameName;

                                NetworkConnectionError n = Network.Connect(host, pw); // or overload with HostData
                                print("Connecting to server from lobbylist resulted in: " + n.ToString());

                                currentLobbyServerGUID = host.guid;
                                FriendlistManager.Instance.me.friendActivity = FriendActivity.InLobby;

                                if(n == NetworkConnectionError.NoError)
                                    SystemTorchInteraction.Instance.PlayAll();
                            }

                            GUILayout.EndHorizontal();
                        }
                    }

                    GUILayout.EndScrollView();
                }
                #endregion
                #region Connecting
                else if (Network.peerType == NetworkPeerType.Connecting)
                {
                    GUILayout.Label("Connecting...");
                }
                #endregion
                #region Connected
                else if (Network.peerType == NetworkPeerType.Client || Network.peerType == NetworkPeerType.Server)
                {
                    #region In-lobby
                    GUILayout.Label("Connected to game (" + currentMatch + ")");
                    GUILayout.Label(lobbyGameStartStateText);

                    GUILayout.BeginVertical();
                    GUILayout.Label("Connected players");
                    foreach (var player in game.allPlayers)
                    {
                        GUILayout.BeginHorizontal();

                        GUILayout.Label("   " + player);
                        if (Network.isServer && player != GameInformationManager.Instance.userProfileName)
                        {
                            if (GUILayout.Button("Kick"))
                            {
                                networkView.RPC("KickPlayer", RPCMode.Others, player);
                            }
                        }

                        GUILayout.EndHorizontal();

                    }
                    GUILayout.EndVertical();
                    GUILayout.Space(8);

                    #region Game-settings and lobby chat
                    GUILayout.BeginVertical();
                    GUILayout.Label("Game-settings");

                    if (Network.isServer)
                    {
                        bool old = cheatsEnabledGameSettings;
                        cheatsEnabledGameSettings = GUILayout.Toggle(cheatsEnabledGameSettings, "Enable cheats");

                        if (old != cheatsEnabledGameSettings)
                            networkView.RPC("EnableCheatsGameSettings", RPCMode.AllBuffered, cheatsEnabledGameSettings); // TODO: Check if this is performant
                    }
                    else
                    {
                        GUILayout.Label("Cheats enabled: " + cheatsEnabledGameSettings.ToString());
                    }

                    GUILayout.Label("Lobby chat");

                    GUILayout.Label("---");
                    lobbyChatScrollPos = GUILayout.BeginScrollView(lobbyChatScrollPos);
                    foreach (var chatmsg in chatMessages)
                    {
                        GUILayout.Label(chatmsg);
                    }
                    GUILayout.EndScrollView();
                    GUILayout.Label("---");

                    GUILayout.BeginHorizontal();
                    lobbyChatText = GUILayout.TextField(lobbyChatText, 100);

                    if (GUILayout.Button("Send", GUILayout.MaxWidth(75)))
                    {
                        if (CanWriteLobbyChatMessage())
                        {
                            networkView.RPC("WriteChatMessage", RPCMode.All, "[" + GameInformationManager.Instance.userProfileName + "] " + lobbyChatText);
                            chatMessageSpamCheckDeltaTimeCounter = 0;
                        }
                    }
                    GUILayout.EndHorizontal();

                    GUILayout.EndVertical();
                    #endregion

                    GUILayout.BeginHorizontal();

                    GUILayout.BeginVertical();
                    GUILayout.Label("Green");
                    foreach (var slot in game.greenSlots)
                    {
                        GUILayout.Label(slot.name);
                    }

                    GUILayout.Space(8);
                    if (GUILayout.Button("Join the Green"))
                    {
                        networkView.RPC("JoinSide", RPCMode.AllBuffered, true, GameInformationManager.Instance.userProfileName, Network.player);
                    }

                    GUILayout.EndVertical();

                    GUILayout.BeginVertical();
                    GUILayout.Label("Red");
                    foreach (var slot in game.redSlots)
                    {
                        GUILayout.Label(slot.name);
                    }

                    GUILayout.Space(8);
                    if (GUILayout.Button("Join the Red"))
                    {
                        networkView.RPC("JoinSide", RPCMode.AllBuffered, false, GameInformationManager.Instance.userProfileName, Network.player);
                    }

                    GUILayout.EndVertical();
                    GUILayout.EndHorizontal();

                    GUILayout.Space(8);

                    if (GUILayout.Button("Disconnect"))
                    {
                        if (Network.peerType == NetworkPeerType.Server)
                        {
                            networkView.RPC("KickAll", RPCMode.Others);
                            MasterServer.UnregisterHost();
                        }

                        DoDisconnect();
                    }
                    #endregion
                }
                #endregion

                if (Network.peerType == NetworkPeerType.Server)
                {
                    if (GUILayout.Button("Start game"))
                    {
                        FriendlistManager.Instance.me.friendActivity = FriendActivity.InGame;

                        MasterServer.UnregisterHost();
                        networkView.RPC("StartGame", RPCMode.All);
                    }
                }

                GUILayout.EndVertical();
                break;
            #endregion

            #region Watch
            case MenuTabs.Watch:
                GUILayout.BeginVertical();

                //List<HostData> hostsForWatch = new List<HostData>((IEnumerable<HostData>)hostsWatch.GetEnumerator());
                //hostsForWatch = (List<HostData>)(from element in hostsForWatch
                //                orderby element.connectedPlayers select element);

                if (currentlyWatchedLeague == null)
                {
                    GUILayout.Label("Leagues");

                    if (GUILayout.Button("Refresh"))
                    {
                        allCurrentLeagues = GetAllCurrentLeagues();
                    }
                    GUILayout.Space(8);

                    GUILayout.Label("Current leagues");

                    watchScrollPos = GUILayout.BeginScrollView(watchScrollPos);

                    if (allCurrentLeagues == null)
                        allCurrentLeagues = GetAllCurrentLeagues();

                    if (allCurrentLeagues != null)
                    {
                        foreach (var current in allCurrentLeagues)
                        {
                            string[] currentLeagueSplitted = current.Split('|');
                            try
                            {
                                if (currentLeagueSplitted.Length < 2)
                                    continue;

                                GUILayout.BeginHorizontal();
                                GUILayout.Label(currentLeagueSplitted[0] + "\n"
                                    + currentLeagueSplitted[1]);
                            }
                            catch (Exception ex)
                            {
                                Debug.LogException(ex);
                            }

                            if (GUILayout.Button("Go"))
                            {
                                currentlyWatchedLeague = currentLeagueSplitted;
                                RefreshHosts();
                            }


                            GUILayout.EndHorizontal();
                        }
                    }
                    else
                    {
                        GUILayout.Label("An error occured while requesting leagues.");
                    }

                    GUILayout.EndScrollView();
                }
                else
                {
                    GUILayout.Label("Currently watched league: " + currentlyWatchedLeague[0] + "\n" + currentlyWatchedLeague[1]);

                    if (GUILayout.Button("Back"))
                    {
                        currentlyWatchedLeague = null;
                    }

                    GUILayout.Space(8);

                    GUILayout.Label("Live league-matches:");
                    if (GUILayout.Button("Refresh"))
                    {
                        RefreshHosts();
                    }

                    if (hostsWatch == null)
                        RefreshHosts();

                    if (hostsWatch != null)
                    {
                        if (hostsWatch.Length <= 0)
                            GUILayout.Label("No live league-matches found!");

                        foreach (var host in hostsWatch)
                        {
                            GUILayout.BeginHorizontal();

                            GUILayout.Label(host.gameName + " (" + host.connectedPlayers + " players and watchers) " + host.gameType);
                            if (GUILayout.Button("Watch"))
                            {
                                inLobby = false;
                                currentMatch = host.gameName;

                                Network.Connect(host.guid, currentlyWatchedLeague[2]);

                                currentLobbyServerGUID = host.guid;
                                FriendlistManager.Instance.me.friendActivity = FriendActivity.Watching;

                                GameCreationManager.Instance.StartGame(game, new LocalGameSettings() { isLeagueMatch = true, leagueName = currentlyWatchedLeague[0], isSpectatorMode = true, matchName = currentMatch });                                
                            }

                            GUILayout.EndHorizontal();
                        }
                    }
                }
                
                GUILayout.EndVertical();
                break;
            #endregion

            #region Profile
            case MenuTabs.ProfileAndSettings:
                GUILayout.BeginVertical();

                GUILayout.Label("Username: " + GameInformationManager.Instance.userProfileName);
                GUILayout.Space(8);

                GUILayout.Label("Settings");

                QualityLevel quality = (QualityLevel)GameSettingsManager.Instance.playerSettings.quality;
                GUILayout.Label("Quality (current: " + quality.ToString() + ")");
                if (GUILayout.Button("Fastest"))
                {
                    GameSettingsManager.Instance.playerSettings.quality = 0;
                }
                if (GUILayout.Button("Fast"))
                {
                    GameSettingsManager.Instance.playerSettings.quality = 1;
                }
                if (GUILayout.Button("Simple"))
                {
                    GameSettingsManager.Instance.playerSettings.quality = 2;
                }
                if (GUILayout.Button("Good"))
                {
                    GameSettingsManager.Instance.playerSettings.quality = 3;
                }
                if (GUILayout.Button("Beautiful"))
                {
                    GameSettingsManager.Instance.playerSettings.quality = 4;
                }
                if (GUILayout.Button("Fantastic"))
                {
                    GameSettingsManager.Instance.playerSettings.quality = 5;
                }
                GUILayout.Space(5);

                GUILayout.Label("Audio volume (current: " + Math.Round(GameSettingsManager.Instance.playerSettings.audioVolume, 1) + ")");
                GameSettingsManager.Instance.playerSettings.audioVolume = GUILayout.HorizontalSlider(GameSettingsManager.Instance.playerSettings.audioVolume, 0, 1);

                GUILayout.Space(5);

                GameSettingsManager.Instance.playerSettings.isTutorialActivated = GUILayout.Toggle(GameSettingsManager.Instance.playerSettings.isTutorialActivated, "Activate the tutorial");

                GUILayout.Space(8);

                if (GUILayout.Button("Save and apply"))
                {
                    PlayerSettingsInfo.Save(GameSettingsManager.Instance.playerSettings);
                    GameSettingsManager.Instance.playerSettings.Apply();
                }

                GUILayout.EndVertical();
                break;
            #endregion

            #region Exit
            case MenuTabs.Exit:
                GUILayout.BeginVertical();

                GUILayout.Label("Do you really want to quit Bloodraider?");

                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Yes"))
                {
                    Application.Quit();

                    // TODO: Maybe try out if a double quit works?
                    //Application.Quit();
                }
                if (GUILayout.Button("No"))
                {
                    currentlySelectedTab = MenuTabs.News;
                }
                GUILayout.EndHorizontal();

                GUILayout.EndVertical();
                break;
            #endregion
        }

        GUILayout.EndArea();
        #endregion
    }

    IEnumerator StartGameCountdown()
    {
        string[] league = null;
        league = IsValidLeague(serverKey);
        if (league != null)
        {
            MasterServer.RegisterHost("Watch_" + league[0], serverName);
        }
        else
        {
            try
            {
                print("The league game \"" + currentMatch + "\" could not be created because the league key for \"" + league[0] + "\" is invalid.");
            }
            catch
            { }
        }

        lobbyGameStartStateText = "Starting game...";
        yield return new WaitForSeconds(1);
        lobbyGameStartStateText = "Starting game... 3";
        yield return new WaitForSeconds(1);
        lobbyGameStartStateText = "Starting game... 2";
        yield return new WaitForSeconds(1);
        lobbyGameStartStateText = "Starting game... 1";
        yield return new WaitForSeconds(1);
        lobbyGameStartStateText = "Go";

        if (Network.peerType == NetworkPeerType.Disconnected)
            yield break;
        
        Network.RemoveRPCsInGroup(1);
        GameCreationManager.Instance.StartGame(game, new LocalGameSettings() { isLeagueMatch = (league != null), leagueName = (league != null ? league[0] : ""), isSpectatorMode = false, matchName = currentMatch, areCheatsEnabled = cheatsEnabledGameSettings });
    }

    void OnApplicationQuit()
    {
        if (Network.peerType != NetworkPeerType.Disconnected)
        {
            DoDisconnect();
        }
    }

    void IsClientUpToDate()
    {
        try
        {
            using (WebClient w = new WebClient())
            {
                string newestVersion = w.DownloadString("http://dl.dropboxusercontent.com/u/89935882/Bloodraider%20MOBA/g_version.txt");
                if (newestVersion != GameInformationManager.version)
                {
                    loadingText = "Wrong version (Yours: " + GameInformationManager.version + ", Newest: " + newestVersion + "), client closing...";
                    Debug.LogError("Wrong version (Yours: " + GameInformationManager.version + ", Newest: " + newestVersion + "), client closing...");

                    //autoClose = true;
                    return;
                }

                return;
            }
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);

            //if (!Debug.isDebugBuild)
            //    throw ex;
            //else
            //    return;
        }
    }
    string[] GetAllCurrentLeagues()
    {
        try
        {
            using (WebClient w = new WebClient())
            {
                return w.DownloadString("http://dl.dropboxusercontent.com/u/89935882/Bloodraider%20MOBA/g_leagues.txt").Split('\n');
            }
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            return null;
        }
    }
    string[] IsValidLeague(string key)
    {
        string[] leagues = GetAllCurrentLeagues();
        if (leagues != null)
        {
            foreach (var league in leagues)
            {
                string[] lInfo = league.Split('|');
                if (lInfo[2] == key)
                    return lInfo;
            }
        }

        return null;
    }
    bool CanWriteLobbyChatMessage()
    {
        return chatMessageSpamCheckDeltaTimeCounter == -1 ? true : false;
    }

    void DoDisconnect()
    {
        serverKey = "<LeagueKey>";
        serverMaxConnectedPlayers = 11;

        if (Network.isClient)
        {
            networkView.RPC("RemoveFromSides", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
            networkView.RPC("RemovePlayer", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
        }
        if (Network.isServer)
        {
            MasterServer.UnregisterHost();
        }
        inLobby = false;

        Network.Disconnect(1000);
        SystemTorchInteraction.Instance.StopAll();
    }
    void RefreshHosts()
    {
        MasterServer.ClearHostList();

        if(refreshModePlay)
            MasterServer.RequestHostList(filterPlayMode.ToString());
        else
            MasterServer.RequestHostList("Watch_" + currentlyWatchedLeague[0]);
    }

    void OnServerInitialized()
    {
        chatMessages.Clear();
        IsClientUpToDate();

        if (inLobby)
        {
            networkView.RPC("SetPlayMode", RPCMode.AllBuffered, filterPlayMode.ToString());
            networkView.RPC("AddPlayer", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
        }
    }
    void OnConnectedToServer()
    {
        chatMessages.Clear();
        IsClientUpToDate();

        if(inLobby)
            networkView.RPC("AddPlayer", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
    }

    [RPC]
    void JoinSide(bool green, string name, NetworkPlayer p)
    {
        game.RemoveSlot(name);
        if (green)
            game.JoinGreen(name, p);
        else
            game.JoinRed(name, p);
    }
    [RPC]
    void RemoveFromSides(string name)
    {
        game.RemoveSlot(name);
    }
    [RPC]
    void AddPlayer(string name)
    {
        game.allPlayers.Add(name);
    }
    [RPC]
    void RemovePlayer(string name)
    {
        game.RemoveSlot(name);

        if(game.allPlayers.Contains(name))
            game.allPlayers.Remove(name);
    }
    [RPC]
    void StartGame()
    {
        if (game.currentSlot == null)
        {
            DoDisconnect();
            return;
        }

        StartCoroutine(StartGameCountdown());
    }
    [RPC]
    void SetPlayMode(string mode)
    {
        game.SetPlayMode(mode);
    }
    [RPC]
    void KickPlayer(string name)
    {
        if (GameInformationManager.Instance.userProfileName == name && Network.peerType == NetworkPeerType.Client)
            DoDisconnect();
    }
    [RPC]
    void KickAll()
    {
        DoDisconnect();
    }
    [RPC]
    void WriteChatMessage(string msg)
    {
        chatMessages.Add(msg);
    }
    [RPC]
    void EnableCheatsGameSettings(bool value)
    {
        cheatsEnabledGameSettings = value;
    }
}

[Serializable]
public enum MenuTabs
{
    News,
    Play,
    Watch,
    ProfileAndSettings,
    Exit
}
[Serializable]
public enum MenuPlayTabs
{
    League,
    Public
}

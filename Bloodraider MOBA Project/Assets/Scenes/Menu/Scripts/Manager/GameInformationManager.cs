﻿using UnityEngine;
using System.Collections;
using System;

public class GameInformationManager : MonoBehaviour
{
    public static GameInformationManager Instance;

    public const string version = "0.0.2", playerPrefsKeyName = "UserProfileInfo";
    public string userProfileName, userGuid;

    public GameInformationManager()
    {
        Instance = this;
    }

    void Awake()
    {
        if (PlayerPrefs.HasKey("UserProfileInfo"))
        {
            userProfileName = PlayerPrefs.GetString(playerPrefsKeyName).Split('$')[0];
            userGuid = PlayerPrefs.GetString(playerPrefsKeyName).Split('$')[1];

            if (GUIMenu.Instance != null)
                GUIMenu.Instance.isUsernameChosen = true;
        }
        else
        {
            if (Network.isClient || Network.isServer)
            {
                Debug.Log("You have no username, you wont be able to play while this is the case. Returning to menu...");
                GameManagementManager.Instance.DoDisconnect();

                return;
            }
            if (GUIMenu.Instance != null)
            {
                GUIMenu.Instance.isUsernameChosen = false;
                return;
            }
        }
    }

    public void SetUsernameAndGuid(string newusername)
    {
        userProfileName = newusername;
        userGuid = Guid.NewGuid().ToString();

        PlayerPrefs.SetString(playerPrefsKeyName, userProfileName + "$" + userGuid);
    }
}

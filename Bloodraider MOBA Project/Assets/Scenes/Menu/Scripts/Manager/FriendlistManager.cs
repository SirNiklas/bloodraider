﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.IO;
using System;
using System.Net.NetworkInformation;
using System.ComponentModel;

using UPnP;


public class FriendlistManager : MonoBehaviour
{
    public static FriendlistManager Instance;
    public IPEndPoint clientEndpoint, serverEndpoint;

    public string friendListServerIPLink;

    public Friend me;
    public List<Friend> friends, friendBlocklist;

    private UdpClient client;
    private BackgroundWorker receiveWorker;

    public void OnJoinLobby(bool canfriendsjoin, string guid, string pw)
    {
        me.canFriendsJoin = canfriendsjoin;
        me.currentLobbyGUID = guid;
        me.currentLobbyPassword = pw;
    }
    public void OnDisconnectFromLobby()
    {
        me.canFriendsJoin = false;
        me.currentLobbyGUID = "";
        me.currentLobbyPassword = "";
    }

    public void SendAddFriend(int raiderid)
    {
        WriteMessage(GetNetworkStringByFriendInstance("FRIEND_ADDME$" + raiderid, me));
    }
    public void SendRemoveFriend(int raiderid)
    {
        WriteMessage(GetNetworkStringByFriendInstance("FRIEND_REMOVEME$" + raiderid, me));
    }

    public void AddFriend(Friend friend)
    {
        if (GetFriendByRaiderId(friend.raiderId) == default(Friend))
            friends.Add(friend);

        GUIMenuSocials.Instance.PostFeedMessage("You added a friend \"" + friend.currentName + "\".");
    }
    public void RemoveFriend(Friend friend)
    {
        if(GetFriendByRaiderId(friend.raiderId) != default(Friend))
            friends.Remove(friend);

        GUIMenuSocials.Instance.PostFeedMessage("You removed a friend \"" + friend.currentName + "\".");
    }
    public void MoveFriendToBlockList(Friend friend)
    {
        RemoveFriend(friend);
        friendBlocklist.Add(friend);

        GUIMenuSocials.Instance.PostFeedMessage("You moved \"" + friend.currentName + "\" to your blocklist.");
    }
    public void RemoveFriendFromBlockList(Friend friend)
    {
        if (friendBlocklist.Contains(friend))
            friendBlocklist.Remove(friend);

        GUIMenuSocials.Instance.PostFeedMessage("You removed \"" + friend.currentName + "\" from the blocklist.");
    }

    void Awake()
    {
        Instance = this;

        //try
        //{
        //    if (NAT.Discover())
        //    {
        //        NAT.ForwardPort(34566, ProtocolType.Udp, "Bloodraider Friendlist Communication");
        //    }
        //}
        //catch (Exception ex)
        //{
        //    GUIMenuSocials.Instance.currentErrorMsg = "UPnP exception occured on portforwarding to friendlist server: " + ex.ToString();
        //}

        string serverIP;
        using (WebClient w = new WebClient())
        {
            serverIP = w.DownloadString(friendListServerIPLink);
        }

        serverEndpoint = new IPEndPoint(IPAddress.Parse(serverIP), 34566);

        clientEndpoint = new IPEndPoint(IPAddress.Any, 34567);
        client = new UdpClient(clientEndpoint);

        #region Load friendlist and own friend-instance
        #endregion

        receiveWorker = new BackgroundWorker();
        receiveWorker.WorkerSupportsCancellation = true;
        receiveWorker.DoWork += receiveWorker_DoWork;
        receiveWorker.RunWorkerAsync();

        if(me.raiderId == 0)
            WriteMessage("REGISTER");

        PingAndControlFriendlistServerSystems();
        InvokeRepeating("UpdateCurrentFriends", 1, 1);
    }

    void receiveWorker_DoWork(object sender, DoWorkEventArgs e)
    {
        ReceiveMessages();
    }

    void OnApplicationQuit()
    {
        Close();
    }
    void OnDestroy()
    {
        Close();
    }

    void Update()
    {
        me.currentName = GameInformationManager.Instance.userProfileName;
    }

    public void PingAndControlFriendlistServerSystems()
    {
        WriteMessage("PING");

        //PingFriendlistServer();
        //PingReply reply = PingFriendlistServer();
        //if (reply.Status == IPStatus.Success)
        //    GUIMenuSocials.Instance.currentFriendlistServerPing = (int)reply.RoundtripTime;

        GUIMenuSocials.Instance.currentErrorMsg = "Searching connection to friendlist server...";
        GUIMenuSocials.Instance.isFriendlistEnabled = false;
    }
    public void Close()
    {
        if (receiveWorker != null)
            receiveWorker.CancelAsync();
        if(client != null)
            client.Close();

        #region Save friendlist and own friend-instance

        #endregion    
    }
    void /*PingReply*/ PingFriendlistServer()
    {
        UnityEngine.Ping p = new UnityEngine.Ping(serverEndpoint.Address.ToString());
        while (!p.isDone)
        { }

        GUIMenuSocials.Instance.currentFriendlistServerPing = p.time;
        p.DestroyPing();

        //System.Net.NetworkInformation.Ping p = new System.Net.NetworkInformation.Ping();
        //return p.Send(FriendlistManager.Instance.serverEndpoint.Address.ToString(), 500);
    }
    //string GetPublicIP()
    //{
    //    string strHostName = System.Net.Dns.GetHostName();
    //    IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

    //    foreach (IPAddress ipAddress in ipEntry.AddressList)
    //    {
    //        if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
    //        {
    //            return ipAddress.ToString();
    //        }
    //    }

    //    return string.Empty;
    //}

    void ReceiveMessages()
    {
        while (true)
        {
            byte[] bytes = client.Receive(ref serverEndpoint);
            string msg = ASCIIEncoding.ASCII.GetString(bytes);

            #region Pong
            if (msg.StartsWith("PONG"))
            {
                GUIMenuSocials.Instance.currentErrorMsg = "";
                GUIMenuSocials.Instance.isFriendlistEnabled = true;
            }
            #endregion
            #region Set Raider-ID
            if (msg.StartsWith("SET_RAIDERID"))
            {
                int mynewraiderid = int.Parse(msg.Split('$')[1]);
                me.raiderId = mynewraiderid;

                print("You have a new Raider-ID by the friendlist server.");
            }
            #endregion
            #region Add me
            else if (msg.StartsWith("FRIEND_ADDME"))
            {
                Friend f = GetFriendInstanceByNetworkString(msg);
                Friend existing;

                if (!IsFriendInBlockList(f, out existing))
                {
                    if (IsFriendAlreadyInFriendlist(f, out existing))
                    {
                        existing = f;
                    }
                    else
                    {
                        AddFriend(f);
                        print("You added a new friend: " + f.currentName);
                    }
                }
            }
            else if (msg.StartsWith("FRIEND_REMOVEME"))
            {
                Friend f = GetFriendInstanceByNetworkString(msg);
                Friend existing;

                if (!IsFriendInBlockList(f, out existing))
                {
                    if (IsFriendAlreadyInFriendlist(f, out existing))
                    {
                        RemoveFriend(existing);
                        print("You removed " + f.currentName + " on his command.");
                    }
                    else
                    {
                        print(f.currentName + " sent a removeme message but is not in your friendlist?");
                    }
                }
            }
            #endregion
        }
    }

    void UpdateCurrentFriends()
    {
        if (!GUIMenuSocials.Instance.isFriendlistEnabled)
            return;

        foreach (var friend in friends)
        {
            WriteMessage(GetNetworkStringByFriendInstance("FRIEND_ADDME", friend));
        }
    }

    void WriteMessage(string msg)
    {
        try
        {
            byte[] bytes = ASCIIEncoding.ASCII.GetBytes(msg);
            int sentBytes = client.Send(bytes, bytes.Length, serverEndpoint);

            //print("Successfully wrote a message to the friendlist server on " + serverEndpoint.ToString() + " with " + sentBytes + " sent bytes.");
        }
        catch (SocketException)
        {
            PingAndControlFriendlistServerSystems();
        }
    }

    // TODO: (De)Serialize friendlists on awake/applicationquit

    string GetNetworkStringByFriendInstance(string tag, Friend f)
    {
        string n = tag;

        n += "$" + f.raiderId;
        n += "$" + f.currentName;
        n += "$" + f.friendActivity.ToString();
        n += "$" + f.currentLobbyGUID;
        n += "$" + f.currentLobbyPassword;

        return n;
    }
    Friend GetFriendInstanceByNetworkString(string msg)
    {
        string[] msgSplitted = msg.Split('$');
        Friend f = new Friend();

        f.raiderId = int.Parse(msgSplitted[1]);
        f.currentName = msgSplitted[2];
        f.friendActivity = (FriendActivity)Enum.Parse(typeof(FriendActivity), msgSplitted[3]);
        f.currentLobbyGUID = msgSplitted[4];
        f.currentLobbyPassword = msgSplitted[5];

        return f;
    }
    Friend GetFriendByRaiderId(int id)
    {
        return friends.Find((f) => { if (f.raiderId == id) return true; else return false; });
    }

    bool IsFriendAlreadyInFriendlist(Friend newfriend, out Friend duplicate)
    {
        foreach (var friend in friends)
        {
            if(friend.EqualsFriend(newfriend))
            {
                duplicate = friend;
                return true;
            }
        }
        
        duplicate = null;
        return false;
    }
    bool IsFriendInBlockList(Friend newfriend, out Friend duplicate)
    {
        foreach (var friend in friendBlocklist)
        {
            if(friend.EqualsFriend(newfriend))
            {
                duplicate = friend;
                return true;
            }
        }
        
        duplicate = null;
        return false;
    }
}

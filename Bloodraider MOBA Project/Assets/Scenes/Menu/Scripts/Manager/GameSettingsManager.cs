﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameSettingsManager : MonoBehaviour
{
    public static GameSettingsManager Instance;
    public PlayerSettingsInfo playerSettings;

    void Awake()
    {
        Instance = this;

        playerSettings = PlayerSettingsInfo.Load();
        playerSettings.Apply();
    }
}

[Serializable]
public class PlayerSettingsInfo
{
    public const string playerSettingsSavePath = @"save\playersettings.data";

    public int quality;
    public float audioVolume;
    public bool isTutorialActivated;

    public string selectAllMyUnitsKey, selectMyHeroKey, selectMyMinionsKey, incrementSelectedUnitInListKey, castSpellWithAllSelectedUnitsKey, centerSelectedHeroKey;

    public PlayerSettingsInfo()
    {
        quality = 3;
        audioVolume = 0.8f;

        selectAllMyUnitsKey = "g";
        selectMyMinionsKey = "f";
        selectMyHeroKey = "f1";
        incrementSelectedUnitInListKey = "tab";
        centerSelectedHeroKey = "h";
        castSpellWithAllSelectedUnitsKey = "left ctrl";
        isTutorialActivated = false;
    }

    public static PlayerSettingsInfo Load()
    {
        if (!File.Exists(playerSettingsSavePath))
            return new PlayerSettingsInfo();

        BinaryFormatter b = new BinaryFormatter();
        using (FileStream fs = File.Open(playerSettingsSavePath, FileMode.Open, FileAccess.Read))
        {
            return (PlayerSettingsInfo)b.Deserialize(fs);
        }
    }
    public static void Save(PlayerSettingsInfo playersettings)
    {
        BinaryFormatter b = new BinaryFormatter();
        using (FileStream fs = File.Create(playerSettingsSavePath))
        {
            b.Serialize(fs, playersettings);
        }
    }

    public void Apply()
    {
        #region Audio
        AudioSource[] audiosources = GameObject.FindObjectsOfType<AudioSource>();
        foreach (var audiosource in audiosources)
        {
            audiosource.volume = audioVolume;
        }
        #endregion

        #region Quality
        QualitySettings.SetQualityLevel(quality);
        #endregion
    }
}

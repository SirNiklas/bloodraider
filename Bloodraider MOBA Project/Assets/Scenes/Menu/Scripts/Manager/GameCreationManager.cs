﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameCreationManager : MonoBehaviour
{
    public static GameCreationManager Instance;

    public GameCreationManager()
    {
        Instance = this;
    }
    public void StartGame(Game gametocreate, LocalGameSettings locals)
    {
        if (gametocreate.greenSlots.Count > 5 || gametocreate.redSlots.Count > 5)
        {
            Debug.Log("Wrong playerslot count. Stopping game-creation.");
            return;
        }

        Game.SaveToTemp(gametocreate);
        LocalGameSettings.SaveToTemp(locals);

        Network.isMessageQueueRunning = false;

        if (Application.CanStreamedLevelBeLoaded("Arena"))
            Application.LoadLevel("Arena");
        else
            print("Level \"Arena\" can not be loaded.");
    }
}

[Serializable]
public class LocalGameSettings
{
    public bool isSpectatorMode, isLeagueMatch, areCheatsEnabled;
    public string leagueName, matchName;

    public static LocalGameSettings LoadFromTemp()
    {
        BinaryFormatter b = new BinaryFormatter();
        using (FileStream fs = File.Open(@"temp\current_settings.dat", FileMode.Open))
        {
            return (LocalGameSettings)b.Deserialize(fs);
        }
    }
    public static void SaveToTemp(LocalGameSettings game)
    {
        BinaryFormatter b = new BinaryFormatter();
        using (FileStream fs = File.Open(@"temp\current_settings.dat", FileMode.Create))
        {
            b.Serialize(fs, game);
        }
    }
}
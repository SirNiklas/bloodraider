﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SystemTorchInteraction : MonoBehaviour
{
    public static SystemTorchInteraction Instance;

    public Light torchLight1, torchLight2;
    public List<ParticleEmitter> torchParticleEmitter;

    void Awake()
    {
        Instance = this;
        StopAll();
    }

    public void PlayAll()
    {
        torchLight1.enabled = true;
        torchLight2.enabled = true;
        foreach (var particle in torchParticleEmitter)
        {
            particle.emit = true;
        }
    }
    public void StopAll()
    {
        torchLight1.enabled = false;
        torchLight2.enabled = false;
        foreach (var particle in torchParticleEmitter)
        {
            particle.emit = false;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class LaneEdgeCreepPositioner : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Unit"))
        {
            LaneCreepUnit laneCreep = other.GetComponent<LaneCreepUnit>();

            if (laneCreep != null)
            {
                laneCreep.isAfterLaneEdge = true;
                laneCreep.Go();
            }
        }
    }
}

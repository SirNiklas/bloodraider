﻿using UnityEngine;
using System.Collections;

public class GUIStatusPopup : MonoBehaviour
{
    public static GUIStatusPopup Instance;

    public Rect popupBoxRect;
    public Texture2D popupBoxTexture;
    public string currentText;
    public float showTime;
    public bool isPopupShown;

    public void ShowPopup(string text)
    {
        if (isPopupShown)
            return;

        currentText = text;
        StartCoroutine(DoPopupRoutine());
    }
    private IEnumerator DoPopupRoutine()
    {
        isPopupShown = true;
        yield return new WaitForSeconds(showTime);
        isPopupShown = false;
    }

    void Awake()
    {
        Instance = this;
        popupBoxRect = new Rect(Screen.width / 2 - 150, 250, 300, 150);
    }

    void OnGUI()
    {
        if (!isPopupShown)
            return;

        GUI.Box(popupBoxRect, popupBoxTexture);
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(popupBoxRect));
        GUILayout.Label(currentText);
        GUILayout.EndArea();
    }
}

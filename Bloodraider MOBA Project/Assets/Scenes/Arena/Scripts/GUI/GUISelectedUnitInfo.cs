﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUISelectedUnitInfo : MonoBehaviour
{
    public static GUISelectedUnitInfo Instance;

    public Rect infoAttributesRect, infoHealthBarRect, infoManaBarRect, infoCurrentHealthBarRect, infoCurrentManaBarRect, infoBuffsRect, infoUnitInformationsRect, infoUnitPortraitIconRect, infoSpellsRect, infoSpellHoverRect, infoInventoryRect;
    public Texture2D emptyInventorySlotIcon;
    public GUIStyle hoverInfoBoxStyle;

    private float maxBarLength;
    private Vector2 attributesScrollPos;

    void Awake()
    {
        Instance = this;

        maxBarLength = Screen.width / 2;

        infoAttributesRect = new Rect(150, Screen.height - 240, 180, 235);
        infoBuffsRect = new Rect(340, Screen.height - 265, 480, 35); // Horizontal scrollbar?
        infoUnitInformationsRect = new Rect(340, Screen.height - 300, 280, 35);
        infoUnitPortraitIconRect = new Rect(10, Screen.height - 240, 130, 235);
        infoHealthBarRect = new Rect(340, Screen.height - 240, maxBarLength, 35);
        infoManaBarRect = new Rect(340, Screen.height - 205, maxBarLength, 23);
        infoSpellsRect = new Rect(340, Screen.height - 175, maxBarLength, 170);
        infoSpellHoverRect = new Rect(340, Screen.height - 478, 200, 298);
        infoInventoryRect = new Rect(340 + maxBarLength, Screen.height - 175, (Screen.width - maxBarLength) - 340, 170);
    }

    void Update()
    {
        BaseUnit s = SystemSelectionMode.Instance.GetSelectedUnit();

        if (s == null)
            return;
        if (GameManagementManager.Instance.state == GameState.HeroSelection)
            return;
        if (GameManagementManager.Instance.state == GameState.Ending)
            return;

        float currentHealth = maxBarLength - ((((float)s.totalMaxHealth - (float)s.attributes[AttributeType.CurrentHealth]) / s.totalMaxHealth) * maxBarLength);
        float currentMana = maxBarLength - ((((float)s.totalMaxMana - (float)s.attributes[AttributeType.CurrentMana]) / s.totalMaxMana) * maxBarLength);

        infoCurrentHealthBarRect = new Rect(340, Screen.height - 240, currentHealth, 35);
        infoCurrentManaBarRect = new Rect(340, Screen.height - 205, currentMana, 23);
    }

    void OnGUI()
    {
        if (SystemSelectionMode.Instance.selectedUnits == null)
            return;
        if (GameManagementManager.Instance.state == GameState.HeroSelection)
            return;

        BaseUnit s = SystemSelectionMode.Instance.GetSelectedUnit();
        List<BaseUnit> all = SystemSelectionMode.Instance.selectedUnits;

        GUI.depth = -200;

        if (s != null)
        {
            GUI.DrawTexture(infoUnitPortraitIconRect, s.portraitIcon);

            #region Attributes
            GUI.Box(infoAttributesRect, "Info");
            GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(infoAttributesRect));

            attributesScrollPos = GUILayout.BeginScrollView(attributesScrollPos);
            GUILayout.BeginVertical();

            if (all != null)
            {
                if (all.Count > 1)
                {
                    GUILayout.Label("Currently selected:");
                    GUILayout.Label(s.unitName);
                    GUILayout.Space(5);

                    GUILayout.Label("Units in selection:");
                    GUILayout.Space(5);

                    foreach (var unit in all)
                    {
                        GUILayout.Label(unit.unitName + (unit == s ? " (current)" : ""));
                    }
                }
                else if(all.Count == 1)
                {
                    if (s is HeroUnit)
                        GUILayout.Label("Level: " + (s as HeroUnit).level.currentLevel + " (" + (s as HeroUnit).level.currentXp + "/ " + (s as HeroUnit).level.neededXp + " Xp)");

                    GUILayout.Label("Damage: " + s.attributes[AttributeType.BaseDamage] + " + " + s.attributes[AttributeType.BonusDamage]);

                    GUILayout.Label("Primary attribute: " + s.primaryAttribute.ToString());

                    string strGainText = " (+" + (s is HeroUnit ? ((HeroUnit)s).strengthGainOnLevelUp.ToString() + ")" : "");
                    GUILayout.Label("Strength: " + s.attributes[AttributeType.BaseStrength] + " + " + s.attributes[AttributeType.BonusStrength] + strGainText);
                    string agiGainText = " (+" + (s is HeroUnit ? ((HeroUnit)s).agilityGainOnLevelUp.ToString() + ")" : "");
                    GUILayout.Label("Agility: " + s.attributes[AttributeType.BaseAgility] + " + " + s.attributes[AttributeType.BonusAgility] + agiGainText);
                    string intGainText = " (+" + (s is HeroUnit ? ((HeroUnit)s).intelligenceGainOnLevelUp.ToString() + ")" : "");
                    GUILayout.Label("Intelligence: " + s.attributes[AttributeType.BaseIntelligence] + " + " + s.attributes[AttributeType.BonusIntelligence] + intGainText);

                    GUILayout.Label("State: " + s.state.ToString());
                    GUILayout.Label("Armor: " + s.attributes[AttributeType.BaseArmor] + " + " + s.attributes[AttributeType.BonusArmor] + " (" + s.totalArmorPercentage + "%)");
                    GUILayout.Label("Magic Resistance: " + s.attributes[AttributeType.BaseMagicResistance] + " + " + s.attributes[AttributeType.BonusMagicResistance] + " (" + s.totalMagicResistance.ToString() + "%)");

                    GUILayout.Label("Movementspeed: " + s.attributes[AttributeType.MovementSpeed]);
                    GUILayout.Label("Attackspeed: " + s.attributes[AttributeType.BaseAttackSpeed] + " + " + s.attributes[AttributeType.BonusAttackSpeed] + " (" + s.attackTime.ToString() + ")");

                }
            }

            GUILayout.EndVertical();
            GUILayout.EndScrollView();
            GUILayout.EndArea();
            #endregion

            #region Bars
            string minioninfo = "";
            if (s is MinionUnit)
            {
                if ((s as MinionUnit).hasMinionLivetime)
                {
                    minioninfo = ", Duration: " + Mathf.RoundToInt((s as MinionUnit).currentMinionLivetime) + "/" + (s as MinionUnit).minionLivetime;
                }
            }

            GUI.Label(infoUnitInformationsRect, s.unitName + " (" + s.side.ToString() + minioninfo + ")" + (s is HeroUnit ? " Usable levelpoints: " + (s as HeroUnit).usableLevelSpellPoints : ""));

            if (!s.isDead)
            {
                GUI.Box(infoCurrentHealthBarRect, "", GUIManager.Instance.enemyHealthBarGUIStyle);
                GUI.Box(infoHealthBarRect, "Health (" + s.attributes[AttributeType.CurrentHealth] + "/" + s.totalMaxHealth + ") +" + s.totalHealthRegeneration);

                if (s.totalMaxMana != 0)
                {
                    GUI.Box(infoCurrentManaBarRect, "", GUIManager.Instance.manaBarGUIStyle);
                    GUI.Box(infoManaBarRect, "Mana (" + s.attributes[AttributeType.CurrentMana] + "/" + s.totalMaxMana + ") +" + s.totalManaRegeneration);
                }
            }
            else
            {
                GUI.Label(infoHealthBarRect, "Dead" + (s is HeroUnit ? ", " + (s as HeroUnit).baseRespawnTime + (s as HeroUnit).bonusRespawnTime + " respawn-time" : ""));
            }

            GUILayout.BeginArea(infoBuffsRect);
            GUILayout.BeginHorizontal();

            GUILayout.Label("Buffs:");
            foreach (var buff in s.buffs)
            {
                if (!buff.isVisible)
                    continue;

                GUILayout.Label(", ");
                GUILayout.Label(buff.buffName + " (" + buff.buffDescription + (buff.hasLivetime ? ", " + buff.currentLivetime + "/" + buff.livetime : "") + ")");
            }

            GUILayout.FlexibleSpace();

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
            #endregion

            #region Spells
            GUI.Box(infoSpellsRect, "Spells");
            GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(infoSpellsRect));

            GUILayout.BeginHorizontal();
            foreach (var spell in s.spells)
            {
                GUILayout.BeginVertical();

                GUILayout.BeginHorizontal();
                GUI.enabled = spell.currentCooldown == 0 && spell.isUsable && spell.target != SpellTarget.Passive && spell.target != SpellTarget.Aura && spell.manaCost.GetCurrentValue(spell.currentLevel) <= s.attributes[AttributeType.CurrentMana] && spell.currentLevel > 0;
                if (GUILayout.Button(new GUIContent((spell.currentCooldown == 0 ? "" : Mathf.RoundToInt(spell.currentCooldown).ToString()), spell.icon, spell.spellName)))
                {
                    if (GameManagementManager.Instance.playerHeroUnit.unitsUnderControl.Contains(s))
                    {
                        SystemSelectionMode.Instance.CastSpell(s, spell);
                    }
                    else
                    {
                        // TODO: Set a screen message that spells from units not under the players control cant be casted
                    }
                }
                GUI.enabled = true;

                if (s is HeroUnit)
                {
                    HeroUnit sHero = s as HeroUnit;
                    if (sHero.usableLevelSpellPoints > 0)
                    {
                        bool ultCanLevel = true;
                        if (spell.isUltimate)
                        {
                            if (sHero.level.currentLevel < 6)
                                ultCanLevel = false;
                        }

                        if (spell.currentLevel < spell.maxLevel)
                        {
                            GUI.enabled = ultCanLevel && GameManagementManager.Instance.playerHeroUnit.unitsUnderControl.Contains(sHero);
                            if (GUILayout.Button("Level Up"))
                            {
                                sHero.LevelUpSpell(spell);
                            }
                            GUI.enabled = true;
                        }
                    }
                }
                GUILayout.EndHorizontal();
                GUILayout.Label("Spell-level: " + spell.currentLevel + "/" + spell.maxLevel);

                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();

            GUILayout.EndArea();
            #endregion

            #region Inventory
            GUI.Box(infoInventoryRect, "Inventory");

            GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(infoInventoryRect));
            GUILayout.BeginVertical();

            if (s.isInventoryEnabled)
            {
                GUILayout.BeginHorizontal();

                GUILayout.Label("Gold: " + s.inventory.gold);
                if (GUILayout.Button("Shop"))
                {
                    GUIShop.Instance.isShopOpened = !GUIShop.Instance.isShopOpened;
                }

                GUILayout.EndHorizontal();

                #region Itemslots
                GUILayout.BeginHorizontal();

                Item i0 = s.inventory.GetItemReferenceBySlotIndex(0);
                GUI.enabled = i0 != null && i0.spellInstance != null && i0.spellInstance.currentCooldown == 0 && i0.spellInstance.isUsable;
                if (GUILayout.Button(i0 != null ? new GUIContent((i0.spellInstance != null ? Mathf.RoundToInt(i0.spellInstance.currentCooldown).ToString() : ""), i0.icon, i0.itemName) : new GUIContent("", emptyInventorySlotIcon, "")))
                {
                    if (i0.spellInstance != null)
                    {
                        SystemSelectionMode.Instance.CastSpell(s, i0.spellInstance);
                    }
                }
                GUI.enabled = true;

                Item i1 = s.inventory.GetItemReferenceBySlotIndex(1);
                GUI.enabled = i1 != null && i1.spellInstance != null && i1.spellInstance.currentCooldown == 0 && i1.spellInstance.isUsable;
                if (GUILayout.Button(i1 != null ? new GUIContent((i1.spellInstance != null ? Mathf.RoundToInt(i1.spellInstance.currentCooldown).ToString() : ""), i1.icon, i1.itemName) : new GUIContent("", emptyInventorySlotIcon, "")))
                {
                    if (i1.spellInstance != null)
                    {
                        SystemSelectionMode.Instance.CastSpell(s, i1.spellInstance);
                    }
                }
                GUI.enabled = true;

                Item i2 = s.inventory.GetItemReferenceBySlotIndex(2);
                GUI.enabled = (i2 != null && i2.spellInstance != null && i2.spellInstance.currentCooldown == 0 && i2.spellInstance.isUsable);
                if (GUILayout.Button(i2 != null ? new GUIContent((i2.spellInstance != null ? Mathf.RoundToInt(i2.spellInstance.currentCooldown).ToString() : ""), i2.icon, i2.itemName) : new GUIContent("", emptyInventorySlotIcon, "")))
                {
                    if (i1.spellInstance != null)
                    {
                        SystemSelectionMode.Instance.CastSpell(s, i2.spellInstance);
                    }
                }
                GUI.enabled = true;

                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();

                Item i3 = s.inventory.GetItemReferenceBySlotIndex(3);
                GUI.enabled = i3 != null && i3.spellInstance != null && i3.spellInstance.currentCooldown == 0 && i3.spellInstance.isUsable;
                if (GUILayout.Button(i3 != null ? new GUIContent((i3.spellInstance != null ? Mathf.RoundToInt(i3.spellInstance.currentCooldown).ToString() : ""), i3.icon, i3.itemName) : new GUIContent("", emptyInventorySlotIcon, "")))
                {
                    if (i3.spellInstance != null)
                    {
                        SystemSelectionMode.Instance.CastSpell(s, i3.spellInstance);
                    }
                }
                GUI.enabled = true;

                Item i4 = s.inventory.GetItemReferenceBySlotIndex(4);
                GUI.enabled = i4 != null && i4.spellInstance != null && i4.spellInstance.currentCooldown == 0 && i4.spellInstance.isUsable;
                if (GUILayout.Button(i4 != null ? new GUIContent((i4.spellInstance != null ? Mathf.RoundToInt(i4.spellInstance.currentCooldown).ToString() : ""), i4.icon, i4.itemName) : new GUIContent("", emptyInventorySlotIcon, "")))
                {
                    if (i4.spellInstance != null)
                    {
                        SystemSelectionMode.Instance.CastSpell(s, i4.spellInstance);
                    }
                }
                GUI.enabled = true;

                Item i5 = s.inventory.GetItemReferenceBySlotIndex(5);
                GUI.enabled = i5 != null && i5.spellInstance != null && i5.spellInstance.currentCooldown == 0 && i5.spellInstance.isUsable;
                if (GUILayout.Button(i5 != null ? new GUIContent((i5.spellInstance != null ? Mathf.RoundToInt(i5.spellInstance.currentCooldown).ToString() : ""), i5.icon, i5.itemName) : new GUIContent("", emptyInventorySlotIcon, "")))
                {
                    if (i5.spellInstance != null)
                    {
                        SystemSelectionMode.Instance.CastSpell(s, i5.spellInstance);
                    }
                }
                GUI.enabled = true;

                GUILayout.EndHorizontal();
                #endregion

            }

            GUILayout.EndVertical();
            GUILayout.EndArea();
            #endregion

            #region Hoverinfo
            #region
            string hoverinfo = "";

            Spell hoveredSpell = s.spells.Find((spell) => { if (spell.spellName == GUI.tooltip) return true; else return false; });
            if (hoveredSpell != null)
            {
                hoverinfo = hoveredSpell.spellName + "\n____\n\n" + hoveredSpell.spellDescription + "\n" + hoveredSpell.GetAllSpellPropertyInformation() + "\nCooldown: " + hoveredSpell.cooldown.ToString() + " Mana: " + hoveredSpell.manaCost.ToString();
            }

            if (hoveredSpell == null)
            {
                foreach (var itemslot in s.inventory.itemSlots)
                {
                    if (itemslot == null)
                        continue;
                    if (itemslot.itemName == GUI.tooltip)
                        hoverinfo = itemslot.itemName + "\n" + itemslot.itemDescription + "\n" + itemslot.itemStatsDescription + "\nGoldworth: " + itemslot.goldworth + " " + (itemslot.isHalfPriceSet == false ? "(Half price in " + Mathf.RoundToInt(Item.itemHalfPriceChangeTimer - itemslot.currentHalfPriceTimer) + " secs.)" : "") + ")\n---" + (itemslot.spellInstance != null ? itemslot.spellInstance.spellName + "\n" + itemslot.spellInstance.spellDescription + "\n" + itemslot.spellInstance.GetAllSpellPropertyInformation() : "");
                }
            }
            #endregion

            infoSpellHoverRect.height = hoverinfo.Length;
            infoSpellHoverRect.yMin = (Screen.height - infoSpellsRect.height) - infoSpellHoverRect.height;

            if (hoverinfo != "")
            {
                GUI.Box(infoSpellHoverRect, "", hoverInfoBoxStyle);
                GUI.Label(GUIManager.Instance.GetImprovedRect(infoSpellHoverRect), hoverinfo);
            }
        }
        GUI.tooltip = "";
        #endregion
    }
}

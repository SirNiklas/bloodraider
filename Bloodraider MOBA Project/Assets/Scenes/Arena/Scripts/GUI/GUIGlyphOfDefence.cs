﻿using UnityEngine;
using System.Collections;

public class GUIGlyphOfDefence : MonoBehaviour
{
    public static GUIGlyphOfDefence Instance;

    public Rect glyphOfDefenceButtonBox;
    public Texture2D glyphOfDefenceButtonTexture;

    void Awake()
    {
        Instance = this;
        glyphOfDefenceButtonBox = new Rect(Screen.width - 180, 0, 180, 55);
    }

    void OnGUI()
    {
        if (GameManagementManager.Instance.state == GameState.Beginning || GameManagementManager.Instance.state == GameState.HeroSelection)
            return;

        GUI.enabled = !SystemGlyphOfDefence.Instance.isOnCooldown;
        if (GUI.Button(glyphOfDefenceButtonBox, new GUIContent(SystemGlyphOfDefence.Instance.isOnCooldown ? "Cooldown: " + Mathf.RoundToInt(SystemGlyphOfDefence.Instance.currentCooldown).ToString() : "Activate Glyph of Defence", glyphOfDefenceButtonTexture)))
        {
            SystemGlyphOfDefence.Instance.ActivateGlyph();
        }
        GUI.enabled = true;
    }
}

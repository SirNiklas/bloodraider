﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GUIShop : MonoBehaviour
{
    public static GUIShop Instance;

    public List<ItemShopInfo> items;
    public bool isShopOpened;
    public Rect shopRect;

    [NonSerialized]
    public ItemShopInfo currentlySelectedItem;
    public bool isBasicsTab = true;
    public ItemType chosenItemTypeTab;

    private Vector2 itemsScrollPos;

    void Awake()
    {
        Instance = this;

        shopRect = new Rect(Screen.width - 385, Screen.height / 2 - 260, 385, 520);
        foreach (var item in items)
        {
            item.Initialize();
        }
    }

    void OnGUI()
    {
        if (!isShopOpened)
            return;

        GUI.depth = -203;

        GUI.Box(shopRect, "Shop");
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(shopRect));
        GUILayout.BeginVertical();

        if (GUILayout.Button("Close"))
        {
            isShopOpened = false;
        }

        #region Basics-tab
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Basics"))
        {
            chosenItemTypeTab = ItemType.Consumables;
            isBasicsTab = true;
        }
        if (GUILayout.Button("Upgrades"))
        {
            chosenItemTypeTab = ItemType.Common;
            isBasicsTab = false;
        }
        GUILayout.EndHorizontal();
        #endregion

        #region Itemtypes-tab
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("All"))
        {
            chosenItemTypeTab = ItemType.All;
        }

        if (isBasicsTab)
        {
            if (GUILayout.Button("Consumables"))
            {
                chosenItemTypeTab = ItemType.Consumables;
            }
            if (GUILayout.Button("Attributes"))
            {
                chosenItemTypeTab = ItemType.Attributes;
            }
            if (GUILayout.Button("Armaments"))
            {
                chosenItemTypeTab = ItemType.Armaments;
            }
            if (GUILayout.Button("Arcane"))
            {
                chosenItemTypeTab = ItemType.Arcane;
            }
        }
        else
        {
            if (GUILayout.Button("Common"))
            {
                chosenItemTypeTab = ItemType.Common;
            }

            GUILayout.Label("Not yet finished (more item-types coming soon)");
        }
        GUILayout.EndHorizontal();
        #endregion

        GUILayout.Label((isBasicsTab ? "Basic" : "Upgradeable") + " items of type " + chosenItemTypeTab.ToString());

        itemsScrollPos = GUILayout.BeginScrollView(itemsScrollPos);
        foreach (var item in items)
        {
            if (item.isBasic != isBasicsTab)
                continue;

            if (chosenItemTypeTab != ItemType.All)
            {
                if (item.itemType != chosenItemTypeTab)
                    continue;
            }

            GUILayout.BeginHorizontal();
            GUILayout.Label(item.itemInstance.icon);
            if (GUILayout.Button(item.itemInstance.itemName))
            {
                currentlySelectedItem = item;
            }

            BaseUnit selectedUnit = SystemSelectionMode.Instance.GetSelectedUnit();
            GUI.enabled = (selectedUnit != null ? selectedUnit.inventory.gold >= item.price : false) && (selectedUnit != null ? selectedUnit.isBuyingEnabled : false) && (GameManagementManager.Instance.playerHeroUnit.unitsUnderControl.Contains(selectedUnit));
            if (GUILayout.Button("Buy"))
            {
                BaseUnit unit = SystemSelectionMode.Instance.GetSelectedUnit();
                {
                    if (unit.inventory.GetFreeSlot() != -1)
                    {
                        unit.UpdateInventorySlot(item.itemResourceName, unit.inventory.GetFreeSlot());
                        unit.inventory.gold -= item.price;
                    }
                }
            }
            GUI.enabled = true;

            GUILayout.EndHorizontal();
        }
        GUILayout.EndScrollView();

        GUILayout.FlexibleSpace();

        #region Currently selected item info
        string name = currentlySelectedItem != null ? currentlySelectedItem.itemInstance.itemName : "";
        string description = currentlySelectedItem != null ? currentlySelectedItem.itemInstance.itemDescription : "";
        string statsDescription = currentlySelectedItem != null ? currentlySelectedItem.itemInstance.itemStatsDescription : "";
        string spellinfo = "";
        if (currentlySelectedItem != null)
            spellinfo = (currentlySelectedItem.itemInstance.spellInstance != null ? currentlySelectedItem.itemInstance.spellInstance.spellName + "\n" + currentlySelectedItem.itemInstance.spellInstance.spellDescription + "\n" + currentlySelectedItem.itemInstance.spellInstance.GetAllSpellPropertyInformation() : "No spell contained");

        GUILayout.Label("Currently selected item:");
        GUILayout.Label(name);
        GUILayout.Label(description);
        GUILayout.Label(statsDescription);
        GUILayout.Space(5);

        // The spellInstance is null because the item is not instantiated/added to an inventory
        //GUILayout.Label("Spell:");
        //GUILayout.Label(spellinfo);

        if (GUILayout.Button("Deselect"))
        {
            currentlySelectedItem = null;
        }
        #endregion

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
}

[Serializable]
public class ItemShopInfo
{
    public string itemResourceName;
    public int price;
    public bool isBasic;
    public ItemType itemType;

    [HideInInspector]
    public Item itemInstance;

    public void Initialize()
    {
        itemInstance = Item.GetItem(itemResourceName);
    }
}
[Serializable]
public enum ItemType
{
    All,

    Consumables,
    Attributes,
    Armaments,
    Arcane,

    Common,
    Support,
    Caster,
    Weapons,
    Armor,
    Artifacts
}

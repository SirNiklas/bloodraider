﻿using UnityEngine;
using System.Collections;

public class GUIHeroSelection : MonoBehaviour
{
    public static GUIHeroSelection Instance;

    public Rect heroScrollSelectionRect;
    private Vector2 heroScrollSelectionPos;

    void Awake()
    {
        Instance = this;
        heroScrollSelectionRect = new Rect(10, 85, Screen.width - 20, Screen.height / 2);
    }

    void OnGUI()
    {
        if (GameManagementManager.Instance.state != GameState.HeroSelection)
            return;
        if (GameManagementManager.Instance.currentGameSettings.isSpectatorMode)
            return;

        GUI.Box(heroScrollSelectionRect, "Hero Selection");
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(heroScrollSelectionRect));
        heroScrollSelectionPos = GUILayout.BeginScrollView(heroScrollSelectionPos);
        GUILayout.BeginHorizontal();

        foreach (var hero in GameManagementManager.Instance.heroes)
        {
            GUILayout.Label(hero.texture);
            GUILayout.Label(hero.name + "\n" + hero.types);

            GUILayout.Space(80);

            GUI.enabled = (GameManagementManager.Instance.playerHeroUnit == null);
            if (GUILayout.Button("Pick"))
            {
                PickHero(hero.name, false);
            }
            GUI.enabled = true;
        }

        GUILayout.EndHorizontal();
        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }

    public void PickHero(string hero, bool random)
    {
        GameManagementManager.Instance.currentGame.currentSlot.hero = hero;
        GameManagementManager.Instance.playerHeroUnit = GameManagementManager.Instance.SpawnHero(GameManagementManager.Instance.currentGame.currentSlot.hero, (GameManagementManager.Instance.currentGame.currentSlot.side == UnitSide.Green ? GameManagementManager.Instance.greenSpawn : GameManagementManager.Instance.redSpawn).position).GetComponent<HeroUnit>();
        GameManagementManager.Instance.playerHeroUnit.side = GameManagementManager.Instance.currentGame.currentSlot.side;
        GameManagementManager.Instance.playerHeroUnit.playerSlot = GameManagementManager.Instance.currentGame.currentSlot;
        GameManagementManager.Instance.playerHeroUnit.inventory.gold = random ? 850 : 625;
        GameManagementManager.Instance.playerHeroUnit.Initialize();

        SystemFogOfWar.Instance.Initialize();
    }
    public void PickRandomHero()
    {
        int random = Random.Range(0, GameManagementManager.Instance.heroes.Count - 1);
        PickHero(GameManagementManager.Instance.heroes[random].name, true);
    }
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class GUITopInfoBar : MonoBehaviour
{
    public Rect topInfoTimeRect, topInfoGreenPlayersRect, topInfoRedPlayersRect, topInfoSelectedPlayerSlotRect, topInfoLeftStatsBarRect, statsInfoCombatLogRect, statsInfoTeamObserverRect;

    public Vector2 infoBoxScrollPos;
    public bool isInfoShown;
    public PlayerSlot selectedInfoPlayerSlot;
    public int selectedInfoPlayerSlotPing;

    public bool isCombatLogOpened, isTeamObserverOpened;

    private int teamObserverGridSelectedItem;

    void Awake()
    {
        topInfoTimeRect = new Rect(Screen.width / 2 - 60, 0, 120, 55);
        topInfoGreenPlayersRect = new Rect(Screen.width / 2 - 500, 0, 440, 55);
        topInfoRedPlayersRect = new Rect(Screen.width / 2 + 60, 0, 500, 55);
        topInfoSelectedPlayerSlotRect = new Rect(Screen.width / 2 - 80, 62, 160, 140);
        topInfoLeftStatsBarRect = new Rect(Screen.width / 2 - 680, 0, 175, 55);

        statsInfoCombatLogRect = new Rect(80, 110, 550, 300);
        statsInfoTeamObserverRect = new Rect(110, 160, 500, 400);
    }

    void OnGUI()
    {
        #region Time
        GUI.Box(topInfoTimeRect, "");
        GUI.Label(topInfoTimeRect, GameManagementManager.Instance.state + "\n" + Mathf.RoundToInt(GameManagementManager.Instance.minutes) + ":" + Mathf.RoundToInt(GameManagementManager.Instance.seconds));
        #endregion
        #region Green Players
        GUI.Box(topInfoGreenPlayersRect, "");
        GUILayout.BeginArea(topInfoGreenPlayersRect);
        GUILayout.BeginHorizontal();

        foreach (var player in GameManagementManager.Instance.currentGame.greenSlots)
        {
            GUILayout.BeginVertical();
            
            GUILayout.Label(player.name);
            if (GUILayout.Button("Info"))
            {
                selectedInfoPlayerSlot = player;
                isInfoShown = !isInfoShown;
            }

            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
        }

        GUILayout.EndHorizontal();
        GUILayout.EndArea();
        #endregion
        #region Red Players
        GUI.Box(topInfoRedPlayersRect, "");
        GUILayout.BeginArea(topInfoRedPlayersRect);
        GUILayout.BeginHorizontal();

        foreach (var player in GameManagementManager.Instance.currentGame.redSlots)
        {
            GUILayout.BeginVertical();

            GUILayout.Label(player.name);
            if (GUILayout.Button("Info"))
            {
                selectedInfoPlayerSlot = player;
                isInfoShown = !isInfoShown;
            }

            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
        }

        GUILayout.EndHorizontal();
        GUILayout.EndArea();
        #endregion
        #region Selected playerslot info
        if (isInfoShown)
        {
            GUI.Box(topInfoSelectedPlayerSlotRect, "Info");
            GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(topInfoSelectedPlayerSlotRect));
            GUILayout.BeginVertical();
            infoBoxScrollPos = GUILayout.BeginScrollView(infoBoxScrollPos);

            GUILayout.Label("Name: " + selectedInfoPlayerSlot.name);
            GUILayout.Label("Side: " + selectedInfoPlayerSlot.side.ToString());
            GUILayout.Label("Hero: " + selectedInfoPlayerSlot.hero.ToString());

            if (GUILayout.Button("Ping"))
            {
                selectedInfoPlayerSlotPing = Network.GetAveragePing(selectedInfoPlayerSlot.networkPlayer);
            }
            GUILayout.Label(selectedInfoPlayerSlotPing == -1 ? "Error!" : (selectedInfoPlayerSlotPing != 0 ? selectedInfoPlayerSlotPing.ToString() : ""));

            if (GUILayout.Button("Close"))
            {
                isInfoShown = false;
                selectedInfoPlayerSlot = null;
                selectedInfoPlayerSlotPing = 0;
            }

            GUILayout.EndScrollView();
            GUILayout.EndVertical();
            GUILayout.EndArea();
        }
        #endregion

        // Stats
        #region Stats
        GUI.Box(topInfoLeftStatsBarRect, "");
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(topInfoLeftStatsBarRect));
        GUILayout.BeginHorizontal();

        GUI.enabled = GameManagementManager.Instance.state == GameState.Game;
        if (GUILayout.Button(new GUIContent("C")))
        {
            isCombatLogOpened =! isCombatLogOpened;
        }
        if (GUILayout.Button(new GUIContent("T")))
        {
            isTeamObserverOpened =! isTeamObserverOpened;
        }
        GUI.enabled = true;

        GUILayout.EndHorizontal();
        GUILayout.EndArea();
        #endregion

        if (isCombatLogOpened)
            GUI.Window(0, statsInfoCombatLogRect, DrawCombatLog, new GUIContent("Combat Log"));
        if(isTeamObserverOpened)
            GUI.Window(1, statsInfoTeamObserverRect, DrawTeamObserver, new GUIContent("Team Observer"));
    }

    private void DrawCombatLog(int id)
    {
        GUILayout.Label("Not yet implemented!");

        GUI.DragWindow();
    }
    private void DrawTeamObserver(int id)
    {
        string[] playersInTeam = new string[5];
        List<PlayerSlot> playersInMyTeam = GameManagementManager.Instance.currentGame.currentSlot.side == UnitSide.Green ? GameManagementManager.Instance.currentGame.greenSlots : GameManagementManager.Instance.currentGame.redSlots;
        for (int i = 0; i < playersInMyTeam.Count; i++)
        {
            playersInTeam[i] = playersInMyTeam[i].name;
        }

        teamObserverGridSelectedItem = GUILayout.SelectionGrid(teamObserverGridSelectedItem, playersInTeam, 2);

        if (teamObserverGridSelectedItem > -1 && teamObserverGridSelectedItem < 5)
        {
            HeroUnit selectedHero = GameManagementManager.Instance.playerHeroUnit;
            for (int i = 0; i < HeroUnit.allHeroes.Count; i++)
            {
                if (HeroUnit.allHeroes[i].playerSlot.name == playersInTeam[teamObserverGridSelectedItem])
                    selectedHero = HeroUnit.allHeroes[i];
            }

            if (selectedHero != null)
            {
                GUILayout.Label("Comparison of \"" + selectedHero.playerSlot.name + "\" (" + selectedHero.unitName + ") with you, \"" + GameManagementManager.Instance.playerHeroUnit.playerSlot.name + "\" (" + GameManagementManager.Instance.playerHeroUnit.unitName + ")");
                GUILayout.Label("Not yet implemented!");
            }
        }

        GUI.DragWindow();
    }
}

[Serializable]
public enum SelectedStatsInfoWindow
{
    CombatLog,
    TeamObserver,
    None
}

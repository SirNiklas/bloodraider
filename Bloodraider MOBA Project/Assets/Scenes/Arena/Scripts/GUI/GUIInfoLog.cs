﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIInfoLog : MonoBehaviour
{
    public static GUIInfoLog Instance;

    public Rect infoLogRect;
    public List<string> messages;
    public string[] lastFourMessages;

    public void AddMessage(string msg)
    {
        messages.Add(msg);
    }

    private void ApplyLastFourMessagesToArray()
    {
        lastFourMessages[3] = messages.Count > 3 ? messages[3] : "";
        lastFourMessages[2] = messages.Count > 2 ? messages[2] : "";
        lastFourMessages[1] = messages.Count > 1 ? messages[1] : "";
        lastFourMessages[0] = messages.Count > 0 ? messages[0] : "";
    }

    void Awake()
    {
        Instance = this;
        infoLogRect = new Rect(10, 180, 220, 170);
    }

    void OnGUI()
    {
        GUILayout.BeginArea(infoLogRect);
        GUILayout.BeginVertical();

        ApplyLastFourMessagesToArray();
        for (int i = 3; i >= 0; i--)
        {
            GUILayout.Label(lastFourMessages[i]);
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
}

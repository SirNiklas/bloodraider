﻿using UnityEngine;
using System.Collections;

public class GUIChat : MonoBehaviour
{
    public const float chatPreviewTime = 3;

    public bool isChatVisible, isChatPreviewVisible;
    public ChatMode currentChatMode;

    public string chatText;
    public Rect chatWindowBox;
    public Vector2 chatWindowScrollPos;

    void Awake()
    {
        chatWindowBox = new Rect(Screen.width / 2 - 200, Screen.height - 350, 400, 250);
        SystemChat.Instance.OnReceiveChatMessage += Instance_OnReceiveChatMessage;
    }

    void Instance_OnReceiveChatMessage(string msg, ChatMode chatmode)
    {
        isChatPreviewVisible = true;
        currentChatMode = chatmode;
        Invoke("DisableChatPreview", chatPreviewTime);
    }

    void Start()
    {
        if (GameManagementManager.Instance.currentGameSettings.isSpectatorMode)
            currentChatMode = ChatMode.Watcher;
        else
            currentChatMode = ChatMode.Team;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            isChatVisible = !isChatVisible;
            if (isChatVisible)
                GUI.FocusControl("ChatTextField");
        }
    }

    void OnGUI()
    {
        if (!isChatVisible)
        {
            if (isChatPreviewVisible)
            {
                GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(chatWindowBox));
                GUILayout.BeginVertical();

                GUILayout.Space(10);

                #region Chat messages
                chatWindowScrollPos = GUILayout.BeginScrollView(chatWindowScrollPos);

                if (currentChatMode == ChatMode.Public)
                {
                    for (int i = 0; i < SystemChat.Instance.publicChat.Count; i++)
                    {
                        GUILayout.Label(SystemChat.Instance.publicChat[i]);
                    }
                }
                else if (currentChatMode == ChatMode.Team)
                {
                    for (int i = 0; i < SystemChat.Instance.teamChat.Count; i++)
                    {
                        GUILayout.Label(SystemChat.Instance.teamChat[i]);
                    }
                }
                else if (currentChatMode == ChatMode.Watcher)
                {
                    for (int i = 0; i < SystemChat.Instance.watcherChat.Count; i++)
                    {
                        GUILayout.Label(SystemChat.Instance.watcherChat[i]);
                    }
                }

                GUILayout.EndScrollView();
                #endregion

                GUILayout.EndVertical();
                GUILayout.EndArea();
            }

            return;
        }

        GUI.depth = -201; // SelectedUnitInfo GUI is -200

        GUI.Box(chatWindowBox, "Chat (" + currentChatMode.ToString() + ")");
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(chatWindowBox));

        #region Chat mode selection
        GUILayout.BeginHorizontal();
        
        GUI.enabled = !GameManagementManager.Instance.currentGameSettings.isSpectatorMode;
        if (GUILayout.Button("Public"))
        {
            currentChatMode = ChatMode.Public;
        }
        GUI.enabled = true;
        GUI.enabled = !GameManagementManager.Instance.currentGameSettings.isSpectatorMode;
        if (GUILayout.Button("Team"))
        {
            currentChatMode = ChatMode.Team;
        }
        GUI.enabled = true;
        GUI.enabled = GameManagementManager.Instance.currentGameSettings.isSpectatorMode;
        if (GUILayout.Button("Watcher"))
        {
            currentChatMode = ChatMode.Watcher;
        }
        GUI.enabled = true;

        GUILayout.EndHorizontal();
        #endregion

        GUILayout.BeginVertical();
        #region Chat messages
        chatWindowScrollPos = GUILayout.BeginScrollView(chatWindowScrollPos);

        if (currentChatMode == ChatMode.Public)
        {
            for (int i = 0; i < SystemChat.Instance.publicChat.Count; i++)
            {
                GUILayout.Label(SystemChat.Instance.publicChat[i]);
            }
        }
        else if (currentChatMode == ChatMode.Team)
        {
            for (int i = 0; i < SystemChat.Instance.teamChat.Count; i++)
            {
                GUILayout.Label(SystemChat.Instance.teamChat[i]);
            }
        }
        else if (currentChatMode == ChatMode.Watcher)
        {
            for (int i = 0; i < SystemChat.Instance.watcherChat.Count; i++)
            {
                GUILayout.Label(SystemChat.Instance.watcherChat[i]);
            }
        }

        GUILayout.EndScrollView();
        #endregion

        #region Textfield and Send
        GUILayout.BeginHorizontal();

        GUI.SetNextControlName("ChatTextField");
        chatText = GUILayout.TextField(chatText, 100);

        if (GUILayout.Button("Send"))
        {
            SystemChat.Instance.WriteChatMessage(currentChatMode, chatText);
            chatText = "";
        }

        GUILayout.EndHorizontal();
        #endregion
        GUILayout.EndVertical();

        GUILayout.EndArea();
    }

    void DisableChatPreview()
    {
        isChatPreviewVisible = false;
    }
}

﻿using UnityEngine;
using System.Collections;

public class GUITutorial : MonoBehaviour
{
    public string[] tutorialBoxes;
    public int currentTutorialBox;

    public Rect tutorialBoxRect;
    public Vector2 scrollPos;

    void Awake()
    {
        tutorialBoxRect = new Rect(Screen.width - 260, 100, 250, 170);
    }

    void OnGUI()
    {
        if (!GameSettingsManager.Instance.playerSettings.isTutorialActivated)
            return;

        GUI.depth = -300;

        GUI.Box(tutorialBoxRect, "Tutorial");
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(tutorialBoxRect));
        GUILayout.BeginVertical();

        scrollPos = GUILayout.BeginScrollView(scrollPos);
        GUILayout.Label(tutorialBoxes[currentTutorialBox]);
        GUILayout.EndScrollView();

        GUILayout.Space(5);
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Close tutorial"))
        {
            GameSettingsManager.Instance.playerSettings.isTutorialActivated = false;
        }

        GUI.enabled = (currentTutorialBox - 1 >= 0);
        if (GUILayout.Button("Back"))
        {
            currentTutorialBox -= 1;
        }
        GUI.enabled = true;

        GUI.enabled = (currentTutorialBox + 1 < tutorialBoxes.Length);
        if (GUILayout.Button("Next"))
        {
            currentTutorialBox += 1;
        }
        GUI.enabled = true;

        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
}

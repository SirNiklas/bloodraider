﻿using UnityEngine;
using System.Collections;

public class WorldGUIBar : MonoBehaviour
{
    public const int guiDepthOnHover = -101, guiDepthOnNoHover = -100;

    public float heightFromUnitOrigin = 100;
    public BaseUnit unit;
    public bool isSelected;

    private float maxHealthBarLength = 100;

    private Rect namePosition;
    private Rect healthBarPosition;
    private Rect constHealthBarPosition;
    private int currentGuiDepth;

    void Update()
    {
        Vector2 heroScreenPos = Camera.main.WorldToScreenPoint(unit.transform.position);

        float currentHealth = (maxHealthBarLength - 4) - ((((float)unit.totalMaxHealth - (float)unit.attributes[AttributeType.CurrentHealth]) / unit.totalMaxHealth) * (maxHealthBarLength - 4));

        namePosition = new Rect(heroScreenPos.x - 50, Camera.main.pixelHeight - (heroScreenPos.y + heightFromUnitOrigin), 200, 25);

        healthBarPosition = new Rect(heroScreenPos.x - 48, Camera.main.pixelHeight - (heroScreenPos.y + (heightFromUnitOrigin - 27)), currentHealth, 6);
        constHealthBarPosition = new Rect(heroScreenPos.x - 50, Camera.main.pixelHeight - (heroScreenPos.y + (heightFromUnitOrigin - 25)), maxHealthBarLength, 10);
    }

    void OnMouseEnter()
    {
        currentGuiDepth = guiDepthOnHover;
    }
    void OnMouseExit()
    {
        currentGuiDepth = guiDepthOnNoHover;
    }

    void OnGUI()
    {
        if (!unit.fullObject.activeSelf)
            return;
        if (unit.isDead)
            return;
        if (GameManagementManager.Instance.state == GameState.HeroSelection)
            return;
        if (unit.state == State.Invulnerable)
            return;

        GUI.depth = currentGuiDepth;
        GUI.Label(namePosition, (unit is HeroUnit ? (unit as HeroUnit).playerSlot.name : "") + (isSelected ? " (Selected)" : ""));

        
        GUI.Box(constHealthBarPosition, "", GUIManager.Instance.worldBarBackgroundGUIStyle);
        GUI.Box(healthBarPosition, "", unit.side == GameManagementManager.Instance.playerHeroUnit.side ? GUIManager.Instance.friendlyHealthBarGUIStyle : GUIManager.Instance.enemyHealthBarGUIStyle);
    }
}

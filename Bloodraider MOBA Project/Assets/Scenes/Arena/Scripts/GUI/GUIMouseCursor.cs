﻿using UnityEngine;
using System.Collections;
using System;

public class GUIMouseCursor : MonoBehaviour
{
    public static GUIMouseCursor Instance;

    public Rect cursorRect;

    public bool showCursor;
    public CursorMode cursor;
    public Texture2D normalCursorTexture, spellCastCursorTexture;

    void Awake()
    {
        Instance = this;
        
    }

    void Update()
    {
        Screen.showCursor = false;
        cursorRect = new Rect(Input.mousePosition.x, Screen.height - Input.mousePosition.y, 32, 32);
    }

    void OnGUI()
    {
        if (!showCursor)
            return;

        GUI.depth = -1000;

        if (cursor == CursorMode.Normal)
        {
            GUI.DrawTexture(cursorRect, normalCursorTexture);
        }
        else if (cursor == CursorMode.SpellCast)
        {
            GUI.DrawTexture(cursorRect, spellCastCursorTexture);
        }
    }
}

[Serializable]
public enum CursorMode
{
    Normal,
    SpellCast
}

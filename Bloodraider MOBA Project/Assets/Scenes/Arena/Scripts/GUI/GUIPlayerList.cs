﻿using UnityEngine;
using System.Collections;

public class GUIPlayerList : MonoBehaviour
{
    public static GUIPlayerList Instance;

    public Rect playerlistRect;
    public Vector2 playerListScrollPos;

    public bool isPlayerListShown;

    void Awake()
    {
        Instance = this;
        playerlistRect = new Rect(0, Screen.height / 2 - 180, 360, 380);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            isPlayerListShown = true;
        else if (Input.GetKeyUp(KeyCode.Space))
            isPlayerListShown = false;
    }

    void OnGUI()
    {
        if (!isPlayerListShown)
            return;

        GUI.depth = -202; // Selected unit info GUI is -200, chat is -201

        GUI.Box(playerlistRect, "Playerlist");
        GUILayout.BeginArea(GUIManager.Instance.GetImprovedRect(playerlistRect));
        GUILayout.BeginVertical();
        playerListScrollPos = GUILayout.BeginScrollView(playerListScrollPos);

        GUILayout.Label(GameManagementManager.Instance.currentGame.currentSlot.name + (GameManagementManager.Instance.currentGame.currentSlot.hero != "" ? " (" + GameManagementManager.Instance.currentGame.currentSlot.hero + ")" : "") + " on " + GameManagementManager.Instance.currentGame.currentSlot.side);
        GUILayout.Label(GameManagementManager.Instance.currentGameSettings.matchName + (GameManagementManager.Instance.currentGameSettings.isLeagueMatch ? " in league \"" + GameManagementManager.Instance.currentGameSettings.leagueName + "\"" : ""));
        GUILayout.Label((GameManagementManager.Instance.currentGameSettings.isSpectatorMode ? "You are spectating (" + GameManagementManager.Instance.GetSpectatorCount() + " spectators)" : GameManagementManager.Instance.GetSpectatorCount() + " spectators"));
        GUILayout.Space(8);

        GUILayout.Label("Spectators");
        foreach (var player in GameManagementManager.Instance.currentGame.spectators)
        {
            GUILayout.Label(player);
        }
        if (GameManagementManager.Instance.currentGame.spectators.Count <= 0)
            GUILayout.Label(" -No spectators-");

        GUILayout.Label("Green players");
        foreach (var greenplayer in HeroUnit.allHeroes.FindAll((h) => { if (h.side == UnitSide.Green) return true; else return false; }))
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(greenplayer.playerSlot.name + " (" + greenplayer.playerSlot.hero + ")");
            GUILayout.Label("K: " + greenplayer.kills + ", D:" + greenplayer.deaths + ", Lh:" + greenplayer.lasthits);

            if (Network.isServer && greenplayer.name != GameInformationManager.Instance.userProfileName)
            {
                if (GUILayout.Button("Kick"))
                {
                    GameManagementManager.Instance.KickSpecificPlayer(greenplayer.playerSlot.name);
                }
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(8);

        GUILayout.Label("Red players");
        foreach (var redplayer in HeroUnit.allHeroes.FindAll((h) => { if (h.side == UnitSide.Red) return true; else return false; }))
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(redplayer.playerSlot.name + " (" + redplayer.playerSlot.hero + ")");
            GUILayout.Label("K: " + redplayer.kills + ", D:" + redplayer.deaths + ", Lh:" + redplayer.lasthits);

            if (Network.isServer && redplayer.name != GameInformationManager.Instance.userProfileName)
            {
                if (GUILayout.Button("Kick"))
                {
                    GameManagementManager.Instance.KickSpecificPlayer(redplayer.playerSlot.name);
                }
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Space(8);

        if (GUILayout.Button("Disconnect" + (Network.isServer ? " (Stop server)" : "")))
        {
            GameManagementManager.Instance.DoDisconnect();
        }

        GUILayout.EndScrollView();
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
}

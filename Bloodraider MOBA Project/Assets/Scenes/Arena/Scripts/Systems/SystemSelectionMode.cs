﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SystemSelectionMode : MonoBehaviour
{
    public static SystemSelectionMode Instance;

    public SelectionMode selectionMode;
    public List<BaseUnit> selectedUnits;
    public int selectedUnit = -1;
    public Spell castedSpell;
    public BaseUnit caster;

    private Camera mainCamera;

    public SystemSelectionMode()
    {
        Instance = this;
    }

    public BaseUnit GetSelectedUnit()
    {
        try
        {
            return selectedUnits[selectedUnit];
        }
        catch (ArgumentOutOfRangeException)
        {
            return null;
        }
    }
    public void PerformSelectUnitsByLMBClick(List<BaseUnit> b)
    {
        ClearSelection();

        foreach (var unit in b)
        {
            if (unit == null)
                continue;
            else
            {
                unit.GetComponent<WorldGUIBar>().isSelected = true;
                selectedUnits.Add(unit);
            }
        }

        selectedUnit = 0;
    }
    public void AddUnitToSelection(BaseUnit b)
    {
        b.GetComponent<WorldGUIBar>().isSelected = true;
        selectedUnits.Add(b);
    }
    public void ClearSelection()
    {
        foreach (var unit in selectedUnits)
        {
            if (unit == null)
                continue;
            unit.GetComponent<WorldGUIBar>().isSelected = false;
        }
        selectedUnits.Clear();

        selectedUnit = -1;
    }

    void Awake()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        if (GameManagementManager.Instance.state == GameState.HeroSelection || GameManagementManager.Instance.state == GameState.Ending)
            return;

        GUIMouseCursor.Instance.cursor = selectionMode == SelectionMode.UnitSelection ? CursorMode.Normal : CursorMode.SpellCast;

        #region Commands
        bool commandsUsable = !GameManagementManager.Instance.currentGameSettings.isSpectatorMode;

        if (Input.GetKeyDown(KeyCode.Escape) && commandsUsable)
        {
            ClearSelection();
        }
        if (Input.GetKeyDown(GameSettingsManager.Instance.playerSettings.centerSelectedHeroKey))
        {
            if (selectedUnits.Count > 0 && selectedUnit != -1)
            {
                BaseUnit currentlyselectedunit = selectedUnits[selectedUnit];
                mainCamera.transform.position = new Vector3(currentlyselectedunit.transform.position.x, mainCamera.transform.position.y, currentlyselectedunit.transform.position.z + 25);
            }
        }
        if (Input.GetKeyDown(GameSettingsManager.Instance.playerSettings.selectAllMyUnitsKey) && commandsUsable)
        {
            PerformSelectUnitsByLMBClick(GameManagementManager.Instance.playerHeroUnit.unitsUnderControl);
        }
        if (Input.GetKeyDown(GameSettingsManager.Instance.playerSettings.selectMyHeroKey) && commandsUsable)
        {
            PerformSelectUnitsByLMBClick(new List<BaseUnit>() { GameManagementManager.Instance.playerHeroUnit });
        }
        if (Input.GetKeyDown(GameSettingsManager.Instance.playerSettings.selectMyMinionsKey) && commandsUsable)
        {
            PerformSelectUnitsByLMBClick(GameManagementManager.Instance.playerHeroUnit.unitsUnderControl.FindAll((b) => { if (b != GameManagementManager.Instance.playerHeroUnit) return true; else return false; }));
        }
        if (Input.GetKeyDown(GameSettingsManager.Instance.playerSettings.incrementSelectedUnitInListKey) && commandsUsable)
        {
            if (selectedUnits.Count < 1)
                selectedUnit = -1;

            if (selectedUnit + 1 > selectedUnits.Count - 1)
                selectedUnit = 0;
            else
                selectedUnit += 1;
        }
        #endregion

        if (Input.GetMouseButtonDown(0))
        {
            if (selectionMode == SelectionMode.UnitSelection)
            {
                ClickInformation c = DoClick();
                if (c.isUnit && c.position != Vector3.zero)
                {
                    if (Input.GetKeyDown(KeyCode.LeftShift))
                    {
                        if (GameManagementManager.Instance.playerHeroUnit.unitsUnderControl.Contains(c.unit))
                        {
                            AddUnitToSelection(c.unit);
                        }
                    }
                    else
                        PerformSelectUnitsByLMBClick(new List<BaseUnit>() { c.unit });
                }
            }
            else if (selectionMode == SelectionMode.Cast)
            {
                ClickInformation c = DoClick();

                if (Input.GetKeyDown(GameSettingsManager.Instance.playerSettings.castSpellWithAllSelectedUnitsKey) && commandsUsable)
                {
                    foreach (var unit in selectedUnits)
                    {
                        if (unit.spells.Find((s) => { if (s.spellName == castedSpell.spellName) return true; else return false; }) != null)
                            unit.CastSpell(castedSpell.spellName, c.position, c.unit);
                    }
                }
                else
                    caster.CastSpell(castedSpell.spellName, c.position, c.unit);

                SetCastModeOff();
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (GameManagementManager.Instance.playerHeroUnit == null)
                return;

            if (selectionMode == SelectionMode.UnitSelection)
            {
                ClickInformation c = DoClick();
                if (c.position != Vector3.zero && selectedUnit != null)
                {
                    if (c.hitCollider.CompareTag("Walkable"))
                    {
                        foreach (var unit in selectedUnits)
                        {
                            if (GameManagementManager.Instance.playerHeroUnit.unitsUnderControl.Contains(unit))
                            {
                                unit.DoRightClickDamage(null);
                                unit.GotoPosition(c.position);
                            }
                        }
                    }
                    else if (c.hitCollider.CompareTag("Unit") && c.isUnit)
                    {
                        foreach (var unit in selectedUnits)
                        {
                            if (GameManagementManager.Instance.playerHeroUnit.unitsUnderControl.Contains(unit))
                            {
                                unit.DoRightClickDamage(c.unit);
                            }
                        }
                    }
                }
            }
            else if (selectionMode == SelectionMode.Cast)
            {
                SetCastModeOff();
            }
        }
    }

    public void CastSpell(BaseUnit c, Spell castedspell)
    {
        castedSpell = castedspell;
        caster = c;

        selectionMode = SelectionMode.Cast;

        if (castedSpell.target == SpellTarget.NoTarget)
        {
            caster.CastSpell(castedSpell.spellName, Vector3.zero, null);
            SetCastModeOff();
        }
    }
    private void SetCastModeOff()
    {
        castedSpell = null;
        caster = null;

        selectionMode = SelectionMode.UnitSelection;
    }

    private ClickInformation DoClick()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            return new ClickInformation(hit.point, hit.collider.GetComponent<BaseUnit>(), hit.collider);
        }

        return new ClickInformation(Vector3.zero, null, null);
    }
}

public struct ClickInformation
{
    public Vector3 position;
    public BaseUnit unit;
    public Collider hitCollider;

    public bool isUnit { get { return (unit != null); } }

    public ClickInformation(Vector3 pos, BaseUnit b, Collider c)
    {
        position = pos;
        unit = b;
        hitCollider = c;
    }
}
public enum SelectionMode
{
    UnitSelection,
    Cast
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SystemChat : MonoBehaviour
{
    public const float chatMessageWritingCooldown = 1;

    public delegate void OnReceiveChatMessageHandler(string message, ChatMode chatmode);
    public event OnReceiveChatMessageHandler OnReceiveChatMessage;

    public static SystemChat Instance;
    public List<string> publicChat, teamChat, watcherChat;

    private float currentChatMessageWritingCooldown;

    void Awake()
    {
        Instance = this;
    }
    void Update()
    {
        if (currentChatMessageWritingCooldown < chatMessageWritingCooldown)
            currentChatMessageWritingCooldown += Time.deltaTime;
        else
            currentChatMessageWritingCooldown = chatMessageWritingCooldown;
    }

    public bool IsAbleToWriteChatMessage()
    {
        return currentChatMessageWritingCooldown == chatMessageWritingCooldown;
    }
    public void WriteChatMessage(ChatMode chatmode, string msg)
    {
        if (!IsAbleToWriteChatMessage())
            return;
        if (string.IsNullOrEmpty(msg))
            return;

        msg = "[" + GameInformationManager.Instance.userProfileName + "] " + msg;
        networkView.RPC("WriteChatMessageNetwork", RPCMode.AllBuffered, chatmode.ToString(), msg);

        currentChatMessageWritingCooldown = 0;
    }

    [RPC]
    void WriteChatMessageNetwork(string chatmode, string msg)
    {
        switch (chatmode)
        {
            case "Public":
                publicChat.Add(msg);
                break;

            case "Team":
                teamChat.Add(msg);
                break;

            case "Watcher":
                watcherChat.Add(msg);
                break;

            default:
                break;
        }

        if (OnReceiveChatMessage != null)
            OnReceiveChatMessage(msg, (ChatMode)Enum.Parse(typeof(ChatMode), chatmode));
    }
}

[Serializable]
public enum ChatMode
{
    Public,
    Team,
    Watcher
}


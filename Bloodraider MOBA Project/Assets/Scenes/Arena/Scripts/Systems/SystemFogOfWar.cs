﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SystemFogOfWar : MonoBehaviour
{
    public static SystemFogOfWar Instance;

    public UnitSide localPlayerSide;
    public List<BaseUnit> seenEnemyUnits;

    void Awake()
    {
        Instance = this;
    }

    public void Initialize()
    {
        if (GameManagementManager.Instance.currentGameSettings.isSpectatorMode)
        {
            localPlayerSide = UnitSide.All;
        }
        else
        {
            localPlayerSide = GameManagementManager.Instance.playerHeroUnit.side;

            foreach (var enemyunit in BaseUnit.allUnits.FindAll((b) => { if (b.side != localPlayerSide) return true; else return false; }))
            {
                enemyunit.SetLocalRenderVisibility(false);
            }
        }
    }

    void LateUpdate()
    {
        #region Obsolete
        //unitData.Clear();
        //BaseUnit.allUnits.ForEach((b) => { unitData.Add(b); });

        //foreach (var newseenunit in newSeenUnits)
        //{
        //    newseenunit.SetLocalRenderVisibility(false);
        //}
        //newSeenUnits.Clear();

        //foreach (var unitdataobj in unitData)
        //{
        //    //if (unitdataobj is StructureUnit)
        //    //    continue;

        //    float currentSightRange = unitdataobj.sightRangeDay; // TODO: Change when day/night system is made
        //    unitdataobj.fogOfWarLight.range = currentSightRange * 10;
        //    unitdataobj.fogOfWarLight.intensity = 0f;

        //    //int sightRangeTopAngle = Mathf.RoundToInt(Mathf.Sqrt(Mathf.Pow(10, 2) + Mathf.Pow(currentSightRange, 2)));
        //    //unitdataobj.fogOfWarLight.spotAngle = sightRangeTopAngle;


        //    newSeenUnits.Clear();

        //    if (unitdataobj.side == localPlayerSide)
        //    {
        //        List<Collider> newCols = new List<Collider>(Physics.OverlapSphere(unitdataobj.transform.position, currentSightRange));
        //        foreach (var col in newCols)
        //        {
        //            if (col.CompareTag("Unit"))
        //            {
        //                BaseUnit unit = col.GetComponent<BaseUnit>();

        //                if (unit.side != localPlayerSide)
        //                {
        //                    unit.SetLocalRenderVisibility(true);
        //                    newSeenUnits.Add(unit);
        //                }
        //            }
        //        }

        //        //foreach (var lastseenunit in lastSeenUnits)
        //        //{
        //        //    if (!newSeenUnits.Contains(lastseenunit))
        //        //        lastseenunit.SetLocalRenderVisibility(false);
        //        //}

        //        //lastSeenUnits.Clear();
        //        //lastSeenUnits.AddRange(newSeenUnits.ToArray());
        //    }
        //}
        #endregion

        if (GameManagementManager.Instance.state == GameState.HeroSelection)
        {
            foreach (var unit in BaseUnit.allUnits)
            {
                unit.SetLocalRenderVisibility(true);
            }
        }
        else
        {
            if (localPlayerSide == UnitSide.All)
            {
                return;
            }

            foreach (var friendlyunit in BaseUnit.allUnits.FindAll((b) => { if (b.side == localPlayerSide) return true; else return false; }))
	        {
		        foreach (var seenunitslastframe in seenEnemyUnits)
                {
                    if (seenunitslastframe != null)
                    {
                        if (!CheckIfInSightRangeAndVisible(friendlyunit, seenunitslastframe))
                        {
                            seenunitslastframe.SetLocalRenderVisibility(false);
                        }
                    }
                } 
	        }
            seenEnemyUnits.Clear();

            #region Sight range checking
            foreach (var friendlyunit in BaseUnit.allUnits.FindAll((b) => { if (b.side == localPlayerSide) return true; else return false; }))
            {
                friendlyunit.SetLocalRenderVisibility(true);
                foreach (var enemyunit in BaseUnit.allUnits.FindAll((b) => { if (b.side != localPlayerSide) return true; else return false; }))
                {
                    if (CheckIfInSightRangeAndVisible(friendlyunit, enemyunit))
                    {
                        enemyunit.SetLocalRenderVisibility(true);
                        seenEnemyUnits.Add(enemyunit);
                    }
                }
            }
            #endregion
        }
    }

    private bool CheckIfInSightRangeAndVisible(BaseUnit ally, BaseUnit enemy)
    {
        float currentSightRange = ally.sightRangeDay; // TODO: Change if day/night system is developed

        if (Vector2.Distance(new Vector2(ally.transform.position.x, ally.transform.position.z), new Vector2(enemy.transform.position.x, enemy.transform.position.z)) <= currentSightRange)
        {
            // An ally is in sight range to an enemy unit
            // Checking so the enemy unit can get added to the seenEnemyUnits list

            if (enemy.isInvisible && !enemy.isInvisibilityRevealed) // The unit is invisible and not invisible revealed, so no, the ally is only in sight range but not able to see the enemy
                return false;
            //if (enemy.transform.position.y > ally.transform.position.y + BaseUnit.uphillToleranceHeight) // Not usable because if spells let you "fly" or get higher you get more vision and other units cannot see you anymore.
            //    return false;

            return true;
        }
        else
            return false;
    }
}

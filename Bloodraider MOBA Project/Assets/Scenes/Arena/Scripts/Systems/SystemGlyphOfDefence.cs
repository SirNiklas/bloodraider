﻿using UnityEngine;
using System.Collections;
using System;

public class SystemGlyphOfDefence : MonoBehaviour
{
    public const int glyphCooldownSeconds = 280;
    public static SystemGlyphOfDefence Instance;

    public bool isOnCooldown;
    public float currentCooldown;

    void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        if (isOnCooldown)
        {
            currentCooldown -= Time.deltaTime;
            if (currentCooldown <= 0)
            {
                currentCooldown = 0;
                isOnCooldown = false;
            }
        }
    }

    public void ActivateGlyph()
    {
        networkView.RPC("ActivateGlyphRPC", RPCMode.All, GameManagementManager.Instance.playerHeroUnit.side.ToString());
    }
    public void ActivateOnlyGlyphCooldown(UnitSide side)
    {
        networkView.RPC("ActivateOnlyGlyphCooldownRPC", RPCMode.All, side);
    }

    [RPC]
    private void ActivateOnlyGlyphCooldownRPC(string unitside)
    {
        UnitSide userSide = (UnitSide)Enum.Parse(typeof(UnitSide), unitside);
        if (userSide == GameManagementManager.Instance.playerHeroUnit.side)
            return;

        isOnCooldown = true;
        currentCooldown = glyphCooldownSeconds;
    }
    [RPC]
    private void ActivateGlyphRPC(string unitside)
    {
        UnitSide userSide = (UnitSide)Enum.Parse(typeof(UnitSide), unitside);
        if (userSide != GameManagementManager.Instance.playerHeroUnit.side)
            return;

        isOnCooldown = true;
        currentCooldown = glyphCooldownSeconds;

        foreach (var unit in BaseUnit.allUnits)
        {
            if (unit is StructureUnit && unit.side == userSide)
                unit.ApplyBuff("Glyph of Defence");
        }
    }
}

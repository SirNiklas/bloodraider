﻿using UnityEngine;
using System.Collections;

public class SystemRTSCamera : MonoBehaviour {

    public Transform cameraTransform;

    public int ScrollSpeed = 100;

    public int ZoomSpeed = 25;
    public int ZoomMin = 5;
    public int ZoomMax = 100;

    public int PanSpeed = 50;
    public int PanAngleMin = 25;
    public int PanAngleMax = 80;

    public string forward = "z";
    public string backward = "s";
    public string left = "a";
    public string right = "d";

    //variabiles for rotating the camera
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 250;
    public float sensitivityY = 250;

    public float minimumX = -360;
    public float maximumX = 360;

    public float minimumY = -270;
    public float maximumY = 90;

    public Rect topScreenRect, leftScreenRect, bottomScreenRect, rightScreenRect;
    public float screenBorderRecognitionThickness = 50;

    void Awake()
    {
        cameraTransform = Camera.main.transform;
        
        // Screen-border rects
        topScreenRect = new Rect(0, 0, Screen.width, screenBorderRecognitionThickness);
        leftScreenRect = new Rect(0, 0, screenBorderRecognitionThickness, Screen.height);
        bottomScreenRect = new Rect(0, Screen.height - screenBorderRecognitionThickness, Screen.width, screenBorderRecognitionThickness);
        rightScreenRect = new Rect(Screen.width - screenBorderRecognitionThickness, 0, screenBorderRecognitionThickness, Screen.height);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Init camera translation for this frame.
        var translation = Time.deltaTime * ScrollSpeed;

        // Zoom in or out
        var zoomDelta = Input.GetAxis("Mouse ScrollWheel") * ZoomSpeed * Time.deltaTime;
        if (zoomDelta != 0)
        {
            //In order to Zoom, there is some limits
            if (cameraTransform.camera.fieldOfView - ZoomSpeed * zoomDelta > ZoomMin && cameraTransform.camera.fieldOfView - ZoomSpeed * zoomDelta < ZoomMax)
            {
                cameraTransform.camera.fieldOfView -= ZoomSpeed * zoomDelta;
            }
        }

        // Move camera with arrow keys
        if (Input.GetKey(left))
        {
            MoveCameraTo(translation, MoveCameraPositionType.Left);
        }
        if (Input.GetKey(backward))
        {
            MoveCameraTo(translation, MoveCameraPositionType.Backward);
        }
        if (Input.GetKey(right))
        {
            MoveCameraTo(translation, MoveCameraPositionType.Right);
        }
        if (Input.GetKey(forward))
        {
            MoveCameraTo(translation, MoveCameraPositionType.Forward);
        }

        // Move camera with the mouse
        Vector2 mousePosition = cameraTransform.camera.WorldToScreenPoint(Input.mousePosition);
        if (topScreenRect.Contains(mousePosition))
        {
            MoveCameraTo(translation, MoveCameraPositionType.Forward);
        }
        if (leftScreenRect.Contains(mousePosition))
        {
            MoveCameraTo(translation, MoveCameraPositionType.Left);
        }
        if (bottomScreenRect.Contains(mousePosition))
        {
            MoveCameraTo(translation, MoveCameraPositionType.Backward);
        }
        if (rightScreenRect.Contains(mousePosition))
        {
            MoveCameraTo(translation, MoveCameraPositionType.Right);
        }

        //Rotate the camera while pressing "Backspace" key MUST BE FIXED
        //if (Input.GetKey(KeyCode.Backspace))
        //{

        //    float rotationY = 0F;

        //    if (axes == RotationAxes.MouseXAndY)
        //    {
        //        float rotationX = cameraTransform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

        //        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        //        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

        //        cameraTransform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        //    }
        //    else if (axes == RotationAxes.MouseX)
        //    {
        //        cameraTransform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
        //    }
        //    else
        //    {
        //        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        //        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

        //        cameraTransform.localEulerAngles = new Vector3(-rotationY, cameraTransform.localEulerAngles.y, 0);
        //    }
        //}


    }

    private void MoveCameraTo(float translation, MoveCameraPositionType movecamerapostype)
    {
        if (movecamerapostype == MoveCameraPositionType.Forward)
        {
            cameraTransform.Translate(new Vector3(0, 0, translation), Space.World);
        }
        else if (movecamerapostype == MoveCameraPositionType.Left)
        {
            cameraTransform.Translate(new Vector3(-translation, 0, 0), Space.World);
        }
        else if (movecamerapostype == MoveCameraPositionType.Backward)
        {
            cameraTransform.Translate(new Vector3(0, 0, -translation), Space.World);
        }
        else if (movecamerapostype == MoveCameraPositionType.Right)
        {
            cameraTransform.Translate(new Vector3(translation, 0, 0), Space.World);
        }
    }
}

public enum MoveCameraPositionType
{
    Forward,
    Left,
    Backward,
    Right
}
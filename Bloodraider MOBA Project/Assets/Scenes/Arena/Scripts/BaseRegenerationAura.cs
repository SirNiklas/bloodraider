﻿using UnityEngine;
using System.Collections;

public class BaseRegenerationAura : MonoBehaviour
{
    public int healthRegeneration, manaRegeneration;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            other.GetComponent<BaseUnit>().ApplyBuff("Base Regeneration");
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            Buff baseRegenerationBuff = other.GetComponent<BaseUnit>().buffs.Find((b) => { if (b.buffName == "Base Regeneration") return true; else return false; });
            if (baseRegenerationBuff == null)
                return;

            other.GetComponent<BaseUnit>().ForceUnapplyBuff("Base Regeneration");
        }
    }

    void baseRegenerationBuff_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] -= healthRegeneration;
        b.attributes[AttributeType.BonusManaRegeneration] -= manaRegeneration;
    }

    void baseRegenerationBuff_OnApplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] += healthRegeneration;
        b.attributes[AttributeType.BonusManaRegeneration] += manaRegeneration;
    }
}

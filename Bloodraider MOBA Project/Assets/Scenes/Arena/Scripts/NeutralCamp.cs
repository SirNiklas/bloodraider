﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class NeutralCamp : MonoBehaviour
{
    public static List<NeutralCamp> neutralCamps = new List<NeutralCamp>();

    public NeutralCampType type;
    public float radius;

    public bool IsCampBlocked()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, radius);
        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
                return true;
        }

        return false;
    }

    void Awake()
    {
        if(!neutralCamps.Contains(this))
            neutralCamps.Add(this);
    }
    void OnDestroy()
    {
        if(neutralCamps.Contains(this))
            neutralCamps.Remove(this);
    }

    public void InvokeRespawn()
    {
        InvokeRepeating("Respawn", NeutralCampManager.Instance.startRespawnTime, NeutralCampManager.Instance.respawnIntervall);
    }

    public virtual void Respawn()
    {
        if (!IsCampBlocked() && Network.isServer)
        {
            List<GameObject> neutralCreeps = NeutralCampManager.Instance.GetNeutralCreeps(type);
            if (neutralCreeps == null)
                return;

            foreach (var neutralcreep in neutralCreeps)
            {
                GameObject neutralcreepobj = (GameObject)Network.Instantiate(neutralcreep, transform.position, Quaternion.identity, 0);
                neutralcreepobj.GetComponent<BaseUnit>().GotoPosition(new Vector3(transform.position.x + UnityEngine.Random.Range(-2, 2), transform.position.y, transform.position.z + UnityEngine.Random.Range(-2, 2)));
                neutralcreepobj.GetComponent<NeutralCreepUnit>().InitializeNeutralCreep();
            }
        }
    }
}

[Serializable]
public enum NeutralCampType
{
    Hard,
    Medium,
    Small,
    Boss
}

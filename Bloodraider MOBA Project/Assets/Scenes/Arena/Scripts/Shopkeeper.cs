﻿using UnityEngine;
using System.Collections;

public class Shopkeeper : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            BaseUnit unit = other.GetComponent<BaseUnit>();
            unit.isBuyingEnabled = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            BaseUnit unit = other.GetComponent<BaseUnit>();
            unit.isBuyingEnabled = false;
        }
    }
}

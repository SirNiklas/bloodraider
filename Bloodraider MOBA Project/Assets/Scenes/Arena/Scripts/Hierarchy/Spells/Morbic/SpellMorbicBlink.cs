﻿using UnityEngine;
using System.Collections;

public class SpellMorbicBlink : Spell
{
    public SpellMorbicBlink()
    {
        OnCastedSpell += SpellMorbicBlink_OnCastedSpell;
    }

    void SpellMorbicBlink_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        //if (Vector3.Distance(position, caster.transform.position) > this.GetSpellProperty("Maximal Blink range"))
        //{
        //    position = Vector3.ClampMagnitude(position, this.GetSpellProperty("Maximal Blink range"));
        //}

        caster.GotoPosition(position);

        caster.navMeshAgentComponent.enabled = false;
        caster.transform.position = new Vector3(position.x, caster.transform.position.y, position.z);
        caster.navMeshAgentComponent.enabled = true;

        caster.DisjointProjectiles(false);
    }
}

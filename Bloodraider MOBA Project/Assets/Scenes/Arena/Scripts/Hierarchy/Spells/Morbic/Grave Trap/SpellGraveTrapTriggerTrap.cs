﻿using UnityEngine;
using System.Collections;

public class SpellGraveTrapTriggerTrap : Spell
{
    public SpellGraveTrapTriggerTrap()
    {
        OnCastedSpell += SpellGraveTrapTriggerTrapManually_OnCastedSpell;
    }

    void SpellGraveTrapTriggerTrapManually_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        MorbicGraveTrapMinionUnit gravetrap = caster as MorbicGraveTrapMinionUnit;
        if (gravetrap != null)
        {
            if (gravetrap.graveTrapSpell == null)
            {
                gravetrap.ReceiveDamage(gravetrap.totalMaxHealth, DamageType.Pure, gravetrap, true);
                return;
            }

            Collider[] cols = Physics.OverlapSphere(gravetrap.transform.position, gravetrap.graveTrapSpell.GetSpellProperty("Effect range"));
            foreach (var col in cols)
            {
                if (col.CompareTag("Unit"))
                {
                    BaseUnit b = col.GetComponent<BaseUnit>();
                    if (b.side == gravetrap.side)
                        continue;
                    b.ApplyLevelBuff("Grave Trap slow", gravetrap.graveTrapSpell);
                }
            }

            gravetrap.ReceiveDamage(gravetrap.totalMaxHealth, DamageType.Pure, gravetrap, true);
        }
    }
}

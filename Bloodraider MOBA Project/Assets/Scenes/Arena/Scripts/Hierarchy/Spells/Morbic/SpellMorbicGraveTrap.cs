﻿using UnityEngine;
using System.Collections;

public class SpellMorbicGraveTrap : Spell
{
    public GameObject graveTrapPrefab;

    public SpellMorbicGraveTrap()
    {
        OnPreCastSpell += SpellMorbicGraveTrap_OnPreCastSpell;
        OnCastedSpell += SpellMorbicSoulKiller_OnCastedSpell;
    }

    bool SpellMorbicGraveTrap_OnPreCastSpell()
    {
        // TODO: If the caster has already set more traps than the max. trap count allows return false
        return true;
    }

    void SpellMorbicSoulKiller_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        GameObject gravetrap = caster.SummonMinion(graveTrapPrefab, position);
        
        MorbicGraveTrapMinionUnit gravetrapminionscript = gravetrap.GetComponent<MorbicGraveTrapMinionUnit>();
        gravetrapminionscript.graveTrapSpell = this;
    }
}

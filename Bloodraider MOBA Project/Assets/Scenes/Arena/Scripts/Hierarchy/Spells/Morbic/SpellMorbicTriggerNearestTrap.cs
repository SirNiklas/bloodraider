﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpellMorbicTriggerNearestTrap : Spell
{
    public SpellMorbicTriggerNearestTrap()
    {
        OnCastedSpell += SpellGraveTrapTriggerNearestTrap_OnCastedSpell;
    }

    void SpellGraveTrapTriggerNearestTrap_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        Collider[] cols = Physics.OverlapSphere(position, 120);
        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                BaseUnit b = col.GetComponent<MorbicGraveTrapMinionUnit>();
                if (b != null)
                {
                    b.CastSpell("Trigger trap", Vector3.zero, null);
                    
                    // TODO: Needs overwork for really triggering the nearest trap
                }
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class SpellMorbicVampiresVengeance : Spell
{
    private BaseUnit spellowner;

    public SpellMorbicVampiresVengeance()
    {
        OnSpellIsActive += SpellMorbicVampiresVengeance_OnSpellIsActive;
    }

    void SpellMorbicVampiresVengeance_OnSpellIsActive(BaseUnit caster, Spell spell)
    {
        spellowner = caster;
        caster.OnDoRightClickDamage += caster_OnDoRightClickDamage;
    }

    void caster_OnDoRightClickDamage(ref int damage, BaseUnit receiver)
    {
        int heal = (int)(damage * (this.GetSpellProperty("Lifesteal Percentage") / 100));
        spellowner.ReceiveHeal(heal);
    }
}

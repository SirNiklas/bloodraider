﻿using UnityEngine;
using System.Collections;

public class SpellPiercingRightclickDamage : Spell
{
    public SpellPiercingRightclickDamage()
    {
        OnSpellIsActive += SpellPiercingRightclickDamage_OnSpellIsActive;
    }

    void SpellPiercingRightclickDamage_OnSpellIsActive(BaseUnit caster, Spell spell)
    {
        caster.OnDoRightClickDamage += caster_OnDoRightClickDamage;
    }

    void caster_OnDoRightClickDamage(ref int damage, BaseUnit receiver)
    {
        if(receiver is StructureUnit)
            damage *= Mathf.RoundToInt((GetSpellProperty("Damage percentage to structures") / 100));
        else if(receiver is HeroUnit)
            damage *= Mathf.RoundToInt((GetSpellProperty("Damage percentage to heroes") / 100));
        else if(receiver is NeutralCreepUnit)
            damage *= Mathf.RoundToInt((GetSpellProperty("Damage percentage to neutrals") / 100));
    }
}

﻿using UnityEngine;
using System.Collections;

public class SpellBackdoorProtection : Spell
{
    public SpellBackdoorProtection()
    {
        OnSpellIsActive += SpellBackdoorProtection_OnSpellIsActive;
    }

    void SpellBackdoorProtection_OnSpellIsActive(BaseUnit caster, Spell spell)
    {
        BuffBackdoorProtectionChecker buff = (BuffBackdoorProtectionChecker)caster.ApplyBuff("Backdoor protection checker");
        buff.conditionDestroyedUnits = (caster as StructureUnit).conditionDestroyedUnits;
    }
}

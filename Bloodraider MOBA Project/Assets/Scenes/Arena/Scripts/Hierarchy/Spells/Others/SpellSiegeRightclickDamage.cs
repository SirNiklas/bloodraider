﻿using UnityEngine;
using System.Collections;

public class SpellSiegeRightclickDamage : Spell
{
    public SpellSiegeRightclickDamage()
    {
        OnSpellIsActive += SpellSiegeRightclickDamage_OnSpellIsActive;
    }

    void SpellSiegeRightclickDamage_OnSpellIsActive(BaseUnit caster, Spell spell)
    {
        caster.OnDoRightClickDamage += caster_OnDoRightClickDamage;
    }

    void caster_OnDoRightClickDamage(ref int damage, BaseUnit receiver)
    {
        if (receiver is LaneCreepUnit)
            damage *= Mathf.RoundToInt((GetSpellProperty("Damage percentage to lanecreeps") / 100));
        else if (receiver is HeroUnit)
            damage *= Mathf.RoundToInt((GetSpellProperty("Damage percentage to heroes") / 100));
        else if (receiver is NeutralCreepUnit)
            damage *= Mathf.RoundToInt((GetSpellProperty("Damage percentage to neutrals") / 100));
    }
}

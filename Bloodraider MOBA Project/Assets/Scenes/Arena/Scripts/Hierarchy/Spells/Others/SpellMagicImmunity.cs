﻿using UnityEngine;
using System.Collections;

public class SpellMagicImmunity : Spell
{
    public SpellMagicImmunity()
    {
        OnSpellIsActive += SpellMagicImmunity_OnSpellIsActive;
    }

    void SpellMagicImmunity_OnSpellIsActive(BaseUnit caster, Spell spell)
    {
        caster.SetState(State.MagicImmune);
    }
}

﻿using UnityEngine;
using System.Collections;

public class SpellUseBottle : Spell
{
    public SpellUseBottle()
    {
        OnCastedSpell += SpellUseBottle_OnCastedSpell;
    }

    void SpellUseBottle_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        caster.ApplyBuff("Bottle Regeneration");
    }
}

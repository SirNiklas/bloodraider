﻿using UnityEngine;
using System.Collections;

public class SpellUseHealingPotion : Spell
{
    public SpellUseHealingPotion()
    {
        OnCastedSpell += SpellUseHealingPotion_OnCastedSpell;
    }

    void SpellUseHealingPotion_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        target.ApplyBuff("Healing Potion Heal");
    }
}

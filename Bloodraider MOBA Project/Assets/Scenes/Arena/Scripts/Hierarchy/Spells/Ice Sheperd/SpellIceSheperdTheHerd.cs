﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpellIceSheperdTheHerd : Spell
{
    public GameObject iceSheperdCompanionGameObject;
    public Dictionary<IceSheperdCompanionMinionUnit, GameObject> companions;

    private IceSheperdHeroUnit caster;

    public SpellIceSheperdTheHerd()
    {
        companions = new Dictionary<IceSheperdCompanionMinionUnit, GameObject>();
        OnSpellIsActive += SpellIceSheperdTheHerd_OnSpellIsActive;
    }

    public void AddCompanion(IceSheperdHeroUnit caster)
    {
        if (companions.Count >= GetSpellProperty("Maximal companion count"))
            return;

        GameObject companion = caster.SummonMinion(iceSheperdCompanionGameObject, caster.transform.position);
        companion.GetComponent<IceSheperdCompanionMinionUnit>().iceSheperd = caster;

        companions.Add(companion.GetComponent<IceSheperdCompanionMinionUnit>(), companion);
    }
    public void RemoveCompanion(IceSheperdCompanionMinionUnit i, IceSheperdHeroUnit caster)
    {
        if (!companions.ContainsKey(i))
            return;

        caster.unitsUnderControl.Remove(i);
        companions.Remove(i);
    }

    void SpellIceSheperdTheHerd_OnSpellIsActive(BaseUnit caster, Spell spell)
    {
        caster.OnKillUnit += caster_OnKillUnit;
        this.caster = caster as IceSheperdHeroUnit;
    }

    void caster_OnKillUnit(ref BaseUnit killedUnit)
    {
        if (killedUnit is HeroUnit)
        {
            AddCompanion(caster);
            caster.ApplyBuff("The Herd Bonus");
        }
    }
}

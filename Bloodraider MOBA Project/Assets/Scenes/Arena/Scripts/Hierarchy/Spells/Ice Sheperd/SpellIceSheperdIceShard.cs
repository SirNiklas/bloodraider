﻿using UnityEngine;
using System.Collections;

public class SpellIceSheperdIceShard : Spell
{
    public SpellIceSheperdIceShard()
    {
        OnCastedSpell += SpellIceSheperdIceShard_OnCastedSpell;
    }

    void SpellIceSheperdIceShard_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        int damage = Mathf.RoundToInt(GetSpellProperty("Base damage") + ((caster is HeroUnit ? ((HeroUnit)caster).unitsUnderControl.Count - 1 : 1) * GetSpellProperty("Bonus minion damage")));
        target.ReceiveDamage(damage, DamageType.Magical, caster, false);

        target.ApplyLevelBuff("Iceshard Slow", this);
    }
}

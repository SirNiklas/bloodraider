﻿using UnityEngine;
using System.Collections;

public class SpellInfernalDominatorInfernalDevour : Spell
{
    public SpellInfernalDominatorInfernalDevour()
    {
        OnCastedSpell += SpellInfernalDominatorInfernalDevour_OnCastedSpell;
    }

    void SpellInfernalDominatorInfernalDevour_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        if (target is HeroUnit || target is StructureUnit)
            return; // TODO: Use error-messages so the user knows whats wrong when he casts the spell
        if (target.totalMaxHealth > 400)
            return;

        caster.ReceiveHeal(Mathf.RoundToInt(target.attributes[AttributeType.CurrentHealth] * GetSpellProperty("Heal Percentage")));
        (caster as HeroUnit).level.AddXp((int)GetSpellProperty("Bonus Xp"));

        int dmg = target.totalMaxHealth;
        target.ReceiveDamage(dmg, DamageType.Pure, caster, false);
    }

}

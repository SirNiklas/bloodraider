﻿using UnityEngine;
using System.Collections;

public class SpellInfernalDominatorHellsElite : Spell
{
    public GameObject hellsElitePrefab;

    public SpellInfernalDominatorHellsElite()
    {
        OnCastedSpell += SpellInfernalDominatorHellsElite_OnCastedSpell;
    }

    void SpellInfernalDominatorHellsElite_OnCastedSpell(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target)
    {
        for (int i = 0; i < (int)GetSpellProperty("Hells elite count"); i++)
        {
            InfernalDominatorHellsEliteMinionUnit h = caster.SummonMinion(hellsElitePrefab, new Vector3(caster.transform.position.x + i, caster.transform.position.y, caster.transform.position.z + i)).GetComponent<InfernalDominatorHellsEliteMinionUnit>();
            h.side = caster.side;
            h.baseDamage = (int)GetSpellProperty("Hells elite damage");
        }
    }
}


﻿using UnityEngine;
using System.Collections;

public class SpellVampireLifesteal : Spell
{
    private BaseUnit spellCaster;

    public SpellVampireLifesteal()
    {
        OnSpellIsActive += SpellVampireLifesteal_OnSpellIsActive;
    }

    void SpellVampireLifesteal_OnSpellIsActive(BaseUnit caster, Spell spell)
    {
        spellCaster = caster;
        caster.OnDoRightClickDamage += caster_OnDoRightClickDamage;
    }

    void caster_OnDoRightClickDamage(ref int damage, BaseUnit receiver)
    {
        int heal = Mathf.RoundToInt(damage * (GetSpellProperty("Lifesteal Percentage") / 100));
        if(spellCaster != null)
            spellCaster.ReceiveHeal(heal);
    }
}

﻿using UnityEngine;
using System.Collections;

public class Item : ScriptableObject
{
    public const float itemHalfPriceChangeTimer = 7;

    public string itemName, itemDescription, itemStatsDescription, statsBuffName, spellName;
    public Texture2D icon;

    public Buff statsBuffInstance;
    public Spell spellInstance;
    public BaseUnit owner, buyer;

    public int goldworth;
    public bool isHalfPriceSet;
    public float currentHalfPriceTimer;

    public delegate void OnCastSpellHandler(Vector3 targetposition, BaseUnit targetunit);
    public event OnCastSpellHandler OnCastSpell;
    public delegate void OnAddToRemoveFromInventoryHandler();
    public event OnAddToRemoveFromInventoryHandler OnAddToInventory, OnRemoveFromInventory;

    public static Item GetItem(string name)
    {
        return UnityEngine.Object.Instantiate(Resources.Load("Items/" + name)) as Item;
    }

    public void AddToInventory(BaseUnit b)
    {
        owner = b;

        // Statsbuff
        if(statsBuffName != "")
            statsBuffInstance = owner.ApplyBuff(statsBuffName);
        if (spellName != "")
        {
            spellInstance = Spell.GetSpell(spellName);
            spellInstance.LevelUp(owner);
        }

        if (OnAddToInventory != null)
            OnAddToInventory();
    }
    public void RemoveFromInventory()
    {
        if(statsBuffName != "")
            owner.UnapplyBuff(statsBuffName);

        owner = null;

        if (OnRemoveFromInventory != null)
            OnRemoveFromInventory();
    }

    public void Update()
    {
        if (isHalfPriceSet)
            return;

        currentHalfPriceTimer += Time.deltaTime;
        if (currentHalfPriceTimer >= itemHalfPriceChangeTimer)
        {
            goldworth /= 2;
            isHalfPriceSet = true;
            currentHalfPriceTimer = 0;
        }
    }

    public void CastSpell(Vector3 targetposition, BaseUnit targetunit)
    {
        if (spellInstance != null)
        {
            spellInstance.Cast(owner, targetposition, targetunit);
        }
    }
}

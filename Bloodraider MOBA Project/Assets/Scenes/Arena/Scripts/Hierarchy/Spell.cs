﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class Spell : ScriptableObject
{
    public string spellName, spellDescription;
    public Texture2D icon;
    public SpellProperty cooldown, range;
    public SpellProperty manaCost;
    public string castAnimationName;
    public int animatorAnimationModeIndex;
    public bool isUsable = true, isCasting, isUltimate, isCastableOnStructures, isSelfCastEnabled;
    public SpellTarget target;

    public AuraSettings auraSettings;

    public delegate bool OnPreCastSpellHandler();
    public event OnPreCastSpellHandler OnPreCastSpell;
    public delegate void OnCastedSpellHandler(BaseUnit caster, Spell spell, Vector3 position, BaseUnit target);
    public event OnCastedSpellHandler OnCastedSpell;
    public delegate void OnSpellIsActiveHandler(BaseUnit caster, Spell spell);
    public event OnSpellIsActiveHandler OnSpellIsActive;
    public delegate void OnLevelUpHandler(BaseUnit caster);
    public event OnLevelUpHandler OnLevelUp;

    public float currentCooldown = 0;

    public int maxLevel, currentLevel;
    public List<SpellProperty> spellProperties;

    private Vector3 clickedPosition;
    private BaseUnit clickedUnit;

    public static Spell GetSpell(string name)
    {
        return UnityEngine.Object.Instantiate(Resources.Load("Spells/" + name)) as Spell;
        //return GameAssetDatabase.Instance.spells[name].MemberwiseClone() as Spell;
    }

    public void Cast(BaseUnit caster, Vector3 targetpos, BaseUnit targetunit)
    {
        if (OnPreCastSpell != null)
        {
            if (!OnPreCastSpell())
            {
                Debug.Log("Spell-casting aborted: A special condition stopped the casting.");
                return;
            }
        }

        // TODO: Set screen messages when a spell cant be casted for this given reasons.
        if (!isCastableOnStructures && targetunit is StructureUnit)
        {
            Debug.Log("Spell-casting aborted: Spell is not castable on structures.");
        }
        if (currentLevel <= 0)
        {
            Debug.Log("Spell-casting aborted: Spell is not leveled.");
            return;
        }
        if ((target == SpellTarget.Passive || target == SpellTarget.Aura))
        {
            Debug.Log("Spell-casting aborted: Spell is passive or an aura.");
            return;
        }
        if (!isUsable)
        {
            Debug.Log("Spell-casting aborted: Spell is not usable.");
            return;
        }
        if (caster.attributes[AttributeType.CurrentMana] < manaCost.GetCurrentValue(currentLevel))
        {
            Debug.Log("Spell-casting aborted: You dont have enough mana to cast.");
            return;
        }
        if (currentCooldown != 0)
        {
            Debug.Log("Spell-casting aborted: Spell is on cooldown.");
            return;
        }
        if (targetunit == caster && !isSelfCastEnabled)
        {
            Debug.Log("Spell-casting aborted: The spell is not castable on yourself.");
        }
        if ((target != SpellTarget.NoTarget && target != SpellTarget.TargetPoint && targetunit == null))
        {
            Debug.Log("Spell-casting aborted: Spell is a targeted spell but no target was selected.");
            return;
        }
        if (target == SpellTarget.TargetAlly && caster.side != targetunit.side)
        {
            Debug.Log("Spell-casting aborted: Spell only castable on allies but the target is an enemy.");
            return;
        }
        if (target == SpellTarget.TargetEnemy && caster.side == targetunit.side)
        {
            Debug.Log("Spell-casting aborted: Spell only castable on enemies but the target is an ally.");
            return;
        }
        if ((target != SpellTarget.NoTarget && Vector3.Distance(caster.transform.position, targetpos) > range.GetCurrentValue(currentLevel)))
        {
            Debug.Log("Spell-casting aborted: Targetpoint is more far away than the max. castrange allows.");
            return;
        }
        if ((target == SpellTarget.Hero || target == SpellTarget.EnemyHero || target == SpellTarget.AllyHero) && !(targetunit is HeroUnit))
        {
            Debug.Log("Spell-casting aborted: Spell only castable on heroes.");
        }
        if (target == SpellTarget.AllyHero && targetunit.side != caster.side)
        {
            Debug.Log("Spell-casting aborted: Spell only castable on ally heroes.");
        }
        if (target == SpellTarget.EnemyHero && targetunit.side == caster.side)
        {
            Debug.Log("Spell-casting aborted: Spell only castable on enemy heroes.");
        }
        if ((target == SpellTarget.Creep || target == SpellTarget.EnemyCreep || target == SpellTarget.AllyCreep) && !((targetunit is LaneCreepUnit) || (targetunit is NeutralCreepUnit)))
        {
            Debug.Log("Spell-casting aborted: Spell only castable on creeps.");
        }
        if (target == SpellTarget.AllyCreep && targetunit.side != caster.side)
        {
            Debug.Log("Spell-casting aborted: Spell only castable on ally creeps.");
        }
        if (target == SpellTarget.EnemyCreep && targetunit.side == caster.side)
        {
            Debug.Log("Spell-casting aborted: Spell only castable on enemy creeps.");
        }

        currentCooldown = cooldown.GetCurrentValue(currentLevel);

        clickedPosition = targetpos;
        clickedUnit = targetunit;

        // Castanimation should get played to finish before OnCastedSpell() gets called

        caster.RemoveMana((int)manaCost.GetCurrentValue(currentLevel));
        if (OnCastedSpell != null)
            OnCastedSpell(caster, this, clickedPosition, clickedUnit);
    }

    public void UpdateSpell()
    {
        if (currentCooldown > 0)
        {
            currentCooldown -= Time.deltaTime;
            if (currentCooldown <= 0)
            {
                currentCooldown = 0;
            }
        }
    }
    public void UpdateAura(BaseUnit owner)
    {
        if (target != SpellTarget.Aura)
            return;
        if (currentLevel <= 0)
            return;

        auraSettings.DoAuraStuff(this, owner);
    }

    public float GetSpellProperty(string name)
    {
        SpellProperty s = spellProperties.Find((sp) => { if (sp.propertyName == name) return true; else return false; });

        if (s == null)
        {
            return 0;
        }
        else
        {
            return s.GetCurrentValue(currentLevel);
        }
    }
    public void LevelUp(BaseUnit levelupper)
    {
        if (currentLevel + 1 > maxLevel)
            return;

        currentLevel += 1;

        if (OnLevelUp != null)
            OnLevelUp(levelupper);

        if (currentLevel - 1 == 0)
        {
            if (OnSpellIsActive != null)
                OnSpellIsActive(levelupper, this);
        }
    }
    public string GetAllSpellPropertyInformation()
    {
        string spinfo = "";
        foreach (var sp in spellProperties)
        {
            spinfo += sp.propertyName + ": " + sp.ToString() + ", ";//"\n";
        }

        if (range != null)
        {
            if (!range.isFullZero)
            {
                spinfo += range.propertyName + ": " + range.ToString() + ", ";//"\n";
            }
        }

        return spinfo;
    }
    public void ResetCooldown()
    {
        currentCooldown = 0;
    }
    public void TriggerCooldown(float cooldown)
    {
        currentCooldown = cooldown;
    }
}

[Serializable]
public enum SpellTarget
{
    Passive,
    NoTarget,
    TargetPoint,
    TargetUnit,
    TargetAlly,
    TargetEnemy,
    Aura,
    AllyHero,
    EnemyHero,
    Hero,
    AllyCreep,
    EnemyCreep,
    Creep
}
[Serializable]
public class SpellProperty
{
    public string propertyName;
    public List<float> values;

    public bool isFullZero
    {
        get
        {
            bool fullzero = true;
            foreach (var val in values)
            {
                if (val != 0)
                    fullzero = false;
            }

            return fullzero;
        }
    }

    public SpellProperty(string name, List<float> vals)
    {
        propertyName = name;
        values = vals;
    }
    public float GetCurrentValue(int spelllevel)
    {
        if (values.Count < 1)
        {
            Debug.Log("This spell-property has no values. Returning 0.");
            return 0;
        }

        if (spelllevel - 1 < 0)
            return 0;
        else
            return values[spelllevel - 1];

    }

    public override string ToString()
    {
        string allvalues = "";
        foreach (var value in values)
        {
            allvalues += value.ToString() + "/";
        }
        if(allvalues.Length != 0)
            allvalues = allvalues.Remove(allvalues.Length - 1, 1);

        return allvalues;
    }
}
[Serializable]
public class AuraSettings
{
    public string buff;
    public float range;
    public AuraAffects affects;

    private List<BaseUnit> unitsInRange;

    public void DoAuraStuff(Spell auraspell, BaseUnit owner)
    {
        #region
        if (unitsInRange != null)
        {
            foreach (var unitinrange in unitsInRange)
            {
                if (unitinrange == null)
                    continue;
                if (Vector3.Distance(unitinrange.transform.position, owner.transform.position) > range)
                    unitinrange.ForceUnapplyBuff(buff);
            }
            unitsInRange.Clear();
        }
        else
            unitsInRange = new List<BaseUnit>();
        #endregion

        #region
        Collider[] cols = Physics.OverlapSphere(owner.transform.position, range);
        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                BaseUnit unit = col.GetComponent<BaseUnit>();
                if (affects == AuraAffects.Allies && unit.side == owner.side)
                    unitsInRange.Add(unit);
                else if (affects == AuraAffects.Enemies && unit.side != owner.side)
                    unitsInRange.Add(unit);
                else if (affects == AuraAffects.Neutral)
                    unitsInRange.Add(unit);
            }
        }
        foreach (var unit in unitsInRange)
        {
            unit.ApplyLevelBuff(buff, auraspell);
        }
        #endregion
    }

    [Serializable]
    public enum AuraAffects
    {
        Allies,
        Neutral,
        Enemies
    }
}

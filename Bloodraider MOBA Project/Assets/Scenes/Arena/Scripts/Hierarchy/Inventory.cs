﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Inventory
{
    public const float goldIncomePerSecond = 0.8f;

    public BaseUnit owner;
    public Item[] itemSlots;
    public int gold;

    private float currentGold;

    public void Initialize()
    {
        itemSlots = new Item[6];
        owner.OnUpdate += owner_OnUpdate;
    }

    void owner_OnUpdate()
    {
        if (!owner.isInventoryEnabled)
        {
            gold = 0;
            return;
        }
        currentGold += Time.deltaTime;
        if(currentGold >= goldIncomePerSecond)
        {
            currentGold = 0;
            gold += 1;
        }

        foreach (var itemslot in itemSlots)
        {
            if (itemslot == null)
                continue;

            itemslot.Update();
        }
    }

    public void AddItem(Item i, int slotindex)
    {
        if (!IsValidSlotIndex(slotindex))
        {
            Debug.Log("The item \"" + i.itemName + "\" cant be stored into the slot " + slotindex + " because the index is out of range.");
            return;
        }
        if (!owner.isInventoryEnabled)
        {
            Debug.Log("The inventory is disabled and no items cant be added to it.");
            return;
        }

        if (itemSlots[slotindex] != null && slotindex != 5)
        {
            AddItem(i, slotindex + 1);
            return;
        }
        else if(itemSlots[slotindex] != null && slotindex == 5)
        {
            Debug.Log("The inventory is full and the item \"" + i.itemName + "\" cant be added.");
            return;
        }

        i.AddToInventory(owner);
        itemSlots[slotindex] = i;

        owner.UpdateInventorySlot(i.itemName, slotindex);
    }
    public void RemoveItem(int slotindex)
    {
        if (!IsValidSlotIndex(slotindex))
            return;

        itemSlots[slotindex] = null;
    }

    public int GetSlotIndexByItemReference(Item b)
    {
        for (int i = 0; i < itemSlots.Length; i++)
        {
            if (Item.ReferenceEquals(itemSlots[i], b))
            {
                return i;
            }
        }

        return -1;
    }
    public Item GetItemReferenceBySlotIndex(int slotindex)
    {
        if (!IsValidSlotIndex(slotindex))
            return null;

        return itemSlots[slotindex];
    }
    public int GetFreeSlot()
    {
        for (int i = 0; i < itemSlots.Length; i++)
        {
            if (itemSlots[i] != null)
                return i;
        }

        return -1;
    }

    private bool IsValidSlotIndex(int slotindex)
    {
        if (slotindex > 5 || slotindex < 0)
            return false;
        else return true;
    }
}

﻿using UnityEngine;
using System.Collections;

public class TowerStructureUnit : StructureUnit
{
    public TowerStructureUnit()
    {
        OnAwake += TowerStructureUnit_OnAwake;
    }

    void TowerStructureUnit_OnAwake()
    {
        Spell s = Spell.GetSpell("Siege Rightclick Damage");
        spells.Add(s);
        s.LevelUp(this);

        Spell truesightaura = Spell.GetSpell("Truesight Aura");
        spells.Add(truesightaura);
        truesightaura.LevelUp(this);
    }
}

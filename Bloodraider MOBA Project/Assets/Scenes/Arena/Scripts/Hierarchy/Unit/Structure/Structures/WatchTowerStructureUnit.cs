﻿using UnityEngine;
using System.Collections;

public class WatchTowerStructureUnit : StructureUnit
{
    public WatchTowerStructureUnit()
    {
        OnAwake += WatchTowerStructureUnit_OnAwake;
        OnIsDead += WatchTowerStructureUnit_OnIsDead;
    }

    void WatchTowerStructureUnit_OnIsDead(ref BaseUnit killer)
    {
        SystemGlyphOfDefence.Instance.ActivateOnlyGlyphCooldown(side);
    }

    void WatchTowerStructureUnit_OnAwake()
    {
        Spell s = Spell.GetSpell("Siege Rightclick Damage");
        spells.Add(s);
        s.LevelUp(this);

        Spell magicimmunity = Spell.GetSpell("Magic Immunity");
        spells.Add(magicimmunity);
        magicimmunity.LevelUp(this);
    }

    public override void CheckAutoAttackAggro()
    {
        if (!autoAttack)
            return;
        if (Network.isClient)
            return;
        if (isDead)
            return;

        Collider[] cols = Physics.OverlapSphere(transform.position, autoAttackAggroRange);
        BaseUnit aggroHighPriority = this;
        BaseUnit aggroNormalPriority = this;

        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                BaseUnit b = col.GetComponent<BaseUnit>();
                if (b.side != side && b != this)
                {
                    if (b is LaneCreepUnit || b is NeutralCreepUnit || b is MinionUnit)
                        aggroHighPriority = b;
                    else if (b is HeroUnit)
                        aggroNormalPriority = b;
                }
            }
        }

        if (aggroHighPriority == lockedTarget)
            return;
        else if (aggroNormalPriority == lockedTarget)
            return;

        if (aggroHighPriority.side == UnitSide.Neutral)
            return;
        if (aggroNormalPriority.side == UnitSide.Neutral)
            return;

        if (aggroHighPriority != this && aggroHighPriority.state != State.Invulnerable && (!aggroHighPriority.isInvisible || aggroHighPriority.isInvisibilityRevealed))
            DoRightClickDamage(aggroHighPriority);
        else if (aggroNormalPriority != this && aggroNormalPriority.state != State.Invulnerable && (!aggroNormalPriority.isInvisible || aggroNormalPriority.isInvisibilityRevealed))
            DoRightClickDamage(aggroNormalPriority);
    }
}

﻿using UnityEngine;
using System.Collections;

public class ThroneStructureUnit : StructureUnit
{
    public ThroneStructureUnit()
    {
        OnAwake += ThroneStructureUnit_OnAwake;
        OnIsDead += ThroneStructureUnit_OnIsDead;
    }

    void ThroneStructureUnit_OnAwake()
    {
        Spell truesightaura = Spell.GetSpell("Truesight Aura");
        spells.Add(truesightaura);
        truesightaura.LevelUp(this);
    }

    void ThroneStructureUnit_OnIsDead(ref BaseUnit killer)
    {
        if (side == UnitSide.Green)
        {
            // Win for the red
        }
        else if (side == UnitSide.Red)
        {
            // Win for the green
        }
    }
}

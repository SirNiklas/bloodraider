﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StructureUnit : BaseUnit
{
    public const float autoAttackAggroRange = 36;

    public List<StructureUnit> conditionDestroyedUnits;
    private Vector3 startPosition;

    public StructureUnit()
    {
        OnAwake += StructureUnit_OnAwake;
        OnUpdate += StructureUnit_OnUpdate;
    }

    void StructureUnit_OnUpdate()
    {
        this.transform.position = startPosition;
    }

    void StructureUnit_OnAwake()
    {
        startPosition = this.transform.position;
        navMeshAgentComponent.Stop();

        Spell bdp = Spell.GetSpell("Backdoor protection");
        spells.Add(bdp);
        bdp.LevelUp(this);
    }

    public override void CheckAutoAttackAggro()
    {
        if (!autoAttack)
            return;
        if (Network.isClient)
            return;
        if (isDead)
            return;

        Collider[] cols = Physics.OverlapSphere(transform.position, autoAttackAggroRange);
        BaseUnit aggroHighPriority = this;
        BaseUnit aggroNormalPriority = this;

        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                BaseUnit b = col.GetComponent<BaseUnit>();
                if (b.side != side && b != this)
                {
                    if (b is LaneCreepUnit || b is NeutralCreepUnit || b is MinionUnit)
                        aggroHighPriority = b;
                    else if (b is HeroUnit)
                        aggroNormalPriority = b;
                }
            }
        }

        if (aggroHighPriority == lockedTarget)
            return;
        else if (aggroNormalPriority == lockedTarget)
            return;

        if (aggroHighPriority != this && aggroHighPriority.state != State.Invulnerable && (!aggroHighPriority.isInvisible || aggroHighPriority.isInvisibilityRevealed))
            DoRightClickDamage(aggroHighPriority);
        else if (aggroNormalPriority != this && aggroNormalPriority.state != State.Invulnerable && (!aggroNormalPriority.isInvisible || aggroNormalPriority.isInvisibilityRevealed))
            DoRightClickDamage(aggroNormalPriority);
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

public class LaneCreepUnit : BaseUnit
{
    public const float autoAttackAggroRange = 28;

    public Lane lane;
    public bool isAfterLaneEdge;

    public LaneCreepUnit()
    {
        OnAwake += LaneCreepUnit_OnAwake;
        OnKillUnit += LaneCreepUnit_OnKillUnit;
    }

    void LaneCreepUnit_OnAwake()
    {
        if (this is GreenWizardCreepUnit || this is RedWizardCreepUnit)
        {
            Spell s = Spell.GetSpell("Piercing Rightclick Damage");

            spells.Add(s);
            s.LevelUp(this);
        }
    }

    void LaneCreepUnit_OnKillUnit(ref BaseUnit killedUnit)
    {
        Go();
    }

    public void Go()
    {
        if (isAfterLaneEdge)
            GoToEnemyThrone();
        else
            GoToLaneEdge();

    }
    private void GoToEnemyThrone()
    {
        if (side == UnitSide.Green)
        {
            DoRightClickDamage(GameManagementManager.Instance.redThrone);
        }
        else if (side == UnitSide.Red)
        {
            DoRightClickDamage(GameManagementManager.Instance.greenThrone);
        }
    }
    private void GoToLaneEdge()
    {
        if (lane == Lane.Top)
        {
            GotoPosition(LaneCreepManager.Instance.topLaneEdge.position);
        }
        else if (lane == Lane.Mid)
        {
            GotoPosition(LaneCreepManager.Instance.midLaneEdge.position);
        }

        else if (lane == Lane.Bot)
        {
            GotoPosition(LaneCreepManager.Instance.botLaneEdge.position);
        }
    }

    public override void CheckAutoAttackAggro()
    {
        if (!autoAttack)
            return;
        if (Network.isClient)
            return;
        if (isDead)
            return;

        Collider[] cols = Physics.OverlapSphere(transform.position, autoAttackAggroRange);
        BaseUnit aggroHighPriority = this;
        BaseUnit aggroNormalPriority = this;

        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                BaseUnit b = col.GetComponent<BaseUnit>();
                if (b.side != side && b != this)
                {
                    if (b is LaneCreepUnit || b is TowerStructureUnit || b is NeutralCreepUnit)
                        aggroHighPriority = b;
                    else if (b is HeroUnit || b is MinionUnit || b is ThroneStructureUnit)
                        aggroNormalPriority = b;
                }
            }
        }

        if (aggroHighPriority == lockedTarget)
            return;
        else if (aggroNormalPriority == lockedTarget)
            return;

        if (aggroHighPriority != this && (!aggroHighPriority.isInvisible || aggroHighPriority.isInvisibilityRevealed))
        {
            DoRightClickDamage(aggroHighPriority);
        }
        else if (aggroNormalPriority != this && (!aggroNormalPriority.isInvisible || aggroNormalPriority.isInvisibilityRevealed))
        {
            //if (lockedTarget != null)
            //{
            //    if (aggroNormalPriority.GetType() == lockedTarget.GetType())
            //        return;
            //}

            DoRightClickDamage(aggroNormalPriority);
        }
        else if (aggroHighPriority == this && aggroNormalPriority == this)
            Go();
    }
}

[Serializable]
public enum Lane
{
    Top,
    Mid,
    Bot
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NeutralCreepUnit : BaseUnit
{
    public const float aggroTime = 7.5f, autoAttackAggroRange = 8, triggerSpellCastRange = 14, triggerSpellUnitCountMinimum = 3;

    private Vector3 startPoint;
    private float currentAggroTime;

    public string castedSpellWhenUnitsNear;
    public bool isNearStartPoint;

    public NeutralCreepUnit()
    {
        OnUpdate += NeutralCreepUnit_OnUpdate;
        OnReceiveDamage += NeutralCreepUnit_OnReceiveDamage;
        OnStart += NeutralCreepUnit_OnStart;
    }

    void NeutralCreepUnit_OnStart()
    {
        InvokeRepeating("DoCastSpellIfEnemiesNearRoutine", 0, 1);
    }

    void NeutralCreepUnit_OnReceiveDamage(ref int damage, ref DamageType dt, ref BaseUnit dd)
    {
        DoRightClickDamage(dd);
    }

    void NeutralCreepUnit_OnUpdate()
    {
        if (Vector3.Distance(startPoint, transform.position) < 3)
        {
            isNearStartPoint = true;
        }
        else
            isNearStartPoint = false;

        if (!isNearStartPoint)
        {
            currentAggroTime += Time.deltaTime;
            if (currentAggroTime >= aggroTime)
            {
                DoRightClickDamage(null);
                GotoPosition(startPoint);

                currentAggroTime = 0;
            }
        }
    }

    public override void CheckAutoAttackAggro()
    {
        if (!autoAttack)
            return;
        if (Network.isClient)
            return;
        if (isDead)
            return;

        if (isNearStartPoint)
        {
            Collider[] cols = Physics.OverlapSphere(transform.position, autoAttackAggroRange);
            BaseUnit aggroHighPriority = this;
            BaseUnit aggroNormalPriority = this;

            foreach (var col in cols)
            {
                if (col.CompareTag("Unit"))
                {
                    BaseUnit b = col.GetComponent<BaseUnit>();
                    if (b.side != side && b != this)
                    {
                        if (b is LaneCreepUnit || b is MinionUnit)
                            aggroHighPriority = b;
                        else if (b is HeroUnit || b is NeutralCreepUnit || b is StructureUnit)
                            aggroNormalPriority = b;

                        currentAggroTime = 0;
                    }
                }
            }

            if (aggroHighPriority == lockedTarget)
                return;
            else if (aggroNormalPriority == lockedTarget)
                return;

            if (aggroHighPriority != this && aggroHighPriority.state != State.Invulnerable && (!aggroHighPriority.isInvisible || aggroHighPriority.isInvisibilityRevealed))
                DoRightClickDamage(aggroHighPriority);
            else if (aggroNormalPriority != this && aggroNormalPriority.state != State.Invulnerable && (!aggroNormalPriority.isInvisible || aggroNormalPriority.isInvisibilityRevealed))
                DoRightClickDamage(aggroNormalPriority);
        }
    }

    public void InitializeNeutralCreep()
    {
        startPoint = transform.position;
    }

    private void DoCastSpellIfEnemiesNearRoutine()
    {
        if (Network.isClient)
            return;
        if (castedSpellWhenUnitsNear == "")
            return;
        if(isDead)
            return;
        if(isStunned)
            return;
        if(isSilenced)
            return;

        Collider[] cols = Physics.OverlapSphere(transform.position, triggerSpellCastRange);
        
        List<BaseUnit> units = new List<BaseUnit>();
        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                units.Add(col.GetComponent<BaseUnit>());
            }
        }

        if (units.Count >= triggerSpellUnitCountMinimum)
        {
            CastSpell(castedSpellWhenUnitsNear, units[1].transform.position, units[1]);
        }
    }
}

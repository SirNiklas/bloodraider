﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VampireNeutralCreepUnit : NeutralCreepUnit
{
    public VampireNeutralCreepUnit()
    {
        OnAwake += VampireNeutralCreepUnit_OnAwake;
    }

    void VampireNeutralCreepUnit_OnAwake()
    {
        Spell lifesteal = Spell.GetSpell("Vampire Lifesteal");
        lifesteal.LevelUp(this);

        spells.Add(lifesteal);
    }
}

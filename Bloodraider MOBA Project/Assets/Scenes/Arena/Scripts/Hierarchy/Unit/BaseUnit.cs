﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;

public abstract class BaseUnit : MonoBehaviour
{
    public static List<BaseUnit> allUnits = new List<BaseUnit>();
    public const string animationIdleName = "idle", animationRunName = "run", animationDieName = "die", animationAttackBaseName = "attack";
    public const float xpBountyReceiveRange = 53, uphillToleranceHeight = 0.8f;
    public const int rightClickAttackUphillMisschance = 25;

    public string unitName;
    public Texture2D portraitIcon;

    public GameObject fullObject;
    public Animation animationComponent;
    public NavMeshAgent navMeshAgentComponent;
    public Animator animator;

    public int baseMaxHealth, baseMaxMana, baseHealthRegeneration, baseManaRegeneration, baseStrength, baseAgility, baseIntelligence, baseMovementspeed, baseAttackRange, baseAttackSpeed, baseDamage, baseMagicResistance, baseArmor;
    public float baseAttackTime, attackTime, currentAttackTime;

    public List<BaseUnit> unitsUnderControl;
    public List<Spell> spells;
    public Inventory inventory;

    public PrimaryAttribute primaryAttribute;
    public UnitSide side;
    public State state;
    public AnimationState animationState;
    public AttackMode attackMode;
    public GameObject rangedProjectile;
    public Transform rangedProjectileSpawn;
    public Dictionary<AttributeType, int> attributes;
    public List<Buff> buffs;
    public bool isStunned, isDead, isSilenced, canAttack, autoAttack, isInvisible, isInvisibilityRevealed, canMissFromUphillTargets = true, isInventoryEnabled, isBuyingEnabled;
    public BaseUnit lockedTarget;
    public int goldBounty, xpBounty;
    public float sightRangeDay, sightRangeNight;
    public List<Projectile> projectiles;
    public Spell lastCastedSpell;

    public bool hasFullHealth
    {
        get { return (attributes[AttributeType.CurrentHealth] >= totalMaxHealth); }
    }
    public bool hasFullMana
    {
        get { return (attributes[AttributeType.CurrentMana] >= totalMaxMana); }
    }
    public int totalStrength
    {
        get { return attributes[AttributeType.BaseStrength] + attributes[AttributeType.BonusStrength]; }
    }
    public int totalAgility
    {
        get { return attributes[AttributeType.BaseAgility] + attributes[AttributeType.BonusAgility]; }
    }
    public int totalIntelligence
    {
        get { return attributes[AttributeType.BaseIntelligence] + attributes[AttributeType.BonusIntelligence]; }
    }
    public int totalMaxHealth
    {
        get { return attributes[AttributeType.BaseMaxHealth] + attributes[AttributeType.BonusMaxHealth]; }
    }
    public int totalMaxMana
    {
        get { return attributes[AttributeType.BaseMaxMana] + attributes[AttributeType.BonusMaxMana]; }
    }
    public int totalHealthRegeneration
    {
        get { return attributes[AttributeType.BaseHealthRegeneration] + attributes[AttributeType.BonusHealthRegeneration]; }
    }
    public int totalManaRegeneration
    {
        get { return attributes[AttributeType.BaseManaRegeneration] + attributes[AttributeType.BonusManaRegeneration]; }
    }
    public int totalDamage
    {
        get { return attributes[AttributeType.BaseDamage] + attributes[AttributeType.BonusDamage]; }
    }
    public int totalAttackSpeed
    {
        get { return attributes[AttributeType.BaseAttackSpeed] + attributes[AttributeType.BonusAttackSpeed]; }
    }
    public int totalArmor
    {
        get { return attributes[AttributeType.BaseArmor] + attributes[AttributeType.BonusArmor]; }
    }
    public int totalArmorPercentage
    {
        get { return Mathf.RoundToInt(((0.05f * (float)totalArmor) / (1 + 0.06f * (float)totalArmor) * 100)); }
    }
    public int totalMagicResistance
    {
        get { return attributes[AttributeType.BaseMagicResistance] + attributes[AttributeType.BonusMagicResistance]; }
    }

    public delegate void OnMessageMethodCalledHandler();
    public event OnMessageMethodCalledHandler OnAwake, OnStart, OnUpdate, OnGetDestroyed;
    public delegate void OnReceiveDamageHandler(ref int damage, ref DamageType dt, ref BaseUnit dd);
    public event OnReceiveDamageHandler OnReceiveDamage;
    public delegate void OnDoRightClickDamageHandler(ref int damage, BaseUnit receiver);
    public event OnDoRightClickDamageHandler OnDoRightClickDamage;
    public delegate void OnIsDeadHandler(ref BaseUnit killer);
    public event OnIsDeadHandler OnIsDead;
    public delegate void OnGetStunnedHandler(ref bool value);
    public event OnGetStunnedHandler OnGetStunned;
    public delegate void OnKillUnitHandler(ref BaseUnit killedUnit);
    public event OnKillUnitHandler OnKillUnit;
    public delegate void OnReceiveHealHandler(ref int heal);
    public event OnReceiveHealHandler OnReceiveHeal;

    void Awake()
    {
        allUnits.Add(this);
        AttributeManager.Instance.InitializeAttributes(this);

        OnKillUnit += BaseUnit_OnKillUnit;
        OnUpdate += BaseUnit_OnUpdate;
        OnAwake += BaseUnit_OnAwake;

        if (OnAwake != null)
            OnAwake();
    }
    void Start()
    {
        AttributeManager.Instance.UpdateAttributes(this);
        attributes[AttributeType.CurrentHealth] = totalMaxHealth;
        attributes[AttributeType.CurrentMana] = totalMaxMana;

        InvokeRepeating("CheckAutoAttackAggro", 1, 0.28f);
        inventory.Initialize();

        if (OnStart != null)
            OnStart();
    }
    void Update()
    {
        if (OnUpdate != null)
            OnUpdate();

        if (isStunned)
            navMeshAgentComponent.Stop();
        else
            navMeshAgentComponent.Resume();


        if (attributes[AttributeType.CurrentHealth] > totalMaxHealth)
            attributes[AttributeType.CurrentHealth] = totalMaxHealth;
        if (attributes[AttributeType.CurrentMana] > totalMaxMana)
            attributes[AttributeType.CurrentMana] = totalMaxMana;

        AttributeManager.Instance.UpdateAttributes(this);
        AnimationManager.Instance.UpdateAnimations(this);
        SpellManager.Instance.UpdateSpells(this);

        navMeshAgentComponent.speed = attributes[AttributeType.MovementSpeed];
        OnEverySecondHandler();
        TryDoRightClickDamageToTarget();
    }
    void IsDead(BaseUnit killer)
    {
        if (isDead)
            return;

        if (killer.OnKillUnit != null)
        {
            BaseUnit me = this;
            killer.OnKillUnit(ref me);
        }

        #region Kill rewards
        if (!(this is MinionUnit))
        {
            killer.inventory.gold += goldBounty;

            Collider[] cols = Physics.OverlapSphere(this.transform.position, xpBountyReceiveRange);
            List<HeroUnit> heroes = new List<HeroUnit>();
            foreach (var col in cols)
            {
                if (col.CompareTag("Unit"))
                {
                    BaseUnit b = col.GetComponent<BaseUnit>();
                    if (b.side != this.side && b is HeroUnit && !b.isDead)
                        heroes.Add(b as HeroUnit);
                }
            }

            foreach (var hero in heroes)
            {
                hero.level.AddXp(xpBounty / heroes.Count);
            }
        }
        #endregion

        if (OnIsDead != null)
            OnIsDead(ref killer);

        isDead = true;
        animationState = AnimationState.Die;
        SetState(State.Invulnerable);

        DoRightClickDamage(null);
        navMeshAgentComponent.Stop();

        if (!(this is HeroUnit))
        {
            Destroy(gameObject, 2);
            Network.RemoveRPCs(networkView.viewID);
        }
    }
    void OnDestroy()
    {
        allUnits.Remove(this);
        if (OnGetDestroyed != null)
            OnGetDestroyed();
    }
    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        if (this is StructureUnit)
            return;

        Vector3 currentDestination = Vector3.zero; //, currentPosition = Vector3.zero;
        if (stream.isWriting)
        {
            currentDestination = navMeshAgentComponent.destination;
            stream.Serialize(ref currentDestination);

            //currentPosition = transform.position;
            //stream.Serialize(ref currentPosition);
        }
        else if (stream.isReading)
        {
            stream.Serialize(ref currentDestination);
            navMeshAgentComponent.SetDestination(currentDestination);

            //stream.Serialize(ref currentPosition);
            //transform.position = currentPosition;
        }
    }

    void BaseUnit_OnAwake()
    {
        SetLocalRenderVisibility(false);
    }
    void BaseUnit_OnUpdate()
    {

    }
    void BaseUnit_OnKillUnit(ref BaseUnit killedUnit)
    {
        if (lockedTarget == killedUnit)
            DoRightClickDamage(null);
    }

    public static BaseUnit FindLocalBaseUnitByNetworkViewID(NetworkViewID id)
    {
        foreach (var unit in allUnits)
        {
            if (unit.networkView != null)
            {
                if (unit.networkView.viewID == id)
                    return unit;
                else continue;
            }
        }

        return null;
    }

    public void SetLocalRenderVisibility(bool value)
    {
        if(!(this is StructureUnit))
            fullObject.SetActive(value);
    }

    //public void SetInvisibleState(bool value)
    //{
    //    networkView.RPC("SetInvisibleStateRPC", RPCMode.All, value);
    //}
    //[RPC]
    //private void SetInvisibleStateRPC(bool value)
    //{
    //    isInvisible = value;
    //}
    public void SetInvisibleState(bool value)
    {
        isInvisible = value;

        if (isInvisible && !isInvisibilityRevealed)
            DisjointProjectiles(false);
    }

    public void RevealInvisibility(bool value)
    {
        isInvisibilityRevealed = value;
    }

    public GameObject SummonMinion(GameObject miniongo, Vector3 position)
    {
        // TODO: Use Network.Instantiate maybe so the networkview-viewid gets applied? Its probably getting applied anyway

        GameObject minion = (GameObject)Instantiate(miniongo, position, Quaternion.identity);
        minion.GetComponent<MinionUnit>().side = this.side;
        minion.GetComponent<MinionUnit>().owner = this;
        this.unitsUnderControl.Add(minion.GetComponent<MinionUnit>());

        return minion;
    }
    public GameObject CreateIllusion(float livetime, int bonusdamage, int dealtdamage, Vector3 offset)
    {
        GameObject illusion = (GameObject)Instantiate(GameManagementManager.Instance.illusionBaseObject, transform.position + offset, Quaternion.identity);
        illusion.GetComponent<IllusionUnit>().Initialize(livetime, bonusdamage, dealtdamage, gameObject);
        this.unitsUnderControl.Add(illusion.GetComponent<IllusionUnit>());

        return illusion;
    }

    public void UpdateInventorySlot(string item, int slotindex)
    {
        networkView.RPC("UpdateInventorySlotRPC", RPCMode.Others, item, slotindex);
    }
    [RPC]
    private void UpdateInventorySlotRPC(string item, int slotindex)
    {
        inventory.AddItem(Item.GetItem(item), slotindex);
    }

    public void CastSpell(string spell, Vector3 castpos, BaseUnit castunit)
    {
        if (isSilenced)
            return;
        if (isStunned)
            return;
        if (isDead)
            return;

        Spell s = spells.Find((_s) => { if (_s.spellName == spell) return true; else return false; });
        if (s == null)
        {
            foreach (var itemslot in inventory.itemSlots)
            {
                if (itemslot.spellName == spell)
                {
                    networkView.RPC("CastSpellRPC", RPCMode.All,
                        spell,
                        castpos,
                        castunit != null ? castunit.networkView.viewID : NetworkViewID.unassigned);
                    return;
                }
            }

            return;
        }

        networkView.RPC("CastSpellRPC", RPCMode.All,
            spell,
            castpos,
            castunit != null ? castunit.networkView.viewID : NetworkViewID.unassigned);
    }
    [RPC]
    private void CastSpellRPC(string spell, Vector3 castpos, NetworkViewID castunitviewid)
    {
        Spell s = spells.Find((_s) => { if (_s.spellName == spell) return true; else return false; });
        if (s == null)
        {
            foreach (var itemslot in inventory.itemSlots)
            {
                if (itemslot.spellName == spell)
                {
                    itemslot.CastSpell(castpos, FindLocalBaseUnitByNetworkViewID(castunitviewid));
                    return;
                }
            }
        }
        else
        {
            ResetDoRightClickDamageRPC();
            s.Cast(this, castpos, FindLocalBaseUnitByNetworkViewID(castunitviewid));

            lastCastedSpell = s;
            animationState = AnimationState.SpellCast;
        }
    }

    //public void ReceiveDamage(int damage, DamageType dt, BaseUnit dd, bool affectsstructures)
    //{
    //    networkView.RPC("ReceiveDamageRPC", RPCMode.All, damage, dt.ToString(), dd != null ? dd.networkView.viewID : NetworkViewID.unassigned, affectsstructures);
    //}
    //[RPC]
    //private void ReceiveDamageRPC(int damage, string damagetype, NetworkViewID ddviewid, bool affectsstructures)
    //{
    //    if (state == State.Invulnerable)
    //        return;
    //    if (!affectsstructures && this is StructureUnit)
    //        return;

    //    DamageType dt = (DamageType)Enum.Parse(typeof(DamageType), damagetype);

    //    int finaldamage = 0;
    //    if (dt == DamageType.Physical)
    //    {
    //        if (state == State.Ethereal)
    //            return;
    //        else
    //        {
    //            finaldamage = Mathf.RoundToInt(damage - (damage * ((float)totalArmorPercentage / 100)));
    //        }
    //    }
    //    else if(dt == DamageType.Magical)
    //    {
    //        if (state == State.MagicImmune)
    //            return;

    //        finaldamage = Mathf.RoundToInt(damage - (damage * ((float)totalMagicResistance / 100)));
    //        if (state == State.Ethereal)
    //            finaldamage = Mathf.RoundToInt(finaldamage * 1.4f);
    //    }
    //    else if (dt == DamageType.Pure)
    //    {
    //        finaldamage = damage;
    //    }
    //    else if (dt == DamageType.Composite)
    //    {
    //        int d1 = damage / 2, d2 = d1;

    //        d1 -= Mathf.RoundToInt(d1 * ((float)totalArmorPercentage / 100));
    //        d2 -= Mathf.RoundToInt(d2 * ((float)totalMagicResistance / 100));

    //        finaldamage = d1 + d2;
    //    }

    //    if (finaldamage < 0)
    //        finaldamage = 0;

    //    attributes[AttributeType.CurrentHealth] -= finaldamage;

    //    damage = finaldamage;

    //    BaseUnit dd = FindLocalBaseUnitByNetworkViewID(ddviewid);
    //    if (OnReceiveDamage != null)
    //    {
    //        OnReceiveDamage(ref damage, ref dt, ref dd);
    //    }

    //    if (attributes[AttributeType.CurrentHealth] < 0)
    //        attributes[AttributeType.CurrentHealth] = 0;

    //    if (IsDead())
    //    {
    //        IsDead(dd);
    //    }
    //}
    public void ReceiveDamage(int damage, DamageType dt, BaseUnit dd, bool affectsstructures)
    {
        if (state == State.Invulnerable)
            return;
        if (!affectsstructures && this is StructureUnit)
            return;

        int finaldamage = 0;
        if (dt == DamageType.Physical)
        {
            if (state == State.Ethereal)
                return;
            else
            {
                finaldamage = Mathf.RoundToInt(damage - (damage * ((float)totalArmorPercentage / 100)));
            }
        }
        else if (dt == DamageType.Magical)
        {
            if (state == State.MagicImmune)
                return;

            finaldamage = Mathf.RoundToInt(damage - (damage * ((float)totalMagicResistance / 100)));
            if (state == State.Ethereal)
                finaldamage = Mathf.RoundToInt(finaldamage * 1.4f);
        }
        else if (dt == DamageType.Pure)
        {
            finaldamage = damage;
        }
        else if (dt == DamageType.Composite)
        {
            int d1 = damage / 2, d2 = d1;

            d1 -= Mathf.RoundToInt(d1 * ((float)totalArmorPercentage / 100));
            d2 -= Mathf.RoundToInt(d2 * ((float)totalMagicResistance / 100));

            finaldamage = d1 + d2;
        }

        if (finaldamage < 0)
            finaldamage = 0;

        attributes[AttributeType.CurrentHealth] -= finaldamage;

        damage = finaldamage;
        if (OnReceiveDamage != null)
        {
            OnReceiveDamage(ref damage, ref dt, ref dd);
        }

        if (attributes[AttributeType.CurrentHealth] < 0)
            attributes[AttributeType.CurrentHealth] = 0;

        if (IsDead())
        {
            IsDead(dd);
        }
    }

    //public void ReceiveHeal(int heal)
    //{
    //    networkView.RPC("ReceiveHealRPC", RPCMode.All, heal);
    //}
    //[RPC]
    //private void ReceiveHealRPC(int heal)
    //{
    //    if (attributes[AttributeType.CurrentHealth] + heal > totalMaxHealth)
    //        attributes[AttributeType.CurrentHealth] = totalMaxHealth;
    //    else
    //        attributes[AttributeType.CurrentHealth] += heal;
    //}
    //private void ReceiveHealRPC(int heal)
    //{
    //    if (attributes[AttributeType.CurrentHealth] + heal > totalMaxHealth)
    //        attributes[AttributeType.CurrentHealth] = totalMaxHealth;
    //    else
    //        attributes[AttributeType.CurrentHealth] += heal;
    //}
    public void ReceiveHeal(int heal)
    {
        if (OnReceiveHeal != null)
            OnReceiveHeal(ref heal);

        if (attributes[AttributeType.CurrentHealth] + heal > totalMaxHealth)
            attributes[AttributeType.CurrentHealth] = totalMaxHealth;
        else
            attributes[AttributeType.CurrentHealth] += heal;
    }

    public void ReceiveMana(int mana)
    {
        if (attributes[AttributeType.CurrentMana] + mana > totalMaxMana)
            attributes[AttributeType.CurrentMana] = totalMaxMana;
        else
            attributes[AttributeType.CurrentMana] += mana;
    }

    //public void RemoveMana(int mana)
    //{
    //    networkView.RPC("RemoveManaRPC", RPCMode.All, mana);
    //}
    //[RPC]
    public void RemoveMana(int mana)
    {
        if (attributes[AttributeType.CurrentMana] - mana < 0)
            attributes[AttributeType.CurrentMana] = 0;
        else
            attributes[AttributeType.CurrentMana] -= mana;
    }

    public void DoRightClickDamage(BaseUnit target)
    {
        if (lockedTarget == target)
            return;
        if (isDead)
            return;

        if (target == null)
            networkView.RPC("ResetDoRightClickDamageRPC", RPCMode.All);
        else if(target != lockedTarget && !isDead)
            networkView.RPC("DoRightClickDamageRPC", RPCMode.All, target.networkView.viewID);
    }
    [RPC]
    private void DoRightClickDamageRPC(NetworkViewID targetviewid)
    {
        BaseUnit target = FindLocalBaseUnitByNetworkViewID(targetviewid);
        if (target != null)
        {
            if (target.side == side)
            {
                ResetDoRightClickDamageRPC();
                return;
            }
        }

        lockedTarget = target;
    }
    [RPC]
    public void ResetDoRightClickDamageRPC()
    {
        lockedTarget = null;
    }

    public void ToggleStun(bool value)
    {
        if (state == State.MagicImmune && value)
            return;

        if (OnGetStunned != null)
            OnGetStunned(ref value);

        isStunned = value;
        if (isStunned)
            navMeshAgentComponent.Stop();
        else
            navMeshAgentComponent.Resume();
    }

    public void GotoPosition(Vector3 position)
    {
        animationState = AnimationState.Run;
        navMeshAgentComponent.SetDestination(position);
    }
    public void SetState(State s)
    {
        state = s;

        if (state == State.MagicImmune)
        {
            PurgeDebuffs(true);
        }
    }

    public void PurgeDebuffs(bool purgeall)
    {
        foreach (var buff in buffs)
        {
            if (purgeall)
                UnapplyBuff(buff.buffName);
            else if (buff.isDebuff)
                UnapplyBuff(buff.buffName);
        }
    }

    public void PlaySound(AudioClip clip)
    {
        audio.PlayOneShot(clip);
    }

    public Buff ApplyBuff(string buff)
    {
        Buff b = Buff.GetBuff(buff);
        if (!b.worksOnStructures && this is StructureUnit)
            return null;

        Buff duplicate = buffs.Find((tb) => { if (tb.buffName == b.buffName) return true; else return false; });
        if (duplicate != null)
        {
            if (!b.isStackable)
            {
                duplicate.currentLivetime = duplicate.livetime;
                return duplicate;
            }
            
        }

        b.Apply(this);
        buffs.Add(b);

        return b;
    }
    public LevelBuff ApplyLevelBuff(string buff, Spell sourcespell)
    {
        LevelBuff b = Buff.GetBuff(buff) as LevelBuff;
        if (b == null)
            return null;
        if (!b.worksOnStructures && this is StructureUnit)
            return null;

        b.spell = sourcespell;

        Buff duplicate = buffs.Find((tb) => { if (tb.buffName == b.buffName) return true; else return false; });
        if (duplicate != null)
        {
            if (!b.isStackable)
            {
                duplicate.currentLivetime = duplicate.livetime;
                return null;
            }

        }

        b.Apply(this);
        buffs.Add(b);

        return b;
    }
    public void UnapplyBuff(string buff)
    {
        Buff remove = buffs.Find((b) => { if (b.buffName == buff) return true; else return false; });

        if (!remove.isRemoveable)
        {
            print("The buff \"" + remove.buffName + "\" should be removed but is unremovable.");
            return;
        }
        if (remove != null)
        {
            remove.Unapply();
            buffs.Remove(remove);
        }
    }
    public void ForceUnapplyBuff(string buff)
    {
        Buff remove = buffs.Find((b) => { if (b.buffName == buff) return true; else return false; });

        if (remove != null)
        {
            remove.Unapply();
            buffs.Remove(remove);
        }
    }

    public virtual void CheckAutoAttackAggro()
    {

    }
    public void RegisterProjectile(Projectile projectile)
    {
        projectiles.Add(projectile);
    }
    public void DisjointProjectiles(bool force)
    {
        foreach (var projectile in projectiles)
        {
            projectile.Disjoint(force);
        }
    }

    //private void StopAnimation()
    //{
    //    animationComponent.Stop();
    //}
    private bool IsDead()
    {
        if (attributes[AttributeType.CurrentHealth] <= 0)
            return true;
        else return false;
    }
    private float currentDeltaTime;
    private void OnEverySecondHandler()
    {
        currentDeltaTime += Time.deltaTime;
        if (currentDeltaTime >= 1)
        {
            currentDeltaTime = 0;

            #region Regeneration
            ReceiveHeal(totalHealthRegeneration);
            ReceiveMana(totalManaRegeneration);
            #endregion

            #region Buffs & spells
            for (int i = 0; i < buffs.Count; i++)
            {
                buffs[i].OnEverySecondEvent();
            }
            for (int i = 0; i < spells.Count; i++)
            {
                spells[i].UpdateAura(this);
            }
            #endregion
        }


    }
    private void TryDoRightClickDamageToTarget()
    {
        if (lockedTarget == null)
            return;
        if (isDead)
            return;
        if (isStunned)
            return;

        if (lockedTarget.isDead)
        {
            ResetDoRightClickDamageRPC();
            return;
        }
        if (lockedTarget.fullObject != null)
        {
            if (!lockedTarget.fullObject.activeSelf)
            {
                DoRightClickDamage(null);
                return;
            }
        }

        //if (currentAttackTime > 0)
        //{
        //    animationState = AnimationState.Attack;
        //    currentAttackTime -= Time.deltaTime;

        //    if (currentAttackTime <= 0)
        //    {
        //        currentAttackTime = 0;
        //    }
        //    else
        //        return;
        //}


        if (currentAttackTime > 0)
        {
            currentAttackTime -= Time.deltaTime;
            animationState = AnimationState.Attack;
            return;
        }
        else
            currentAttackTime = 0;

        if (currentAttackTime <= 0 && Vector2.Distance(new Vector2(lockedTarget.transform.position.x, lockedTarget.transform.position.z), new Vector2(this.transform.position.x, this.transform.position.z)) <= attributes[AttributeType.AttackRange])
        {
            //navMeshAgentComponent.destination = transform.position;
            currentAttackTime = attackTime;

            #region Look if attacking an uphill target and damage dealing
            //if (lockedTarget.transform.position.y > this.transform.position.y)
            //{
            //    int didAttackMiss = UnityEngine.Random.Range(1, 100);
            //    didAttackMiss = 1; // TODO: Do a better system that checks if the enemy target unit is uphill, then remove this line
            //    if (didAttackMiss <= rightClickAttackUphillMisschance)
            //    {
            //        // TODO: Do damage
            //    }
            //    else
            //        print("Your attack randomly missed because the enemy unit was uphill.");
            //}

            GotoPosition(transform.position);

            int totalDmg = totalDamage;
            if (attackMode == AttackMode.Ranged)
            {
                GameObject projectile = (GameObject)Instantiate(rangedProjectile, rangedProjectileSpawn.position, Quaternion.identity);
                Projectile p = projectile.GetComponent<Projectile>();
                p.target = lockedTarget;
                lockedTarget.RegisterProjectile(p);

                p.OnProjectileHits += p_OnProjectileHits;
            }
            else
            {
                if (OnDoRightClickDamage != null)
                    OnDoRightClickDamage(ref totalDmg, lockedTarget);

                lockedTarget.ReceiveDamage(totalDmg, DamageType.Physical, this, true);
            }
            #endregion
        }
        else
        {
            currentAttackTime = attackTime;
            this.GotoPosition(lockedTarget.transform.position);
        }
    }

    void p_OnProjectileHits(BaseUnit target)
    {
        int totalDmg = totalDamage;
        if (OnDoRightClickDamage != null)
            OnDoRightClickDamage(ref totalDmg, target);

        target.ReceiveDamage(totalDmg, DamageType.Physical, this, true);
    }
}

[Serializable]
public enum AttributeType
{
    // Stärkeabhängigkeit
    BaseMaxHealth,
    BonusMaxHealth,
    CurrentHealth,
    BaseHealthRegeneration,
    BonusHealthRegeneration,
    // Agilitätsabhängigkeit
    BaseAttackSpeed,
    BonusAttackSpeed,
    BaseArmor,
    BonusArmor,
    // Intelligenzabhängigkeit
    BaseMaxMana,
    BonusMaxMana,
    CurrentMana,
    BaseMagicResistance,
    BonusMagicResistance,
    BaseManaRegeneration,
    BonusManaRegeneration,
    // Attribute
    BaseStrength,
    BonusStrength,
    BaseAgility,
    BonusAgility,
    BaseIntelligence,
    BonusIntelligence,

    // Others
    MovementSpeed,
    AttackRange,

    // Primärattributsabhängigkeit
    BaseDamage,
    BonusDamage
}
[Serializable]
public enum UnitSide
{
    Green,
    Red,
    Neutral,
    All
}
[Serializable]
public enum PrimaryAttribute
{
    Strength,
    Agility,
    Intelligence
}
[Serializable]
public enum DamageType
{
    Physical,
    Magical,
    Pure,
    Composite
}
[Serializable]
public enum State
{
    Normal,
    MagicImmune,
    Ethereal,
    Invulnerable
}
[Serializable]
public enum AnimationState
{
    Idle,
    Run,
    Die,
    Attack,
    SpellCast,
    Stunned
}
[Serializable]
public enum AttackMode
{
    Melee,
    Ranged
}

﻿using UnityEngine;
using System.Collections;

public class MorbicHeroUnit : HeroUnit
{
    public MorbicHeroUnit()
    {
        OnAwake += MorbicHeroUnit_OnAwake;
    }

    void MorbicHeroUnit_OnAwake()
    {
        spells.Add(Spell.GetSpell("Vampires Vengeance"));
        spells.Add(Spell.GetSpell("Blink"));
        spells.Add(Spell.GetSpell("Pest Aura"));
        spells.Add(Spell.GetSpell("Grave Trap"));

        Spell triggernearesttrapspell = Spell.GetSpell("Trigger nearest trap");
        spells.Add(triggernearesttrapspell);
        triggernearesttrapspell.LevelUp(this);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HeroUnit : BaseUnit
{
    public static List<HeroUnit> allHeroes = new List<HeroUnit>();
    public int kills, deaths, lasthits;

    public PlayerSlot playerSlot;
    public GameObject leftHandGameObject, rightHandGameObject;

    public int strengthGainOnLevelUp, agilityGainOnLevelUp, intelligenceGainOnLevelUp;
    public Level level;
    public int usableLevelSpellPoints;

    public float baseRespawnTime, bonusRespawnTime;

    public HeroUnit()
    {
        OnAwake += HeroUnit_OnAwake;
        OnStart += HeroUnit_OnStart;
        OnUpdate += HeroUnit_OnUpdate;
        OnIsDead += HeroUnit_OnIsDead;
        OnKillUnit += HeroUnit_OnKillUnit;
        OnGetDestroyed += HeroUnit_OnGetDestroyed;
    }

    void HeroUnit_OnGetDestroyed()
    {
        if (allHeroes.Contains(this))
            allHeroes.Remove(this);
    }

    void HeroUnit_OnKillUnit(ref BaseUnit killedUnit)
    {
        lasthits += 1;
        if (killedUnit is HeroUnit)
            kills += 1;
    }

    void HeroUnit_OnIsDead(ref BaseUnit killer)
    {
        if (IsInvoking("Respawn"))
        {
            print("The method \"HeroUnit.Respawn\" got invoked more than once. Stopping invoke...");
            return;
        }

        GUIInfoLog.Instance.AddMessage(playerSlot.name + " (" + unitName + ") has been killed by " + (killer is HeroUnit ? (killer as HeroUnit).playerSlot.name + " (" + killer.unitName + ")" : killer.unitName));
        deaths += 1;

        if (baseRespawnTime + bonusRespawnTime < 0)
            baseRespawnTime = bonusRespawnTime = 0;

        Invoke("Respawn", baseRespawnTime + bonusRespawnTime);
    }

    void HeroUnit_OnAwake()
    {
        if(!allHeroes.Contains(this))
            allHeroes.Add(this);

        unitsUnderControl.Add(this);
    }

    void HeroUnit_OnUpdate()
    {
        goldBounty = 40 * level.currentLevel;
        xpBounty = 35 * level.currentLevel;

        baseRespawnTime = 4 * level.currentLevel;
    }

    void HeroUnit_OnStart()
    {
        level.OnLevelUp += level_OnLevelUp;
        level.AddXp(1);
    }

    void level_OnLevelUp()
    {
        if (level.currentLevel != 1)
        {
            attributes[AttributeType.BaseStrength] += strengthGainOnLevelUp;
            attributes[AttributeType.BaseAgility] += agilityGainOnLevelUp;
            attributes[AttributeType.BaseIntelligence] += intelligenceGainOnLevelUp;
        }

        usableLevelSpellPoints += 1;

        if (hasFullHealth)
            attributes[AttributeType.CurrentHealth] = totalMaxHealth;
        if(hasFullMana)
            attributes[AttributeType.CurrentMana] = totalMaxMana;
    }

    public void Respawn()
    {
        DisjointProjectiles(true);
        SetState(State.Normal);
        ReceiveHeal(totalMaxHealth);
        attributes[AttributeType.CurrentMana] = totalMaxMana;
        this.transform.position = side == UnitSide.Green ? GameManagementManager.Instance.greenSpawn.position : GameManagementManager.Instance.redSpawn.position;
        navMeshAgentComponent.Resume();
        animationState = AnimationState.Idle;
        isDead = false;
        GotoPosition(this.transform.position);
    }

    public void Initialize()
    {
        networkView.RPC("SetNetworkPlayerInformation", RPCMode.AllBuffered, playerSlot.name, playerSlot.side.ToString(), Network.player, playerSlot.hero);
    }

    public void LevelUpSpell(Spell spell)
    {
        if (usableLevelSpellPoints <= 0 || spell.currentLevel == spell.maxLevel)
        {
            print("You have no usable skillpoints or the spell is already leveled to max.");
            return;
        }
        if (!spells.Contains(spell))
        {
            print("The hero has no spell called \"" + spell.spellName + "\".");
            return;
        }
        if (spell.isUltimate && level.currentLevel < 6)
        {
            // TODO: Do an ultimate skillsystem.

            print("The spell is an ultimate and therefore not skillable until level 6.");
            return;
        }

        networkView.RPC("LevelUpSpellRPC", RPCMode.AllBuffered, spell.spellName);
    }
    [RPC]
    private void LevelUpSpellRPC(string spellname)
    {
        Spell spell = spells.Find((s) => { if (s.spellName == spellname) return true; else return false; });

        spell.LevelUp(this);
        usableLevelSpellPoints -= 1;
    }

    [RPC]
    private void SetNetworkPlayerInformation(string name, string side, NetworkPlayer player, string hero)
    {
        playerSlot = new PlayerSlot(name, (UnitSide)Enum.Parse(typeof(UnitSide), side), true, player);
        playerSlot.hero = hero;

        List<PlayerSlot> players = playerSlot.side == UnitSide.Green ? GameManagementManager.Instance.currentGame.greenSlots : GameManagementManager.Instance.currentGame.redSlots;
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].name == playerSlot.name)
                players[i] = playerSlot;
        }

        this.side = playerSlot.side;
    }
}

[Serializable]
public class Level
{
    public const int neededXpMultiplicator = 200, maxLevel = 25; // TODO: Find out a nice xp-multiplicator.
    public int currentLevel, currentXp, neededXp;

    public delegate void OnLevelUpHandler();
    public event OnLevelUpHandler OnLevelUp;

    public Level()
    {
        currentLevel = 0;
        currentXp = 0;
        neededXp = 1;

        AddXp(1);
    }

    public bool AddXp(int xp)
    {
        if (currentXp + xp >= neededXp)
        {
            if (currentLevel + 1 > maxLevel)
                return false;

            currentLevel += 1;
            currentXp = (currentXp + xp) - neededXp;
            neededXp = currentLevel * neededXpMultiplicator;

            if(OnLevelUp != null)
                OnLevelUp();

            return true;
        }
        else
        {
            currentXp += xp;
            return false;
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IceSheperdHeroUnit : HeroUnit
{
    public IceSheperdHeroUnit()
    {
        OnAwake += IceSheperdHeroUnit_OnAwake;
    }

    void IceSheperdHeroUnit_OnAwake()
    {
        spells.Add(Spell.GetSpell("The Herd"));
        spells.Add(Spell.GetSpell("Iceshard"));
        spells.Add(Spell.GetSpell("Herd Aura"));
    }
}

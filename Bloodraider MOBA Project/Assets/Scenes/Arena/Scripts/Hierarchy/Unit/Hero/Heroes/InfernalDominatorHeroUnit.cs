﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InfernalDominatorHeroUnit : HeroUnit
{
    public InfernalDominatorHeroUnit()
    {
        OnAwake += InfernalDominatorHeroUnit_OnAwake;
    }

    void InfernalDominatorHeroUnit_OnAwake()
    {
        spells.Add(Spell.GetSpell("Infernal Devour"));
        spells.Add(Spell.GetSpell("Hells Elite"));
    }
}

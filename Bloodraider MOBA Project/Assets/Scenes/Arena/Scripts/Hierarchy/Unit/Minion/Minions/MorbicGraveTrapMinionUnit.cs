﻿using UnityEngine;
using System.Collections;

public class MorbicGraveTrapMinionUnit : MinionUnit
{
    public SpellMorbicGraveTrap graveTrapSpell;

    public MorbicGraveTrapMinionUnit()
    {
        OnAwake += MorbicGraveTrapMinionUnit_OnAwake;
    }

    void MorbicGraveTrapMinionUnit_OnAwake()
    {
        Spell triggertrapmanuallyspell = Spell.GetSpell("Trigger trap");
        this.spells.Add(triggertrapmanuallyspell);
        triggertrapmanuallyspell.LevelUp(this);
    }
}

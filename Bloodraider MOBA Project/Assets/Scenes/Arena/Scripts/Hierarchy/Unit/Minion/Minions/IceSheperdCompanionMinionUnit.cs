﻿using UnityEngine;
using System.Collections;

public class IceSheperdCompanionMinionUnit : MinionUnit
{
    public IceSheperdHeroUnit iceSheperd;

    public IceSheperdCompanionMinionUnit()
    {
        OnAwake += IceSheperdCompanion_OnAwake;
        OnIsDead += IceSheperdCompanionMinionUnit_OnIsDead;
    }

    void IceSheperdCompanionMinionUnit_OnIsDead(ref BaseUnit killer)
    {
        iceSheperd.UnapplyBuff("The Herd Bonus");
    }

    void IceSheperdCompanion_OnAwake()
    {

    }
}

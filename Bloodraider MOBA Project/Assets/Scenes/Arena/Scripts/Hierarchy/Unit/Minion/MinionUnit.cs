﻿using UnityEngine;
using System.Collections;

public class MinionUnit : BaseUnit
{
    public const float autoAttackAggroRange = 16;

    public BaseUnit owner;

    public bool hasMinionLivetime;
    public float minionLivetime, currentMinionLivetime;

    public MinionUnit()
    {
        OnAwake += MinionUnit_OnAwake;
        OnUpdate += MinionUnit_OnUpdate;
        OnKillUnit += MinionUnit_OnKillUnit;
        OnIsDead += MinionUnit_OnIsDead;
        currentMinionLivetime = minionLivetime;
    }

    void MinionUnit_OnIsDead(ref BaseUnit killer)
    {
        owner.unitsUnderControl.Remove(this);
    }

    void MinionUnit_OnKillUnit(ref BaseUnit killedUnit)
    {
        if (killedUnit == this)
            return;

        owner.inventory.gold += killedUnit.goldBounty;
    }

    void MinionUnit_OnAwake()
    {
        currentMinionLivetime = minionLivetime;
    }

    void MinionUnit_OnUpdate()
    {
        if (hasMinionLivetime && !isDead)
        {
            currentMinionLivetime -= Time.deltaTime;
            if (currentMinionLivetime <= 0)
            {
                //int dmg = this.totalMaxHealth + 1;
                //this.ReceiveDamage(dmg, DamageType.Pure, this, false);
                attributes[AttributeType.CurrentHealth] = 0; // This is maybe the safer way to kill the unit
            }
        }
    }

    public override void CheckAutoAttackAggro()
    {
        if (!autoAttack)
            return;
        if (Network.isClient)
            return;
        if (isDead)
            return;

        Collider[] cols = Physics.OverlapSphere(transform.position, autoAttackAggroRange);
        BaseUnit aggroHighPriority = this;
        BaseUnit aggroNormalPriority = this;

        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                BaseUnit b = col.GetComponent<BaseUnit>();
                if (b.side != side && b != this)
                {
                    if (b is NeutralCreepUnit)
                        aggroHighPriority = b;
                    else if (b is HeroUnit || b is LaneCreepUnit || b is MinionUnit || b is StructureUnit)
                        aggroNormalPriority = b;
                }
            }
        }

        if (aggroHighPriority == lockedTarget)
            return;
        else if (aggroNormalPriority == lockedTarget)
            return;

        if (aggroHighPriority != this && aggroHighPriority.state != State.Invulnerable && (!aggroHighPriority.isInvisible || aggroHighPriority.isInvisibilityRevealed))
            DoRightClickDamage(aggroHighPriority);
        else if (aggroNormalPriority != this && aggroNormalPriority.state != State.Invulnerable && (!aggroNormalPriority.isInvisible || aggroNormalPriority.isInvisibilityRevealed))
            DoRightClickDamage(aggroNormalPriority);
    }
}

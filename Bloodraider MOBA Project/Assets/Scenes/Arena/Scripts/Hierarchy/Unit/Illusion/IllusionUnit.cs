﻿using UnityEngine;
using System.Collections;

public class IllusionUnit : BaseUnit
{
    public const int autoAttackAggroRange = 16;

    public Transform fullObjectChild;
    public float liveTime, currentLivetime;
    public int bonusDamagePercentage, dealtDamagePercentage;

    public GameObject sourceUnitGameObject;
    public BaseUnit sourceUnit;
    public BaseUnit owner;

    public IllusionUnit()
    {
        liveTime = 1;

        OnUpdate += IllusionUnit_OnUpdate;
        OnIsDead += IllusionUnit_OnIsDead;
    }

    void IllusionUnit_OnIsDead(ref BaseUnit killer)
    {
        owner.unitsUnderControl.Remove(this);
    }

    void IllusionUnit_OnUpdate()
    {
        if (currentLivetime > 0)
        {
            currentLivetime -= Time.deltaTime;
            if (currentLivetime <= 0)
            {
                attributes[AttributeType.CurrentHealth] = 0;
            }
        }
    }

    public void Initialize(float livetime, int bonusdamage, int dealtdamage, GameObject sourceunitgo)
    {
        sourceUnitGameObject = sourceunitgo;
        sourceUnit = sourceUnitGameObject.GetComponent<BaseUnit>();

        liveTime = livetime;
        currentLivetime = liveTime;

        bonusDamagePercentage = bonusdamage;
        dealtDamagePercentage = dealtdamage;

        GameObject fullObj = (GameObject)Instantiate(sourceUnit.fullObject, transform.position, Quaternion.identity);
        fullObj.transform.parent = fullObjectChild;
        fullObj.transform.position = Vector3.zero;
        fullObj.transform.localPosition = Vector3.zero;

        fullObject = fullObj;

        CharacterController cc = this.GetComponent<CharacterController>();
        CharacterController sourceCC = sourceUnitGameObject.GetComponent<CharacterController>();
        cc.radius = sourceCC.radius;
        cc.height = sourceCC.height;

        NavMeshAgent nva = this.GetComponent<NavMeshAgent>();
        NavMeshAgent sourceNva = sourceUnitGameObject.GetComponent<NavMeshAgent>();
        nva.radius = sourceNva.radius;
        cc.height = sourceNva.height;

        GetComponent<WorldGUIBar>().heightFromUnitOrigin = sourceUnit.GetComponent<WorldGUIBar>().heightFromUnitOrigin;

        name = sourceUnit.name + " Illusion";
        unitName = sourceUnit.unitName;
        portraitIcon = sourceUnit.portraitIcon;

        primaryAttribute = sourceUnit.primaryAttribute;
        attackMode = sourceUnit.attackMode;
        rangedProjectile = sourceUnit.rangedProjectile;
        GameObject rangedprojectilespawn = (GameObject)Instantiate(sourceUnit.rangedProjectileSpawn, transform.position, Quaternion.identity);
        rangedprojectilespawn.transform.parent = this.transform;
        rangedProjectileSpawn = rangedprojectilespawn.transform;

        owner = sourceUnit;
        side = sourceUnit.side;
        baseStrength = sourceUnit.baseStrength;
        baseAgility = sourceUnit.baseAgility;
        baseIntelligence = sourceUnit.baseIntelligence;
        baseArmor = sourceUnit.baseArmor;
        baseAttackRange = sourceUnit.baseAttackRange;
        baseAttackSpeed = sourceUnit.baseAttackSpeed;
        baseAttackTime = sourceUnit.baseAttackTime;
        baseMagicResistance = sourceUnit.baseMagicResistance;
        baseHealthRegeneration = sourceUnit.baseHealthRegeneration;
        baseManaRegeneration = sourceUnit.baseManaRegeneration;
        baseMaxHealth = sourceUnit.baseMaxHealth;
        baseMaxMana = sourceUnit.baseMaxMana;
        baseMovementspeed = sourceUnit.baseMovementspeed;

        attributes = sourceUnit.attributes;
        spells = sourceUnit.spells;

        foreach (var spell in spells)
        {
            spell.isUsable = false;
        }

        BuffIllusionUnit illusionUnitBuff = (BuffIllusionUnit)ApplyBuff("Illusion unit");

        if (GameManagementManager.Instance.currentGameSettings.isSpectatorMode)
            illusionUnitBuff.isVisible = true;
        else
            illusionUnitBuff.isVisible = side != GameManagementManager.Instance.playerHeroUnit.side ? false : true;

        illusionUnitBuff.bonusDamagePercentage = bonusDamagePercentage;
        illusionUnitBuff.dealtDamagePercentage = dealtDamagePercentage;
    }

    public override void CheckAutoAttackAggro()
    {
        if (!autoAttack)
            return;
        if (Network.isClient)
            return;
        if (isDead)
            return;

        Collider[] cols = Physics.OverlapSphere(transform.position, autoAttackAggroRange);
        BaseUnit aggroHighPriority = this;
        BaseUnit aggroNormalPriority = this;

        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                BaseUnit b = col.GetComponent<BaseUnit>();
                if (b.side != side && b != this)
                {
                    if (b is NeutralCreepUnit || b is HeroUnit || b is LaneCreepUnit)
                        aggroHighPriority = b;
                    else if (b is MinionUnit || b is StructureUnit)
                        aggroNormalPriority = b;
                }
            }
        }

        if (aggroHighPriority == lockedTarget)
            return;
        else if (aggroNormalPriority == lockedTarget)
            return;

        if (aggroHighPriority != this && aggroHighPriority.state != State.Invulnerable && (!aggroHighPriority.isInvisible || aggroHighPriority.isInvisibilityRevealed))
            DoRightClickDamage(aggroHighPriority);
        else if (aggroNormalPriority != this && aggroNormalPriority.state != State.Invulnerable && (!aggroNormalPriority.isInvisible || aggroNormalPriority.isInvisibilityRevealed))
            DoRightClickDamage(aggroNormalPriority);
    }
}

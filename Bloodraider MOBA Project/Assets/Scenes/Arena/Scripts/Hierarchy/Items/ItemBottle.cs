﻿using UnityEngine;
using System.Collections;

public class ItemBottle : Item
{
    public int bottleCharges;

    public ItemBottle()
    {
        OnCastSpell += ItemBottle_OnCastSpell;

        OnAddToInventory += ItemBottle_OnAddToInventory;
        OnRemoveFromInventory += ItemBottle_OnRemoveFromInventory;
    }

    void ItemBottle_OnRemoveFromInventory()
    {
        spellInstance.OnPreCastSpell -= spellInstance_OnPreCastSpell;
        spellInstance.OnSpellIsActive -= spellInstance_OnSpellIsActive;
    }
    void ItemBottle_OnAddToInventory()
    {
        spellInstance.OnPreCastSpell += spellInstance_OnPreCastSpell;
        spellInstance.OnSpellIsActive += spellInstance_OnSpellIsActive;
    }

    void spellInstance_OnSpellIsActive(BaseUnit caster, Spell spell)
    {
        caster.OnUpdate += caster_OnUpdate;
    }

    void caster_OnUpdate()
    {
        if (owner.buffs.Find((b) => { if (b.buffName == "Base regeneration") return true; else return false; }))
        {
            bottleCharges = 3;
        }
    }
    bool spellInstance_OnPreCastSpell()
    {
        if (bottleCharges == 0)
        {
            Debug.Log("The bottle you try to use has no bottle charges.");
            return false;
        }
        else return true;
    }

    void ItemBottle_OnCastSpell(Vector3 targetposition, BaseUnit targetunit)
    {
        bottleCharges -= 1;
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Buff : ScriptableObject
{
    public string buffName, buffDescription;
    public Texture2D icon;

    public bool isVisible, isRemoveable, isDebuff, hasLivetime, isApplied, worksOnStructures, isStackable;
    public float livetime, currentLivetime;
    public BaseUnit affectedUnit;

    public delegate void OnApplyStateChangedHandler(Buff f, BaseUnit b);
    public event OnApplyStateChangedHandler OnApplyBuff;
    public event OnApplyStateChangedHandler OnUnapplyBuff;
    public delegate void OnEverySecondHandler(Buff f);
    public event OnEverySecondHandler OnEverySecond;

    public Buff()
    { }

    public static Buff GetBuff(string name)
    {
        return UnityEngine.Object.Instantiate(Resources.Load("Buffs/" + name)) as Buff;
    }

    public void Apply(BaseUnit b)
    {
        affectedUnit = b;
        currentLivetime = livetime;

        if (OnApplyBuff != null)
            OnApplyBuff(this, affectedUnit);

        isApplied = true;
    }
    public void Unapply()
    {
        if (OnUnapplyBuff != null)
            OnUnapplyBuff(this, affectedUnit);

        affectedUnit = null;
        isApplied = false;
    }

    public void OnEverySecondEvent()
    {
        if (OnEverySecond != null)
            OnEverySecond(this);

        if (!hasLivetime)
            return;

        currentLivetime -= 1;
        if (currentLivetime <= 0)
        {
            affectedUnit.ForceUnapplyBuff(buffName);
            currentLivetime = 0;
        }
    }
}

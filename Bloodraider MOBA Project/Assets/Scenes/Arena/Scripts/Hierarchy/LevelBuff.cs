﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelBuff : Buff
{
    public Spell spell;

    public float GetSpellProperty(string name)
    {
        return spell.GetSpellProperty(name);
    }
}

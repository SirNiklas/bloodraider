﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuffBackdoorProtectionChecker : Buff
{
    public float backdoorProtectionInactiveRange = 60;
    public List<StructureUnit> conditionDestroyedUnits;

    public BuffBackdoorProtectionChecker()
    {
        OnApplyBuff += BuffBackdoorProtectionCheck_OnApplyBuff;
        OnUnapplyBuff += BuffBackdoorProtectionCheck_OnUnapplyBuff;
    }

    void BuffBackdoorProtectionCheck_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        OnEverySecond -= BuffBackdoorProtection_OnEverySecond;
    }

    void BuffBackdoorProtectionCheck_OnApplyBuff(Buff f, BaseUnit b)
    {
        OnEverySecond += BuffBackdoorProtection_OnEverySecond;
    }

    void BuffBackdoorProtection_OnEverySecond(Buff f)
    {
        if (!AreConditionStructuresDestroyed())
            affectedUnit.SetState(State.Invulnerable);
        else
            affectedUnit.SetState(State.Normal);

        Collider[] cols = Physics.OverlapSphere(affectedUnit.transform.position, backdoorProtectionInactiveRange);
        foreach (var col in cols)
        {
            if (col.CompareTag("Unit"))
            {
                BaseUnit unit = col.GetComponent<BaseUnit>();
                if (unit is LaneCreepUnit && unit != affectedUnit)
                {
                    LaneCreepUnit lanecreepunit = (LaneCreepUnit)unit;
                    if (lanecreepunit != null)
                    {
                        if (lanecreepunit.side != affectedUnit.side)
                        {
                            affectedUnit.ForceUnapplyBuff("Backdoor Protection bonus");
                            return;
                        }
                    }
                }
            }
        }

        affectedUnit.ApplyBuff("Backdoor protection bonus");
    }

    private bool AreConditionStructuresDestroyed()
    {
        foreach (var c in conditionDestroyedUnits)
        {
            if (c != null)
                return false;
        }

        return true;
    }
}

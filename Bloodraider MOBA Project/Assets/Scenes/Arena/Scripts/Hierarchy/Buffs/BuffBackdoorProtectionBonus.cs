﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class BuffBackdoorProtectionBonus : Buff
{
    public const int backdoorRegenerationBonus = 40, backdoorArmorBonus = 40;
        
    public int idleStateHealth;
    private bool bonusIsAdded;

    public BuffBackdoorProtectionBonus()
    {
        OnApplyBuff += BuffBackdoorProtectionBonus_OnApplyBuff;
        OnUnapplyBuff += BuffBackdoorProtectionBonus_OnUnapplyBuff;

        OnEverySecond += BuffBackdoorProtectionBonus_OnEverySecond;
    }

    void BuffBackdoorProtectionBonus_OnEverySecond(Buff f)
    {
        if (affectedUnit.attributes[AttributeType.CurrentHealth] < idleStateHealth && !bonusIsAdded)
        {
            affectedUnit.attributes[AttributeType.BonusHealthRegeneration] += backdoorRegenerationBonus;
            affectedUnit.attributes[AttributeType.BonusArmor] += backdoorArmorBonus;

            bonusIsAdded = true;
        }
        else if (affectedUnit.attributes[AttributeType.CurrentHealth] >= idleStateHealth && bonusIsAdded)
        {
            affectedUnit.attributes[AttributeType.BonusHealthRegeneration] -= backdoorRegenerationBonus;
            affectedUnit.attributes[AttributeType.BonusArmor] -= backdoorArmorBonus;

            bonusIsAdded = false;
        }
    }

    void BuffBackdoorProtectionBonus_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] -= backdoorRegenerationBonus;
        b.attributes[AttributeType.BonusArmor] -= backdoorArmorBonus;
    }

    void BuffBackdoorProtectionBonus_OnApplyBuff(Buff f, BaseUnit b)
    {
        idleStateHealth = b.attributes[AttributeType.CurrentHealth];


    }
}

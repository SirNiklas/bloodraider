﻿using UnityEngine;
using System.Collections;

public class BuffIceSheperdHerdAura : LevelBuff
{
    private int currentBonusHpRegeneration, currentBonusManaRegeneration;

    public BuffIceSheperdHerdAura()
    {
        OnApplyBuff += BuffIceSheperdHerdAura_OnApplyBuff;
        OnUnapplyBuff += BuffIceSheperdHerdAura_OnUnapplyBuff;
    }

    void BuffIceSheperdHerdAura_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] -= currentBonusHpRegeneration;
        b.attributes[AttributeType.BonusManaRegeneration] -= currentBonusManaRegeneration;
    }
    void BuffIceSheperdHerdAura_OnApplyBuff(Buff f, BaseUnit b)
    {
        currentBonusHpRegeneration = (int)this.GetSpellProperty("Bonus health regeneration");
        currentBonusManaRegeneration = (int)this.GetSpellProperty("Bonus mana regeneration");
        
        b.attributes[AttributeType.BonusHealthRegeneration] += currentBonusHpRegeneration;
        b.attributes[AttributeType.BonusManaRegeneration] += currentBonusManaRegeneration;
    }
}

﻿using UnityEngine;
using System.Collections;

public class BuffIceSheperdIceShardSlow : LevelBuff
{
    private int currentIceShardMsSlow, currentIceShardAsSlow;

    public BuffIceSheperdIceShardSlow()
    {
        OnApplyBuff += BuffIceSheperdIceShardSlow_OnApplyBuff;
        OnUnapplyBuff += BuffIceSheperdIceShardSlow_OnUnapplyBuff;
    }

    void BuffIceSheperdIceShardSlow_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.MovementSpeed] += currentIceShardMsSlow;
        b.attributes[AttributeType.BonusAttackSpeed] += currentIceShardAsSlow;
    }

    void BuffIceSheperdIceShardSlow_OnApplyBuff(Buff f, BaseUnit b)
    {
        currentIceShardMsSlow = Mathf.RoundToInt(b.attributes[AttributeType.MovementSpeed] * (this.GetSpellProperty("Movement speed slow percentage") / 100));
        b.attributes[AttributeType.MovementSpeed] -= currentIceShardMsSlow;

        currentIceShardAsSlow = Mathf.RoundToInt(b.attributes[AttributeType.BaseAttackSpeed] * (this.GetSpellProperty("Attack speed slow percentage") / 100));
        b.attributes[AttributeType.BonusAttackSpeed] -= currentIceShardAsSlow;
    }
}

﻿using UnityEngine;
using System.Collections;

public class BuffIceSheperdTheHerdBonus : Buff
{
    public int bonusHealth, bonusMovementSpeedPercentage;
    private int bonusMovementSpeedPercentageValue;

    public BuffIceSheperdTheHerdBonus()
    {
        OnApplyBuff += BuffIceSheperdTheHerdBonus_OnApplyBuff;
        OnUnapplyBuff += BuffIceSheperdTheHerdBonus_OnUnapplyBuff;
    }

    void BuffIceSheperdTheHerdBonus_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusMaxHealth] -= bonusHealth;
        b.attributes[AttributeType.MovementSpeed] -= bonusMovementSpeedPercentageValue;
    }

    void BuffIceSheperdTheHerdBonus_OnApplyBuff(Buff f, BaseUnit b)
    {
        bonusMovementSpeedPercentageValue = Mathf.RoundToInt(b.attributes[AttributeType.MovementSpeed] * (bonusMovementSpeedPercentage / 100));

        b.attributes[AttributeType.BonusMaxHealth] += bonusHealth;
        b.attributes[AttributeType.MovementSpeed] += bonusMovementSpeedPercentageValue;
    }
}

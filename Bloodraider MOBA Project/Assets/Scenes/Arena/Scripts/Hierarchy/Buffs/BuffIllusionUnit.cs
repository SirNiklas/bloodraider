﻿using UnityEngine;
using System.Collections;

public class BuffIllusionUnit : Buff
{
    public int bonusDamagePercentage, dealtDamagePercentage;

    public BuffIllusionUnit()
    {
        OnApplyBuff += BuffIllusionUnit_OnApplyBuff;
        OnUnapplyBuff += BuffIllusionUnit_OnUnapplyBuff;
    }

    void BuffIllusionUnit_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.OnReceiveDamage -= b_OnReceiveDamage;
        b.OnDoRightClickDamage -= b_OnDoRightClickDamage;
    }

    void BuffIllusionUnit_OnApplyBuff(Buff f, BaseUnit b)
    {
        b.OnReceiveDamage += b_OnReceiveDamage;
        b.OnDoRightClickDamage += b_OnDoRightClickDamage;

        buffDescription = "This is an illusion, dealing only " + dealtDamagePercentage + "% of rightclick damage and receiving " + bonusDamagePercentage + "% bonus damage.";
    }

    void b_OnDoRightClickDamage(ref int damage, BaseUnit receiver)
    {
        int dealtdmg = (dealtDamagePercentage / 100) * damage;
        damage -= dealtdmg;
    }

    void b_OnReceiveDamage(ref int damage, ref DamageType dt, ref BaseUnit dd)
    {
        int bonusdmg = (bonusDamagePercentage / 100) * damage;
        damage += bonusdmg;
    }
}

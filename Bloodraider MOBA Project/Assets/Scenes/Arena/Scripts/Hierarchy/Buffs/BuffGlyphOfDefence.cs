﻿using UnityEngine;
using System.Collections;

public class BuffGlyphOfDefence : Buff
{
    public GameObject glyphEffectObject;
    private GameObject currentGlyphEffectGO;

    public BuffGlyphOfDefence()
    {
        OnApplyBuff += BuffGlyphOfDefence_OnApplyBuff;
        OnUnapplyBuff += BuffGlyphOfDefence_OnUnapplyBuff;
    }

    void BuffGlyphOfDefence_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        Destroy(currentGlyphEffectGO);
        b.SetState(State.Normal);
    }

    void BuffGlyphOfDefence_OnApplyBuff(Buff f, BaseUnit b)
    {
        b.SetState(State.Invulnerable);

        currentGlyphEffectGO = (GameObject)Instantiate(glyphEffectObject, new Vector3(b.transform.position.x, b.transform.position.y + 10, b.transform.position.z), Quaternion.identity);
        currentGlyphEffectGO.transform.parent = b.transform;
    }
}

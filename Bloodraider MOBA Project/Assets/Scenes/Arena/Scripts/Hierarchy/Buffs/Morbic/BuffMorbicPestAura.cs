﻿using UnityEngine;
using System.Collections;

public class BuffMorbicPestAura : LevelBuff
{
    private int currentHealthDamagePercentage;

    public BuffMorbicPestAura()
    {
        //OnEverySecond += BuffMorbicPest_OnEverySecond;
        OnApplyBuff += BuffMorbicPest_OnApplyBuff;
        OnUnapplyBuff += BuffMorbicPest_OnUnapplyBuff;
    }

    void BuffMorbicPest_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] += currentHealthDamagePercentage;
    }

    void BuffMorbicPest_OnApplyBuff(Buff f, BaseUnit b)
    {
        currentHealthDamagePercentage = Mathf.RoundToInt(affectedUnit.attributes[AttributeType.CurrentHealth] * (this.GetSpellProperty("Max health damage percentage") / 100));
        b.attributes[AttributeType.BonusHealthRegeneration] -= currentHealthDamagePercentage;
    }
}

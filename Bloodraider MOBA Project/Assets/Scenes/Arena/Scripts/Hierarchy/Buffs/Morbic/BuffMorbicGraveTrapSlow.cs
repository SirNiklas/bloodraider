﻿using UnityEngine;
using System.Collections;

public class BuffMorbicGraveTrapSlow : LevelBuff
{
    private int currentMovementspeedSlow;

    public BuffMorbicGraveTrapSlow()
    {
        OnApplyBuff += BuffMorbicGraveTrapSlow_OnApplyBuff;
        OnUnapplyBuff += BuffMorbicGraveTrapSlow_OnUnapplyBuff;
    }

    void BuffMorbicGraveTrapSlow_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.MovementSpeed] += currentMovementspeedSlow;
        b.attributes[AttributeType.BonusAttackSpeed] += (int)spell.GetSpellProperty("Attackspeed slow");
    }

    void BuffMorbicGraveTrapSlow_OnApplyBuff(Buff f, BaseUnit b)
    {
        currentMovementspeedSlow = Mathf.RoundToInt((b.attributes[AttributeType.MovementSpeed] * (spell.GetSpellProperty("Movementspeed slow percentage") / 100)));

        b.attributes[AttributeType.MovementSpeed] -= currentMovementspeedSlow;
        b.attributes[AttributeType.BonusAttackSpeed] -= (int)spell.GetSpellProperty("Attackspeed slow");
    }
}

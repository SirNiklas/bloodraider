﻿using UnityEngine;
using System.Collections;

public class BuffBottleRegeneration : Buff
{
    public int bonusHealthRegeneration;
    public int bonusManaRegeneration;

    public BuffBottleRegeneration()
    {
        OnApplyBuff += BuffBottleRegeneration_OnApplyBuff;
        OnUnapplyBuff += BuffBottleRegeneration_OnUnapplyBuff;
    }

    void BuffBottleRegeneration_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] -= bonusHealthRegeneration;
        b.attributes[AttributeType.BonusManaRegeneration] -= bonusManaRegeneration;
    }

    void BuffBottleRegeneration_OnApplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] += bonusHealthRegeneration;
        b.attributes[AttributeType.BonusManaRegeneration] += bonusManaRegeneration;
    }
}

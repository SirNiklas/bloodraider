﻿using UnityEngine;
using System.Collections;

public class BuffHealingPotionHeal : Buff
{
    public int bonusHealthRegeneration;

    public BuffHealingPotionHeal()
    {
        OnApplyBuff += BuffHealingPotionHeal_OnApplyBuff;
        OnUnapplyBuff += BuffHealingPotionHeal_OnUnapplyBuff;
    }

    void BuffHealingPotionHeal_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.OnReceiveDamage -= b_OnReceiveDamage;
        b.attributes[AttributeType.BonusHealthRegeneration] -= bonusHealthRegeneration;
    }

    void BuffHealingPotionHeal_OnApplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] += bonusHealthRegeneration;
        b.OnReceiveDamage += b_OnReceiveDamage;
    }

    void b_OnReceiveDamage(ref int damage, ref DamageType dt, ref BaseUnit dd)
    {
        if (affectedUnit == null)
            return;
        if (dd.side == affectedUnit.side)
            return;

        affectedUnit.UnapplyBuff(buffName);
    }
}

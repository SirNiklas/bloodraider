﻿using UnityEngine;
using System.Collections;

public class BuffInvisibilityRevealed : Buff
{
    public BuffInvisibilityRevealed()
    {
        OnApplyBuff += BuffInvisibilityDetected_OnApplyBuff;
        OnUnapplyBuff += BuffInvisibilityDetected_OnUnapplyBuff;
    }

    void BuffInvisibilityDetected_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.RevealInvisibility(false);
    }

    void BuffInvisibilityDetected_OnApplyBuff(Buff f, BaseUnit b)
    {
        b.RevealInvisibility(true);
    }
}

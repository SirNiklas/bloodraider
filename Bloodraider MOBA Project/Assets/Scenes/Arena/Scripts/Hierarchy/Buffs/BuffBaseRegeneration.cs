﻿using UnityEngine;
using System.Collections;

public class BuffBaseRegeneration : Buff
{
    public int bonusRegeneration;

    public BuffBaseRegeneration()
    {
        OnApplyBuff += BuffBaseRegeneration_OnApplyBuff;
        OnUnapplyBuff += BuffBaseRegeneration_OnUnapplyBuff;
    }

    void BuffBaseRegeneration_OnUnapplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] -= bonusRegeneration;
        b.attributes[AttributeType.BonusManaRegeneration] -= bonusRegeneration;
    }

    void BuffBaseRegeneration_OnApplyBuff(Buff f, BaseUnit b)
    {
        b.attributes[AttributeType.BonusHealthRegeneration] += bonusRegeneration;
        b.attributes[AttributeType.BonusManaRegeneration] += bonusRegeneration;
    }
}

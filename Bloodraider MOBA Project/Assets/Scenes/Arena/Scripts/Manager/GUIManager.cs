﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour
{
    public static GUIManager Instance;

    public GUISkin globalMenuGUISkin;
    public GUIStyle friendlyHealthBarGUIStyle, enemyHealthBarGUIStyle, manaBarGUIStyle, worldBarBackgroundGUIStyle;

    public GUIManager()
    {
        Instance = this;
    }

    public Rect GetImprovedRect(Rect old)
    {
        return new Rect(old.x + 10, old.y + 25, old.width - 20, old.height - 35);
    }
}

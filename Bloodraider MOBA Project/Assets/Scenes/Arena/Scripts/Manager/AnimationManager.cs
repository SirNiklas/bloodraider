﻿using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour
{
    public static AnimationManager Instance;
    void Awake()
    {
        Instance = this;
    }

    // 0 = Attack1, 1 = Die, 2 = Run, 3 = Idle
    public void UpdateAnimations(BaseUnit b)
    {
        if (!b.animator)
            return;

        int animationMode = -1;
        switch (b.animationState)
        {
            case AnimationState.Idle:
                animationMode = 3;
                break;
            case AnimationState.Run:
                animationMode = 2;
                break;
            case AnimationState.Die:
                animationMode = 1;
                break;
            case AnimationState.Attack:
                animationMode = 0;
                break;
            case AnimationState.SpellCast:
                if (b.lastCastedSpell == null)
                    animationMode = -1;
                else
                    animationMode = b.lastCastedSpell.animatorAnimationModeIndex;
                break;
        }

        b.animator.SetInteger("Animation Mode", animationMode);
    }
}

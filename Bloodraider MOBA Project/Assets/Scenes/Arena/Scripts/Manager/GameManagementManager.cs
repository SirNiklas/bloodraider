﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class GameManagementManager : MonoBehaviour
{
    public static GameManagementManager Instance;

    public List<UnitInformation> heroes;
    public Game currentGame;
    public LocalGameSettings currentGameSettings;

    public Transform greenSpawn, redSpawn;
    public ThroneStructureUnit greenThrone, redThrone;
    public Transform[] laneCreepSpawns;
    public GameObject illusionBaseObject;

    public GameState state;
    public float minutes, seconds;

    public HeroUnit playerHeroUnit;

    public GameManagementManager()
    {
        Instance = this;
    }

    void Awake()
    {
        Network.isMessageQueueRunning = true;

        try
        {
            currentGame = Game.LoadFromTemp();
            currentGameSettings = LocalGameSettings.LoadFromTemp();

            currentGame.allPlayers.Clear();
        }
        catch (Exception ex)
        {
            Debug.Log("Game-data loading failed: " + ex.ToString() + "\nReturning to menu.");
            if (Network.isClient || Network.isServer)
                DoDisconnect();
            else
                Application.LoadLevel("Menu");

            return;
        }

        if (Network.peerType == NetworkPeerType.Disconnected)
        {
            state = GameState.Game;
        }
    }

    void Start()
    {
        if (Network.peerType == NetworkPeerType.Disconnected)
            return;

        Invoke("UpdateCurrentGame", 1);
        InvokeRepeating("UpdateLocalGameTime", 0, 1);

        if (Network.isServer)
        {
            networkView.RPC("SetGameState", RPCMode.AllBuffered, "HeroSelection");

            foreach (var neutralcamp in NeutralCamp.neutralCamps)
            {
                neutralcamp.InvokeRespawn();
            }
            LaneCreepManager.Instance.InvokeRespawn();
        }
    }

    void UpdateLocalGameTime()
    {
        if (Network.isServer)
        {
            seconds += 1;
            if (seconds >= 60)
            {
                seconds = 0;
                minutes += 1;
            }
        }
    }
    void UpdateCurrentGame()
    {
        networkView.RPC("AddPlayer", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
        if (!currentGameSettings.isSpectatorMode)
            networkView.RPC("JoinSide", RPCMode.AllBuffered, currentGame.currentSlot.side == UnitSide.Green ? true : false, GameInformationManager.Instance.userProfileName, Network.player);
        else
            networkView.RPC("AddSpectator", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
    }

    void Update()
    {
        if (Network.peerType == NetworkPeerType.Disconnected)
            return;
        
        if (seconds > -90 && Network.isServer && state == GameState.HeroSelection)
        {
            if (playerHeroUnit == null)
            {
                GUIHeroSelection.Instance.PickRandomHero();
            }

            networkView.RPC("SetGameState", RPCMode.AllBuffered, "Beginning");
        }

        if (minutes == .0f && seconds >= .0f && Network.isServer && state == GameState.Beginning)
            networkView.RPC("SetGameState", RPCMode.AllBuffered, "Game");
    }
    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        if (stream.isWriting && Network.peerType == NetworkPeerType.Server)
        {
            int secs = (int)seconds, mins = (int)minutes;

            stream.Serialize(ref secs);
            stream.Serialize(ref mins);
        }
        else if (stream.isReading && Network.peerType == NetworkPeerType.Client)
        {
            int secs = 0, mins = 0;

            stream.Serialize(ref secs);
            stream.Serialize(ref mins);

            seconds = secs;
            minutes = mins;
        }
    }

    void OnApplicationQuit()
    {
        if (Network.peerType == NetworkPeerType.Disconnected)
            return;

        DoDisconnect();
    }
    void OnPlayerDisconnected(NetworkPlayer player)
    {
        //Network.RemoveRPCs(player); // Is that needed if the player objects arent removed?
    }

    void OnGUI()
    {
        Rect r = new Rect(5, 5, 320, 500);
        GUILayout.BeginArea(r);
        GUILayout.BeginVertical();

        GUILayout.Label("Bloodraider client v" + GameInformationManager.version);
        GUILayout.Label("Alpha release, WIP");

        //foreach (var ping in pings)
        //{
        //    GUILayout.Label(ping);
        //}

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    public void DoDisconnect()
    {
        if (Network.isServer)
            MasterServer.UnregisterHost();

        networkView.RPC("RemoveFromSides", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
        networkView.RPC("RemovePlayer", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
        if (currentGameSettings.isSpectatorMode)
        {
            networkView.RPC("RemoveSpectator", RPCMode.AllBuffered, GameInformationManager.Instance.userProfileName);
        }

        Network.Disconnect(500);
        File.Delete(@"temp\current.dat");

        Invoke("LoadMenuScene", 1);
    }
    public void KickSpecificPlayer(string name)
    {
        if (Network.isServer)
            networkView.RPC("KickPlayer", RPCMode.All, name);
    }
    public int GetSpectatorCount()
    {
        return GameManagementManager.Instance.currentGame.spectators.Count;
    }
    void LoadMenuScene()
    {
        Application.LoadLevel("Menu");
    }
    //void UpdatePing()
    //{
          //pings.Clear();

          //foreach(NetworkPlayer n in Network.connections)
          //{
          //    pings.Add(n.ToString() + "-Ping: " + Network.GetAveragePing(n));
          //}
    //}

    //public void SpawnHeroes()
    //{
    //    foreach (var green in currentGame.greenSlots)
    //    {
    //        if (green.isEmpty)
    //            continue;
    //        if (green.hero == "")
    //            green.hero = heroes[UnityEngine.Random.Range(0, heroes.Count - 1)].name;
            
    //        Vector3 oldPos = (green.side == UnitSide.Green ? greenSpawn : redSpawn).position;
    //        GameObject go = SpawnHero(green.hero, new Vector3(oldPos.x + UnityEngine.Random.Range(-2, 2), oldPos.y, oldPos.z + UnityEngine.Random.Range(-2, 2)));
    //        go.GetComponent<HeroUnit>().playerSlot = green;
    //        go.GetComponent<BaseUnit>().side = UnitSide.Green;

    //        if (green.isPlayer)
    //        {
    //            playerHeroUnit = go.GetComponent<HeroUnit>();
    //            SystemSelectionMode.Instance.PerformSelectUnitByLMBClick(go.GetComponent<BaseUnit>());
    //        }
    //    }
    //    foreach (var red in currentGame.redSlots)
    //    {
    //        if (red.isEmpty)
    //            continue;
    //        if (red.hero == "")
    //            red.hero = heroes[UnityEngine.Random.Range(0, heroes.Count - 1)].name;

    //        Vector3 oldPos = (red.side == UnitSide.Green ? greenSpawn : redSpawn).position;
    //        GameObject go = SpawnHero(red.hero, new Vector3(oldPos.x + UnityEngine.Random.Range(-2, 2), oldPos.y, oldPos.z + UnityEngine.Random.Range(-2, 2)));
    //        go.GetComponent<HeroUnit>().playerSlot = red;
    //        go.GetComponent<BaseUnit>().side = UnitSide.Red;

    //        if (red.isPlayer)
    //        {
    //            playerHeroUnit = go.GetComponent<HeroUnit>();
    //            SystemSelectionMode.Instance.selectedUnit = go.GetComponent<BaseUnit>();
    //        }
    //    }
    //}

    public GameObject SpawnHero(string name, Vector3 position)
    {
        return (GameObject)Network.Instantiate(heroes.Find((go) => { if (go.name == name) return true; else return false; }).gameObject, position, Quaternion.identity, 0);
    }
    //[RPC]
    //void UpdateCurrentGame(string[] greenplayers, string[] redplayers, string[] allplayers)
    //{
    //    currentGame.greenSlots.Clear();
    //    foreach (var green in greenplayers)
    //    {
    //        string[] gs = green.Split('$');
    //        currentGame.JoinGreen(gs[0]);
    //    }

    //    currentGame.redSlots.Clear();
    //    foreach (var red in redplayers)
    //    {
    //        currentGame.JoinRed(red);
    //    }

    //    currentGame.allPlayers.Clear();
    //    foreach (var allone in allplayers)
    //    {
    //        currentGame.allPlayers.Add(allone);
    //    }
    //}
    [RPC]
    void JoinSide(bool green, string name, NetworkPlayer p)
    {
        currentGame.RemoveSlot(name);
        if (green)
            currentGame.JoinGreen(name, p);
        else
            currentGame.JoinRed(name, p);
    }
    [RPC]
    void RemoveFromSides(string name)
    {
        currentGame.RemoveSlot(name);
    }
    [RPC]
    void AddPlayer(string name)
    {
        currentGame.allPlayers.Add(name);
    }
    [RPC]
    void RemovePlayer(string name)
    {
        if (currentGame.allPlayers.Contains(name))
            currentGame.allPlayers.Remove(name);
    }
    [RPC]
    void AddSpectator(string name)
    {
        currentGame.spectators.Add(name);
    }
    [RPC]
    void RemoveSpectator(string name)
    {
        if (currentGame.spectators.Contains(name))
            currentGame.spectators.Remove(name);
    }
    //[RPC]
    //void AddPlayer(string name)
    //{
    //    if (currentGame.allPlayers.Contains(name))
    //        currentGame.allPlayers.Remove(name);

    //    currentGame.allPlayers.Add(name);
    //}
    //[RPC]
    //void RemovePlayer(string name)
    //{
    //    currentGame.RemoveSlot(name);

    //    if(currentGame.allPlayers.Contains(name))
    //        currentGame.allPlayers.Remove(name);
    //}
    [RPC]
    void SetGameState(string newstate)
    {
        state = (GameState)Enum.Parse(typeof(GameState), newstate);

        if (state == GameState.HeroSelection)
            GUIStatusPopup.Instance.ShowPopup("Pick a hero");
        else if(state == GameState.Game)
            GUIStatusPopup.Instance.ShowPopup("The game has started!");
    }
    [RPC]
    void KickPlayer(string name)
    {
        if (GameInformationManager.Instance.userProfileName == name)
        {
            DoDisconnect();
        }
    }
}

[Serializable]
public enum GameState
{
    HeroSelection,
    Beginning,
    Game,
    Ending
}

[Serializable]
public class UnitInformation
{
    public string name, types;
    public Texture2D texture;

    public GameObject gameObject;
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttributeManager : MonoBehaviour
{
    public static AttributeManager Instance;
    public AttributeManager()
    {
        Instance = this;
    }

    public int strengthStatMaxHealthMultiplicator = 20, intelligenceStatMaxManaMultiplicator = 13, agilityStatAttackSpeedMultiplicator = 1, agilityStatArmorDivisor = 8;
    public float strengthStatHealthRegenerationMultiplicator = 0.03f, intelligenceStatManaRegenerationMultiplicator = 0.04f, intelligenceStatMagicResistanceMultiplicator = 0.35f;

    public void InitializeAttributes(BaseUnit b)
    {
        b.attributes = new Dictionary<AttributeType, int>();

        // Stärke
        b.attributes.Add(AttributeType.CurrentHealth, 0);
        b.attributes.Add(AttributeType.BaseMaxHealth, b.baseMaxHealth);
        b.attributes.Add(AttributeType.BonusMaxHealth, 0);
        b.attributes.Add(AttributeType.BaseHealthRegeneration, b.baseHealthRegeneration);
        b.attributes.Add(AttributeType.BonusHealthRegeneration, 0);
        // Agilität
        b.attributes.Add(AttributeType.BaseAttackSpeed, b.baseAttackSpeed);
        b.attributes.Add(AttributeType.BonusAttackSpeed, 0);
        b.attributes.Add(AttributeType.BaseArmor, 0);
        b.attributes.Add(AttributeType.BonusArmor, 0);
        // Intelligenz
        b.attributes.Add(AttributeType.CurrentMana, 0);
        b.attributes.Add(AttributeType.BaseMaxMana, 0);
        b.attributes.Add(AttributeType.BonusMaxMana, 0);
        b.attributes.Add(AttributeType.BaseMagicResistance, 0);
        b.attributes.Add(AttributeType.BonusMagicResistance, 0);
        b.attributes.Add(AttributeType.BaseManaRegeneration, b.baseManaRegeneration);
        b.attributes.Add(AttributeType.BonusManaRegeneration, 0);

        // Attribute
        b.attributes.Add(AttributeType.BaseStrength, b.baseStrength);
        b.attributes.Add(AttributeType.BaseAgility, b.baseAgility);
        b.attributes.Add(AttributeType.BaseIntelligence, b.baseIntelligence);
        b.attributes.Add(AttributeType.BonusStrength, 0);
        b.attributes.Add(AttributeType.BonusAgility, 0);
        b.attributes.Add(AttributeType.BonusIntelligence, 0);

        // Anderes
        b.attributes.Add(AttributeType.MovementSpeed, b.baseMovementspeed);
        b.attributes.Add(AttributeType.AttackRange, b.baseAttackRange);
        b.attributes.Add(AttributeType.BaseDamage, b.baseDamage);
        b.attributes.Add(AttributeType.BonusDamage, 0);

        UpdateAttributes(b);
        b.attributes[AttributeType.CurrentHealth] = b.totalMaxHealth;
        b.attributes[AttributeType.CurrentMana] = b.totalMaxMana;
    }
    public void UpdateAttributes(BaseUnit b)
    {
        // Stärke
        b.attributes[AttributeType.BaseMaxHealth] = (b.totalStrength * strengthStatMaxHealthMultiplicator) + b.baseMaxHealth;
        b.attributes[AttributeType.BaseHealthRegeneration] = Mathf.RoundToInt(b.totalStrength * strengthStatHealthRegenerationMultiplicator) + b.baseHealthRegeneration;
        // Agilität
        b.attributes[AttributeType.BaseAttackSpeed] = (b.totalAgility * agilityStatAttackSpeedMultiplicator) + b.baseAttackSpeed;
        b.attributes[AttributeType.BaseArmor] = (b.totalAgility / agilityStatArmorDivisor) + b.baseArmor;
        // Intelligenz
        b.attributes[AttributeType.BaseMaxMana] = (b.totalIntelligence * intelligenceStatMaxManaMultiplicator) + b.baseMaxMana;
        b.attributes[AttributeType.BaseManaRegeneration] = Mathf.RoundToInt(b.totalIntelligence * intelligenceStatManaRegenerationMultiplicator) + b.baseManaRegeneration;
        b.attributes[AttributeType.BaseMagicResistance] = Mathf.RoundToInt(b.totalIntelligence * intelligenceStatMagicResistanceMultiplicator) + b.baseMagicResistance;

        float ias = (float)b.totalAttackSpeed / 100f;
        b.attackTime = b.baseAttackTime / (1f + ias);

        // Schaden
        if (b.primaryAttribute == PrimaryAttribute.Strength)
            b.attributes[AttributeType.BaseDamage] = b.totalStrength + b.baseDamage;
        else if (b.primaryAttribute == PrimaryAttribute.Agility)
            b.attributes[AttributeType.BaseDamage] = b.totalAgility + b.baseDamage;
        else if (b.primaryAttribute == PrimaryAttribute.Intelligence)
            b.attributes[AttributeType.BaseDamage] = b.totalIntelligence + b.baseDamage;
    }
}

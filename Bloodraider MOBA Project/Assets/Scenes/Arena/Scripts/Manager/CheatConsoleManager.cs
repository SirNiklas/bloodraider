﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;

public class CheatConsoleManager : MonoBehaviour
{
    public static CheatConsoleManager Instance;
    public List<Command> availableCommands;

    public void ExecuteCommand(string consoletext)
    {
        consoletext = consoletext.TrimStart();
        consoletext = consoletext.TrimEnd();

        string commandname = "";
        string[] parameter = new string[0];
        try
        {
            commandname = consoletext.Split('(')[0];
            parameter = consoletext.Split('(')[1].Replace(")", "").Replace(", ", ",").Split(',');
        }
        catch
        {
            GUICheatConsole.Instance.WriteConsoleLog("Error: The command is not parseable.");
            return;
        }

        Command calledCommand = ResolveCommandName(commandname);
        if (calledCommand == null)
        {
            GUICheatConsole.Instance.WriteConsoleLog("Error: The called command wasnt found.");
            return;
        }

        List<ParameterValue> parameterValues = new List<ParameterValue>();
        for (int i = 0; i < parameter.Length; i++)
        {
            try
            {
                if (calledCommand.parameter[i].type == ParameterType.Float)
                {
                    float value = float.Parse(parameter[i]);
                    parameterValues.Add(new ParameterValue(value));
                }
                else if (calledCommand.parameter[i].type == ParameterType.Int)
                {
                    int value = int.Parse(parameter[i]);
                    parameterValues.Add(new ParameterValue(value));
                }
                else if (calledCommand.parameter[i].type == ParameterType.String)
                {
                    string value = parameter[i]; // Kein parsen notwendig, da der Quelltyp dem Zieltyp gleicht.
                    parameterValues.Add(new ParameterValue(value));
                }
                else if (calledCommand.parameter[i].type == ParameterType.Bool)
                {
                    bool value = bool.Parse(parameter[i]);
                    parameterValues.Add(new ParameterValue(value));
                }
            }
            catch (Exception ex)
            {
                GUICheatConsole.Instance.WriteConsoleLog("Error: Executing \"" + consoletext + "\n created an error:\n" + ex.Message);
                return;
            }
        }

        calledCommand.CallCommandMethod(parameterValues);
    }

    private Command ResolveCommandName(string name)
    {
        return availableCommands.Find(new Predicate<Command>((c) => { if (c.name == name) return true; else return false; }));
    }
    private void Awake()
    {
        Instance = this;
        Command.commandMethodsType = this.GetType();
    }

    #region Command-Methods
    public void Help(bool full)
    {
        if (full)
        {
            foreach (var command in availableCommands)
            {
                GUICheatConsole.Instance.WriteConsoleLog(command.name + "(");
                foreach (var pm in command.parameter)
                {
                    GUICheatConsole.Instance.WriteConsoleLog(pm.name + " : " + pm.type.ToString());
                }
                GUICheatConsole.Instance.WriteConsoleLog(")");
                GUICheatConsole.Instance.WriteConsoleLog("\n");
            }
        }
        else
        {
            foreach (var command in availableCommands)
            {
                GUICheatConsole.Instance.WriteConsoleLog(command.name + "()");
                GUICheatConsole.Instance.WriteConsoleLog("\n");
            }
        }
    }
    public void Print(string message)
    {
        GUICheatConsole.Instance.WriteConsoleLog("Print: " + message);
    }
    public void Clear()
    {
        GUICheatConsole.Instance.cheatLog.Clear();
    }

    public void Ping()
    {
        GUICheatConsole.Instance.WriteConsoleLog("This feature is not yet implemented!");
    }
    public void GiffGold(int amount)
    {
        if (GameManagementManager.Instance.currentGameSettings.areCheatsEnabled)
        {
            GUICheatConsole.Instance.WriteConsoleLog("No cheats enabled.");
            return;
        }
        if (GameManagementManager.Instance.currentGameSettings.isSpectatorMode)
        {
            GUICheatConsole.Instance.WriteConsoleLog("Players in spectator-mode cannot use this command.");
            return;
        }
        if (amount < 0 || amount > 1000000)
        {
            GUICheatConsole.Instance.WriteConsoleLog("You cannot add less than 0 or more than 1000000 gold.");
            return;
        }

        if (GameManagementManager.Instance.state == GameState.Game)
        {
            GameManagementManager.Instance.playerHeroUnit.inventory.gold += amount;
        }
        else
            GUICheatConsole.Instance.WriteConsoleLog("You cannot use this command before the start of the game.");
    }
    public void GiffXp(int amount)
    {
        if (GameManagementManager.Instance.currentGameSettings.areCheatsEnabled)
        {
            GUICheatConsole.Instance.WriteConsoleLog("No cheats enabled.");
            return;
        }
        if (GameManagementManager.Instance.currentGameSettings.isSpectatorMode)
        {
            GUICheatConsole.Instance.WriteConsoleLog("Players in spectator-mode cannot use this command.");
            return;
        }
        if (amount < 0)
        {
            GUICheatConsole.Instance.WriteConsoleLog("You cannot add less than 0 xp.");
            return;
        }

        if (GameManagementManager.Instance.state == GameState.Game)
        {
            GameManagementManager.Instance.playerHeroUnit.level.AddXp(amount);
        }
        else
            GUICheatConsole.Instance.WriteConsoleLog("You cannot use this command before the start of the game.");
    }
    #endregion
}

// Stellt ein ausführbares Kommando dar.
[Serializable]
public class Command
{
    public static Type commandMethodsType;

    public string name;
    public List<Parameter> parameter;

    public void CallCommandMethod(List<ParameterValue> fulfilledparameters)
    {
        object[] args = new object[fulfilledparameters.Count];
        for (int i = 0; i < args.Length; i++)
		{
            args[i] = fulfilledparameters[i].value;
		}

        commandMethodsType.GetMethod(name).Invoke(CheatConsoleManager.Instance, args);
    }
}

// Stellt einen zu erfüllenden Parameter dar.
[Serializable]
public class Parameter
{
    public string name;
    public ParameterType type;

    public Parameter(string _name, ParameterType _type)
    {
        name = _name;
        type = _type;
    }
}
// Stellt einen erfüllten Parameter dar.
public class ParameterValue
{
    public object value;

    public ParameterValue(object _value)
    {
        value = _value;
    }
}
// Stellt den Typ eines zu erfüllenden Parameter dar.
[Serializable]
public enum ParameterType
{
    Int,
    Float,
    String,
    Bool
}
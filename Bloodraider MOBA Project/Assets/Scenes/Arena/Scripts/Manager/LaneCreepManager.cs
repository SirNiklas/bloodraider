﻿using UnityEngine;
using System.Collections;

public class LaneCreepManager : MonoBehaviour
{
    public static LaneCreepManager Instance;

    public GameObject greenMeleeCreep, greenWizardCreep, redMeleeCreep, redWizardCreep;

    public float startRespawnTime, respawnIntervall;
    public Transform topLaneEdge, midLaneEdge, botLaneEdge;

    public LaneCreepManager()
    {
        Instance = this;
    }

    public void InvokeRespawn()
    {
        if(Network.isServer)
            InvokeRepeating("Respawn", startRespawnTime, respawnIntervall);
    }

    private Lane GetLaneBySpawnName(string spawnname)
    {
        if (spawnname.Contains("Top"))
            return Lane.Top;
        else if (spawnname.Contains("Mid"))
            return Lane.Mid;
        else if (spawnname.Contains("Bot"))
            return Lane.Bot;

        return Lane.Mid;
    }

    void Respawn()
    {
        if (!Network.isServer)
            return;

        foreach (var lanecreepspawn in GameManagementManager.Instance.laneCreepSpawns)
        {
            bool green = lanecreepspawn.name.Contains("Green");
            Lane lane = GetLaneBySpawnName(lanecreepspawn.name);

            for (int i = 0; i < 3; i++)
            {
                GameObject creep = (GameObject)Network.Instantiate((green ? greenMeleeCreep : redMeleeCreep), new Vector3(lanecreepspawn.position.x + Random.Range(-5f, 5f), lanecreepspawn.position.y, lanecreepspawn.position.z + Random.Range(-5f, 5f)), Quaternion.identity, 0);

                LaneCreepUnit laneCreep = creep.GetComponent<LaneCreepUnit>();
                laneCreep.side = (green ? UnitSide.Green : UnitSide.Red);
                laneCreep.lane = lane;
                laneCreep.Go();
            }

            GameObject creep2 = (GameObject)Network.Instantiate((green ? greenWizardCreep : redWizardCreep), new Vector3(lanecreepspawn.position.x + Random.Range(-5f, 5f), lanecreepspawn.position.y, lanecreepspawn.position.z + Random.Range(-5f, 5)), Quaternion.identity, 0);

            LaneCreepUnit laneCreep2 = creep2.GetComponent<LaneCreepUnit>();
            laneCreep2.side = (green ? UnitSide.Green : UnitSide.Red);
            laneCreep2.lane = lane;
            laneCreep2.Go();
        }
    }
}

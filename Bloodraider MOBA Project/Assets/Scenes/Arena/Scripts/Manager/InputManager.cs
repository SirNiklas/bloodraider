﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    private Camera mainCamera;
    private Ray ray;

    void Awake()
    {
        Instance = this;
        mainCamera = Camera.main;
    }

    public bool GetMousePositionRaycastHit(out RaycastHit hit)
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            return true;
        }
        else
            return false;
    }
}

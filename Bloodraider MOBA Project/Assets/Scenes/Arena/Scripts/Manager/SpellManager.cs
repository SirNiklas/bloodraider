﻿using UnityEngine;
using System.Collections;

public class SpellManager : MonoBehaviour
{
    public static SpellManager Instance;

    void Awake()
    {
        Instance = this;
    }

    public void UpdateSpells(BaseUnit b)
    {
        foreach (var spell in b.spells)
        {
            spell.UpdateSpell();
        }
        foreach (var itemslot in b.inventory.itemSlots)
        {
            if(itemslot != null)
            {
                if (itemslot.spellInstance != null)
                    itemslot.spellInstance.UpdateSpell();
            }
        }
    }
}

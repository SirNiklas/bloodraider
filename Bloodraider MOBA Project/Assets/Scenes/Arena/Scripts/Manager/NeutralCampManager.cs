﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NeutralCampManager : MonoBehaviour
{
    public static NeutralCampManager Instance;

    public float startRespawnTime, respawnIntervall;
    public List<NeutralCampCreeps> hardCampNeutralCreeps, mediumCampNeutralCreeps, smallCampNeutralCreeps, bossCampNeutralCreeps;

    void Awake()
    {
        Instance = this;
    }

    public List<GameObject> GetNeutralCreeps(NeutralCampType type)
    {
        try
        {
            switch (type)
            {
                case NeutralCampType.Hard:
                    return hardCampNeutralCreeps[UnityEngine.Random.Range(0, hardCampNeutralCreeps.Count - 1)].neutralCreeps;
                case NeutralCampType.Medium:
                    return mediumCampNeutralCreeps[UnityEngine.Random.Range(0, hardCampNeutralCreeps.Count - 1)].neutralCreeps;
                case NeutralCampType.Small:
                    return smallCampNeutralCreeps[UnityEngine.Random.Range(0, hardCampNeutralCreeps.Count - 1)].neutralCreeps;
                case NeutralCampType.Boss:
                    return bossCampNeutralCreeps[UnityEngine.Random.Range(0, hardCampNeutralCreeps.Count - 1)].neutralCreeps;
                default:
                    return null;
            }
        }
        catch (ArgumentOutOfRangeException) { return null; }
    }
}

[Serializable]
public class NeutralCampCreeps
{
    public string neutralCampName;
    public List<GameObject> neutralCreeps;
}
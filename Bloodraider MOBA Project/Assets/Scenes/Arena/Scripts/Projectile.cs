﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public const float projectileHitRange = 1f;

    public BaseUnit target;
    public Vector3 targetPos;

    public delegate void OnProjectileHitsHandler(BaseUnit target);
    public event OnProjectileHitsHandler OnProjectileHits;

    public float speed;
    public bool isDisjointable, isDisjointed;

    void Update()
    {
        if (target == null)
        {
            Disjoint(true);
            Destroy(gameObject);
        }

        if(!isDisjointed)
            targetPos = target.transform.position;

        transform.LookAt(targetPos);
        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

        if (Vector3.Distance(targetPos, transform.position) < projectileHitRange && OnProjectileHits != null)
        {
            if(!isDisjointed)
                OnProjectileHits(target);

            Destroy(gameObject);
        }
    }

    public void Disjoint(bool force)
    {
        if (!isDisjointable && !force)
            return;
        
        isDisjointed = true;
    }
}

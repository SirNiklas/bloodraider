﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class GUIAssetCreator
{
    [MenuItem("Resources/Create current")]
    public static void CreateCurrent()
    {
        ScriptableObject asset = ScriptableObject.CreateInstance(typeof(BuffIllusionUnit));
        AssetDatabase.CreateAsset(asset, "Assets/Resources/New Asset.asset");

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
